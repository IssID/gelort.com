
<div class=titlebar> Основы языка программирования Python за 10 минут </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
    echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
    <div class=contentInBox>

<img src="https://habrastorage.org/getpro/habr/olpictures/52b/ef1/976/52bef19768f0eb502fd1600cf180965d.png" width="300" height="97" alt="Python Logo" hspace="10" vspace="10" align="left">
<br>На сайте Poromenos' Stuff была 
<br>опубликована статья, в которой, в сжатой форме, 
<br>рассказывают об основах языка Python. Я предлагаю вам перевод этой статьи. Перевод не дословный. Я постарался подробнее объяснить некоторые моменты, которые могут быть непонятны. 
<br>Если вы собрались изучать язык Python, но не можете найти подходящего руководства, то эта 
<br>статья вам очень пригодится! За короткое время, вы сможете познакомиться с 
<br>основами языка Python. Хотя эта статья часто опирается 
<br>на то, что вы уже имеете опыт программирования, но, я надеюсь, даже новичкам 
<br>этот материал будет полезен. Внимательно прочитайте каждый параграф. В связи с 
<br>сжатостью материала, некоторые темы рассмотрены поверхностно, но содержат весь 
<br>необходимый метриал.
<br>
<br>Основные свойства
<br>
<br>
<br>Python не требует явного объявления переменных, является регистро-зависим (переменная var не эквивалентна переменной Var или VAR — это три разные переменные) объектно-ориентированным языком.
<br>
<br>Синтаксис
<br>
<br>
<br>Во первых стоит отметить интересную особенность Python. Он не содержит операторных скобок (begin..end в pascal или {..}в Си), вместо этого блоки выделяются отступами: пробелами или табуляцией, а вход в блок из операторов осуществляется двоеточием. Однострочные комментарии начинаются со знака фунта «#», многострочные — начинаются и заканчиваются тремя двойными кавычками «"""». 
<br>Чтобы присвоить значение пременной используется знак «=», а для сравнения — 
<br>«==». Для увеличения значения переменной, или добавления к строке используется оператор «+=», а для уменьшения — «-=». Все эти операции могут взаимодействовать с большинством типов, в том числе со строками. Например
<br>
<br><blockquote> myvar = <font color="#AE81BC">3</font>
<br> myvar += <font color="#AE81BC">2</font>
<br> myvar -= <font color="#AE81BC">1</font>
<br><font color="#808080">"""Это многострочный комментарий 
<br>Строки заключенные в три двойные
<br>кавычки игнорируются"""</font>
<br> mystring = <font color="E6DB74">"Hello"</font>
<br> mystring += <font color="E6DB74">" world."</font>
<br> <font color="#66D9EF"><b>print</b></font> mystring
<br>Hello world.
<br><font color="#808080"># Следующая строка меняет значения переменных местами. (Всего одна строка!)</font>
<br> myvar, mystring = mystring, myvar</blockquote>
<br>
<br>
<br>Структуры данных
<br>Python содержит такие структуры данных как списки (lists), кортежи (tuples) и словари (dictionaries). Списки — похожи на одномерные массивы (но вы можете использовать Список включающий списки — многомерный массив), кортежи — неизменяемые списки, словари — тоже списки, но индексы могут быть любого типа, а не только числовыми. "Массивы" в Python могут содержать данные любого типа, то есть в одном массиве может могут находиться числовые, строковые и другие типы данных. Массивы начинаются с индекса 0, а последний элемент можно получить по индексу -1 Вы можете присваивать переменным функции и использовать их соответственно.
<br>
<br><blockquote>sample = <span style="color: white;">[</font><font color="#AE81BC">1</font>, <span style="color: white;">[</font><font color="E6DB74">"another"</font>, <font color="E6DB74">"list"</font><span style="color: white;">]</font>, <span style="color: white;">(</font><font color="E6DB74">"a"</font>, <font color="E6DB74">"tuple"</font><span style="color: white;">)</font><span style="color: white;">]</font>
<br><font color="#808080">#Список состоит из целого числа, другого списка и кортежа</font>
<br>mylist = <span style="color: white;">[</font><font color="E6DB74">"List item 1"</font>, <font color="#AE81BC">2</font>, <font color="#AE81BC">3</font>.<font color="#AE81BC">14</font><span style="color: white;">]</font> 
<br><font color="#808080">#Этот список содержит строку, целое и дробное число</font>
<br>mylist<span style="color: white;">[</font><font color="#AE81BC">0</font><span style="color: white;">]</font> = <font color="E6DB74">"List item 1 again"</font> 
<br><font color="#808080">#Изменяем первый (нулевой) элемент листа mylist</font>
<br>mylist<span style="color: white;">[</font>-<font color="#AE81BC">1</font><span style="color: white;">]</font> = <font color="#AE81BC">3</font>.<font color="#AE81BC">14</font> 
<br><font color="#808080">#Изменяем последний элемент листа</font>
<br>mydict = <span style="color: white;">{</font><font color="E6DB74">"Key 1"</font>: <font color="E6DB74">"Value 1"</font>, <font color="#AE81BC">2</font>: <font color="#AE81BC">3</font>, <font color="E6DB74">"pi"</font>: <font color="#AE81BC">3</font>.<font color="#AE81BC">14</font><span style="color: white;">}</font> 
<br><font color="#808080">#Создаем словарь, с числовыми и целочисленным индексами</font>
<br>mydict<span style="color: white;">[</font><font color="E6DB74">"pi"</font><span style="color: white;">]</font> = <font color="#AE81BC">3</font>.<font color="#AE81BC">15</font> 
<br><font color="#808080">#Изменяем элемент словаря под индексом "pi".</font>
<br>mytuple = <span style="color: white;">(</font><font color="#AE81BC">1</font>, <font color="#AE81BC">2</font>, <font color="#AE81BC">3</font><span style="color: white;">)</font> 
<br><font color="#808080">#Задаем кортеж</font>
<br>myfunction = <font color="#66D9EF">len</font> 
<br><font color="#808080">#Python позволяет таким образом объявлять синонимы функции</font>
<br><font color="#66D9EF"><b>print</b></font> <font color="#A6E22E">myfunction</font><span style="color: white;">(</font><font color="#66D9EF">list</font><span style="color: white;">)</font> 
<br><font color="#AE81BC">3</font></blockquote>
<br>
<br>
<br>Вы можете использовать часть массива, задавая первый и последний индекс через двоеточие «:». В таком случае вы получите часть массива, от первого индекса до второго не включительно. Если не указан первый элемент, то отсчет начинается с начала массива, а если не указан последний — то масив считывается до последнего элемента. Отрицательные значения определяют положение элемента с конца. Например:
<br>
<br><blockquote> mylist = <span style="color: white;">[</font><font color="E6DB74">"List item 1"</font>, <font color="#AE81BC">2</font>, <font color="#AE81BC">3</font>.<font color="#AE81BC">14</font><span style="color: white;">]</font>
<br> <font color="#66D9EF"><b>print</b></font> mylist<span style="color: white;">[</font>:<span style="color: white;">]</font> <font color="#808080">#Считываются все элементы массива</font>
<br><span style="color: white;">[</font><font color="E6DB74">'List item 1'</font>, <font color="#AE81BC">2</font>, <font color="#AE81BC">3</font>.<font color="#AE81BC">1400000000000001</font><span style="color: white;">]</font>
<br> <font color="#66D9EF"><b>print</b></font> mylist<span style="color: white;">[</font><font color="#AE81BC">0</font>:<font color="#AE81BC">2</font><span style="color: white;">]</font> <fontcolor="#808080">#Считываются нулевой и первый элемент массива.</font>
<br><span style="color: white;">[</font><font color="E6DB74">'List item 1'</font>, <font color="#AE81BC">2</font><span style="color: white;">]</font>
<br> <font color="#66D9EF"><b>print</b></font> mylist<span style="color: white;">[</font>-<font color="#AE81BC">3</font>:-<font color="#AE81BC">1</font><span style="color: white;">]</font> <font color="#808080">#Считываются элементы от нулевого (-3) до второго (-1) (не включительно)</font>
<br><span style="color: white;">[</font><font color="E6DB74">'List item 1'</font>, <font color="#AE81BC">2</font><span style="color: white;">]</font>
<br> <font color="#66D9EF"><b>print</b></font> mylist<span style="color: white;">[</font><font color="#AE81BC">1</font>:<span style="color: white;">]</font> <font color="#808080">#Считываются элементы от первого, до последнего</font>
<br><span style="color: white;">[</font><font color="#AE81BC">2</font>, <font color="#AE81BC">3</font>.<font color="#AE81BC">14</font><span style="color: white;">]</font></blockquote>
<br>
<br>
<br>Строки
<br>Строки в Python обособляются кавычками двойными «"» или одинарными «'». Внутри двойных ковычек могут присутствовать одинарные или наоборот. К примеру строка «Он сказал 'привет'!» будет выведена на экран как «Он сказал 'привет'!». Если нужно использовать строку из несколько строчек, то эту строку надо начинать и заканчивать тремя двойными кавычками «"""». Вы можете подставить в шаблон строки элементы из кортежа или словаря. Знак процента «%» между строкой и кортежем, заменяет в строке символы «%s» на элемент кортежа. Словари позволяют вставлять в строку элемент под заданным индексом. Для этого надо использовать в строке конструкцию «%(индекс)s». В этом случае вместо «%(индекс)s» будет подставлено значение словаря под заданным индексом.
<br>
<br><blockquote><b>print</b> <font color="E6DB74">"Name: %s<span style="color: #AE81BC; font-weight: bold;">\n</font>Number: %s<span style="color: #AE81BC; font-weight: bold;">\n</font>String: %s"</font> % <span style="color: white;">(</font>my<b>class</b>.<span style="color: white;">name</font>, <font color="#AE81BC">3</font>, <font color="#AE81BC">3</font> * <font color="E6DB74">"-"</font><span style="color: white;">)</font>
<br>Name: Poromenos
<br>Number: <font color="#AE81BC">3</font>
<br>String: ---
<br> 
<br>strString = <font color="E6DB74">"""Этот текст расположен
<br>на нескольких строках"""</font>
<br> <br /> <font color="#66D9EF"><b>print</b></font> <font color="E6DB74">"This %(verb)s a %(noun)s."</font> % <span style="color: white;">{</font><font color="E6DB74">"noun"</font>: <font color="E6DB74">"test"</font>, <font color="E6DB74">"verb"</font>: <font color="E6DB74">"is"</font><span style="color: white;">}</font>
<br>This is a test.</blockquote>
<br>
<br>
<br>Операторы
<br>Операторы while, if, for составляют операторы перемещения. Здесь нет аналога оператора select, так что придется обходиться if. В операторе for происходит сравнение переменной и списка. Чтобы получить список цифр до числа <number> — используйте функцию range(<number>). Вот пример использования операторов 
<br>
<br><blockquote>rangelist = <font color="#66D9EF">range</font><span style="color: white;">(</font><font color="#AE81BC">10</font><span style="color: white;">) </font><font color="#808080">#Получаем список из десяти цифр (от 0 до 9)</font>
<br> <font color="#66D9EF"><b>print</b></font> rangelist
<br><span style="color: white;">[</font><font color="#AE81BC">0</font>, <font color="#AE81BC">1</font>, <font color="#AE81BC">2</font>, <font color="#AE81BC">3</font>, <font color="#AE81BC">4</font>, <font color="#AE81BC">5</font>, <font color="#AE81BC">6</font>, <font color="#AE81BC">7</font>, <font color="#AE81BC">8</font>, <font color="#AE81BC">9</font><span style="color: white;">]</font>
<br><font color="#66D9EF"><b>for</b></font> number <font color="#66D9EF">in</font> rangelist: <font color="#808080">#Пока переменная number (которая каждый раз увеличивается на единицу) входит в список... </font>
<br>    <font color="#808080"># Проверяем входит ли переменная</font>
<br>    <font color="#808080"># numbers в кортеж чисел </font><span style="color: white;">(</font><font color="#AE81BC">3</font>, <font color="#AE81BC">4</font>, <font color="#AE81BC">7</font>, <font color="#AE81BC">9</font><span style="color: white;">)</font>
<br>    <font color="#66D9EF"><b>if</b></font> number <font color="#66D9EF">in</font> <span style="color: white;">(</font><font color="#AE81BC">3</font>, <font color="#AE81BC">4</font>, <font color="#AE81BC">7</font>, <font color="#AE81BC">9</font><span style="color: white;">)</font>: <font color="#808080">#Если переменная number входит в кортеж (3, 4, 7, 9)...</font>
<br>        <font color="#808080"># Операция «<b>break</b>» обеспечивает</font>
<br>        <font color="#808080"># выход из цикла в любой момент</font>
<br>        <font color="#66D9EF"><b>break</b></font> 
<br>    <font color="#66D9EF"><b>else</b></font>:
<br>        <font color="#808080"># «<b>continue</b>» осуществляет "прокрутку"</font>
<br>        <font color="#808080"># цикла. Здесь это не требуется, так как после этой операции </font>
<br>        <font color="#808080"># в любом случае программа переходит опять к обработке цикла</font>
<br>        <font color="#66D9EF"><b>continue</b></font>
<br><font color="#66D9EF"><b>else</b></font>:
<br>    <font color="#808080"># «<b>else</b>» указывать необязательно. Условие выполняется</font>
<br>    <font color="#808080"># если цикл не был прерван при помощи «<b>break</b>».</font>
<br>    <font color="#66D9EF"><b>pass</b></font> <font color="#808080"># Ничего не делать</font>
<br> 
<br><font color="#66D9EF"><b>if</b></font> rangelist<span style="color: white;">[</font><font color="#AE81BC">1</font><span style="color: white;">]</font> == <font color="#AE81BC">2</font>:
<br>    <font color="#66D9EF"><b>print</b></font> <font color="E6DB74">"The second item (lists are 0-based) is 2"</font>
<br><font color="#66D9EF"><strong>elif</strong></font> rangelist<span style="color: white;">[</font><font color="#AE81BC">1</font><span style="color: white;">]</font> == <font color="#AE81BC">3</font>:
<br>    <font color="#66D9EF"><b>print</b></font> <font color="E6DB74">"The second item (lists are 0-based) is 3"</font>
<br><font color="#66D9EF"><b>else</b></font>:
<br>    <font color="#66D9EF"><b>print</b></font> <font color="E6DB74">"Dunno"</font>
<br> 
<br><font color="#66D9EF"><strong>while</strong></font> rangelist<span style="color: white;">[</font><font color="#AE81BC">1</font><span style="color: white;">]</font> == <font color="#AE81BC">1</font>:
<br>    <font color="#66D9EF"><b>pass</b></font></blockquote>
<br>
<br>
<br>Функции
<br>Для объявления функции служит ключевое слово «def». Аргументы функции задаются в скобках после названия функции. Можно задавать необязательные аргументы, присваивая им значение по умолчанию. Функции могут возвращать кортежи, в таком случае надо писать возвращаемые значения через запятую. Ключевое слово «lambda» служит для объявления элементарных функций .
<br>
<br><blockquote><font color="#808080"># arg2 и arg3 - необязательые аргументы, принимают значение объявленное по умолчни,</font>
<br><font color="#808080"># если не задать им другое значение при вызове функци.</font>
<br><font color="#66D9EF"><b>def</b></font> <font color="#A6E22E">myfunction</font><span style="color: white;">(</font>arg1, arg2 = <font color="#AE81BC">100</font>, arg3 = <font color="E6DB74">"test"</font><span style="color: white;">)</font>:
<br>    <font color="#66D9EF"><b>return</b></font> arg3, arg2, arg1
<br><font color="#808080">#Функция вызывается со значением первого аргумента - "Argument 1", второго - по умолчанию, и третьего - "Named argument"</font>.
<br>ret1, ret2, ret3 = myfunction<span style="color: white;">(</font><font color="E6DB74">"Argument 1"</font>, arg3 = <font color="E6DB74">"Named argument"</font><span style="color: white;">)</font>
<br><font color="#808080"># ret1, ret2 и ret3 принимают значения "Named argument", 100, "Argument 1" соответственно</font>
<br> <font color="#66D9EF"><b>print</b></font> ret1, ret2, ret3
<br>Named argument <font color="#AE81BC">100</font> Argument <font color="#AE81BC">1</font>
<br> 
<br><font color="#808080"># Следующая запись эквивалентна <b>def</b> f(x): <b>return</b> x + 1</font>
<br>functionvar = <font color="#66D9EF"><b>lambda</b></font> x: x + <font color="#AE81BC">1</font>
<br> <font color="#66D9EF"><b>print</b></font> functionvar<span style="color: white;">(</font><font color="#AE81BC">1</font><span style="color: white;">)</font>
<br><font color="#AE81BC">2</font></blockquote>
<br>
<br>
<br>Классы
<br>Язык Python ограничен в множественном наследовании в классах. Внутренние переменные и внутренние методы классов начинаются с двух знаков нижнего подчеркивания «__» (например «__myprivatevar»). Мы можем также присвоить значение переменной класса извне. Пример:
<br>
<br><blockquote><font color="#66D9EF"><b>class</b></font> My<b>class</b>:
<br>    common = <font color="#AE81BC">10</font>
<br>    <font color="#66D9EF"><b>def</b></font> <span style="color: #66D9EF;">__init__</font><span style="color: white;">(</font><font color="#FD8520">self</font><span style="color: white;">)</font>:
<br>        <font color="#FD8520">self</font>.<span style="color: white;">myvariable</font> = <font color="#AE81BC">3</font>
<br>    <font color="#66D9EF"><b>def</b></font> <font color="#A6E22E">myfunction</font><span style="color: white;">(</font><font color="#FD8520">self</font>, arg1, arg2<span style="color: white;">)</font>:
<br>        <font color="#66D9EF"><b>return</b></font> <font color="#FD8520">self</font>.<span style="color: white;">myvariable</font>
<br> 
<br>    <font color="#808080"># Здесь мы объявили класс My<b>class</b>. Функция __init__ вызывается автоматически при инициализации классов.</font>
<br> classinstance = My<b>class</b><span style="color: white;">()</font> <font color="#808080"># Мы инициализировали класс и переменная myvariable приобрела значение 3 как заявлено в методе инициализации</font>
<br> classinstance.<span style="color: white;"><font color="#A6E22E">myfunction</font>(</font><font color="#AE81BC">1</font>, <font color="#AE81BC">2</font><span style="color: white;">)</font> <font color="#808080">#Метод myfunction класса My<b>class</b> возвращает значение переменной myvariable</font>
<br><font color="#AE81BC">3</font>
<br><font color="#808080"># Переменная common объявлена во всех классах</font>
<br> classinstance2 = My<b>class</b><span style="color: white;">()</font>
<br> classinstance.<span style="color: white;">common</font>
<br><font color="#AE81BC">10</font>
<br> classinstance2.<span style="color: white;">common</font>
<br><font color="#AE81BC">10</font>
<br><font color="#808080"># Поэтому, если мы изменим ее значение в классе My<b>class</b></font> <font color="#808080">изменятся</font>
<br><font color="#808080"># и ее значения в объектах, инициализированных классом My<b>class</b> </font>
<br> Myclass.<span style="color: white;">common</font> = <font color="#AE81BC">30</font>
<br> classinstance.<span style="color: white;">common</font>
<br><font color="#AE81BC">30</font>
<br> classinstance2.<span style="color: white;">common</font>
<br><font color="#AE81BC">30</font>
<br><font color="#808080"># А здесь мы не изменяем переменную класса. Вместо этого</font>
<br><font color="#808080"># мы объявляем оную в объекте и присваиваем ей новое значение </font>
<br> classinstance.<span style="color: white;">common</font> = <font color="#AE81BC">10</font>
<br> classinstance.<span style="color: white;">common</font>
<br><font color="#AE81BC">10</font>
<br> classinstance2.<span style="color: white;">common</font>
<br><font color="#AE81BC">30</font>
<br> Myclass.<span style="color: white;">common</font> = <font color="#AE81BC">50</font>
<br><font color="#808080"># Теперь изменение переменной класса не коснется </font>
<br><font color="#808080"># переменных объектов этого класса</font>
<br> classinstance.<span style="color: white;">common</font>
<br><font color="#AE81BC">10</font>
<br> classinstance2.<span style="color: white;">common</font>
<br><font color="#AE81BC">50</font>
<br> 
<br><font color="#808080"># Следующий класс является наследником класса My<b>class</b></font>
<br><font color="#808080"># наследуя его свойства и методы, ктому же класс может </font>
<br><font color="#808080"># наследоваться из нескольких классов, в этом случае запись  </font>
<br><font color="#808080"># такая: <b>class</b> Otherclass(Myclass1, Myclass2, MyclassN)</font>
<br><font color="#66D9EF"><b>class</b></font> Otherclass<span style="color: white;">(</font>Myclass<span style="color: white;">)</font>:
<br>    <font color="#66D9EF"><b>def</b></font> <span style="color: #66D9EF;">__init__</font><span style="color: white;">(</font><font color="#FD8520">self</font>, arg1<span style="color: white;">)</font>:
<br>        <font color="#FD8520">self</font>.<span style="color: white;">myvariable</font> = <font color="#AE81BC">3</font>
<br>        <font color="#66D9EF"><b>print</b></font> arg1
<br> 
<br> classinstance = Otherclass<span style="color: white;">(</font><font color="E6DB74">"hello"</font><span style="color: white;">)</font>
<br>hello
<br> classinstance.<span style="color: white;"><font color="#A6E22E">myfunction</font>(</font><font color="#AE81BC">1</font>, <font color="#AE81BC">2</font><span style="color: white;">)</font>
<br><font color="#AE81BC">3</font>
<br><font color="#808080"># Этот класс не имеет совйтсва test, но мы можем </font>
<br><font color="#808080"># объявить такую переменную  для объекта. Причем</font>
<br><span style=";;;color: #808080; font-style: italic;"># tэта переменная будет членом только <b>class</b>instance.</font></span>
<br> classinstance.test = <font color="#AE81BC">10</font>
<br> classinstance.test
<br><font color="#AE81BC">10</font></blockquote>
<br>
<br>
<br>Исключения
<br>Исключения в Python имеют структуру try-except [exceptionname]:
<br>
<br><blockquote><font color="#66D9EF"><b>def</b></font> somefunction<span style="color: white;">()</font>:
<br>    <font color="#66D9EF"><b>try</b></font>:
<br>        <font color="#808080"># Деление на ноль вызывает ошибку</font>
<br>        <font color="#AE81BC">10</font> / <font color="#AE81BC">0</font>
<br>    <font color="#66D9EF"><b>except</b></font> <font color="#66D9EF">ZeroDivisionError</font>:
<br>		<font color="#808080"># Но программа не "Выполняет недопустимую операцию"</font>
<br>		<font color="#808080"># А обрабатывает блок исключения соответствующий ошибке «ZeroDivisionError»</font>
<br>        <font color="#66D9EF"><b>print</b></font> <font color="E6DB74">"Oops, invalid."</font>
<br> 
<br><b>somefunction</b><span style="color: white;">()</font>
<br>Oops, invalid.</blockquote>
<br>
<br>
<br>Импорт
<br>Внешние библиотеки можно подключить процедурой «import [libname]», где [libname] — название подключаемой библиотеки. Вы так же можете использовать команду «from [libname] import [funcname]», чтобы вы могли использовать функцию [funcname] из библиотеки [libname]
<br>
<br><blockquote><font color="#66D9EF"><b>import</b></font> <font color="#dc143c;">random</font> <font color="#808080">#Импортируем библиотеку «random»</font>
<br><font color="#66D9EF"><b>from</b></font> <font color="#dc143c;">time</font> <font color="#66D9EF"><b>import</b></font> clock <font color="#808080">#И заодно функцию «clock» из библиотеки «time»</font>
<br> 
<br>randomint = <font color="#dc143c;">random</font>.<span style="color: white;">randint</font><span style="color: white;">(</font><font color="#AE81BC">1</font>, <font color="#AE81BC">100</font><span style="color: white;">)</font>
<br> <font color="#66D9EF"><b>print</b></font> randomint
<br><font color="#AE81BC">64</font></blockquote>
<br>
<br>
<br>Работа с файловой системой
<br>
<br>
<br>Python имеет много встроенных библиотек. В этом примере мы попробуем сохранить в бинарном файле структуру списка, прочитать ее и сохраним строку в текстовом файле. Для преобразования структуры данных мы будем использовать стандартную библиотеку «pickle»
<br>
<br><blockquote><font color="#66D9EF"><b>import</b></font> <font color="#dc143c;">pickle</font>
<br>mylist = <span style="color: white;">[</font><font color="E6DB74">"This"</font>, <font color="E6DB74">"is"</font>, <font color="#AE81BC">4</font>, <font color="#AE81BC">13327</font><span style="color: white;">]</font>
<br><font color="#808080"># Откроем файл C:\binary.dat для записи. Символ «r» </font>
<br><font color="#808080"># предотвращает замену специальных сиволов (таких как \n, \t, \b и др.).</font>
<br>myfile = <font color="#66D9EF">file</font><span style="color: white;">(</font>r<font color="E6DB74">"C:<span style="color: #AE81BC;">\b</font>inary.dat"</font>, <font color="E6DB74">"w"</font><span style="color: white;">)</font>
<br><font color="#dc143c;">pickle</font>.<span style="color: white;">dump(</font>mylist, myfile<span style="color: white;">)</font>
<br>myfile.<span style="color: white;">close()</font>
<br> 
<br>myfile = <font color="#66D9EF">file</font><span style="color: white;">(</font>r<font color="E6DB74">"C:<span style="color: #AE81BC;">\t</font>ext.txt"</font>, <font color="E6DB74">"w"</font><span style="color: white;">)</font>
<br>myfile.<span style="color: white;">write(</font><font color="E6DB74">"This is a sample string"</font><span style="color: white;">)</font>
<br>myfile.<span style="color: white;">close()</font>
<br> 
<br>myfile = <font color="#66D9EF">file</font><span style="color: white;">(</font>r<font color="E6DB74">"C:<span style="color: #AE81BC; font-weight: bold;">\t</font>ext.txt"</font><span style="color: white;">)</font>
<br> <font color="#66D9EF"><b>print</b></font> myfile.<span style="color: white;">read()</font>
<br><font color="E6DB74">'This is a sample string'</font>
<br>myfile.<span style="color: white;">close()</font>
<br> 
<br><font color="#808080"># Открываем файл для чтения</font>
<br>myfile = <font color="#66D9EF">file</font><span style="color: white;">(</font>r<font color="E6DB74">"C:<span style="color: #AE81BC; font-weight: bold;">\b</font>inary.dat"</font><span style="color: white;">)</font>
<br>loadedlist = <font color="#dc143c;">pickle</font>.<span style="color: white;">load(</font>myfile<span style="color: white;">)</font>
<br>myfile.<span style="color: white;">close()</font>
<br> <font color="#66D9EF"><b>print</b></font> loadedlist
<br><span style="color: white;">[</font><font color="E6DB74">'This'</font>, <font color="E6DB74">'is'</font>, <font color="#AE81BC">4</font>, <font color="#AE81BC">13327</font><span style="color: white;">]</font></blockquote>
<br>
<br>
<br>Особенности
<br>Условия могут комбинироваться. 1 < a < 3 выполняется тогда, когда а больше 1, но меньше 3.
<br>Используйте операцию «del» чтобы очищать переменные или элементы массива.
<br>Python предлагает большие возможности для работы со списками. Вы можете использовать операторы объявлении структуры списка. Оператор for позволяет задавать элементы списка в определенной последовательности, а if — позволяет выбирать элементы по условию.
<br>
<br><blockquote> lst1 = <span style="color: white;">[</font><font color="#AE81BC">1</font>, <font color="#AE81BC">2</font>, <font color="#AE81BC">3</font><span style="color: white;">]</font>
<br> lst2 = <span style="color: white;">[</font><font color="#AE81BC">3</font>, <font color="#AE81BC">4</font>, <font color="#AE81BC">5</font><span style="color: white;">]</font>
<br> <font color="#66D9EF"><b>print</b></font> <span style="color: white;">[</font>x * y <font color="#66D9EF"><b>for</b></font> x <font color="#66D9EF">in</font> lst1 <font color="#66D9EF"><b>for</b></font> y <font color="#66D9EF">in</font> lst2<span style="color: white;">]</font>
<br><span style="color: white;">[</font><font color="#AE81BC">3</font>, <font color="#AE81BC">4</font>, <font color="#AE81BC">5</font>, <font color="#AE81BC">6</font>, <font color="#AE81BC">8</font>, <font color="#AE81BC">10</font>, <font color="#AE81BC">9</font>, <font color="#AE81BC">12</font>, <font color="#AE81BC">15</font><span style="color: white;">]</font>
<br> <font color="#66D9EF"><b>print</b></font> <span style="color: white;">[</font>x <font color="#66D9EF"><b>for</b></font> x <font color="#66D9EF">in</font> lst1 <font color="#66D9EF"><b>if</b></font> <font color="#AE81BC">4</font> > x > <font color="#AE81BC">1</font><span style="color: white;">]</font>
<br><span style="color: white;">[</font><font color="#AE81BC">2</font>, <font color="#AE81BC">3</font><span style="color: white;">]</font>
<br><font color="#808080"># Оператор «any» возвращает true, если хотя   </font>
<br><font color="#808080"># бы одно из условий, входящих в него, выполняется.</font>
<br> any<span style="color: white;">(</font>i % <font color="#AE81BC">3</font> <font color="#66D9EF"><b>for</b></font> i <font color="#66D9EF">in</font> <span style="color: white;">[</font><font color="#AE81BC">3</font>, <font color="#AE81BC">3</font>, <font color="#AE81BC">4</font>, <font color="#AE81BC">4</font>, <font color="#AE81BC">3</font><span style="color: white;">])</font>
<br><font color="#66D9EF">True</font>
<br><font color="#808080"># Следующая процедура подсчитывает количество </font>
<br><font color="#808080"># подходящих элементов в списке</font>
<br> <font color="#66D9EF">sum</font><span style="color: white;">(</font><font color="#AE81BC">1</font> <font color="#66D9EF"><b>for</b></font> i <font color="#66D9EF">in</font> <span style="color: white;">[</font><font color="#AE81BC">3</font>, <font color="#AE81BC">3</font>, <font color="#AE81BC">4</font>, <font color="#AE81BC">4</font>, <font color="#AE81BC">3</font><span style="color: white;">]</font> <font color="#66D9EF"><b>if</b></font> i == <font color="#AE81BC">3</font><span style="color: white;">)</font>
<br><font color="#AE81BC">3</font>
<br> <font color="#66D9EF"><b>del</b></font> lst1<span style="color: white;">[</font><font color="#AE81BC">0</font><span style="color: white;">]</font>
<br> <font color="#66D9EF"><b>print</b></font> lst1
<br><span style="color: white;">[</font><font color="#AE81BC">2</font>, <font color="#AE81BC">3</font><span style="color: white;">]</font>
<br> <font color="#66D9EF"><b>del</b></font> lst1</blockquote>
<br>
<br>
<br>Глобальные переменные объявляются вне функций и могут быть прочитанны без каких либо объявлений. Но если вам необходимо изменить значение глобальной переменной из функции, то вам необходимо объявить ее в начале функции ключевым словом «global», если вы этого не сделаете, то Python объявит переменную, доступную только для этой функции.
<br>
<br><blockquote>number = <font color="#AE81BC">5</font>
<br> 
<br><font color="#66D9EF"><b>def</b></font> myfunc<span style="color: white;">()</font>:
<br>    <font color="#808080"># Выводит 5</font>
<br>    <font color="#66D9EF"><b>print</b></font> number
<br> 
<br><font color="#66D9EF"><b>def</b></font> anotherfunc<span style="color: white;">()</font>:
<br>    <font color="#808080"># Это вызывает исключение, поскольку глобальная апеременная </font>
<br>    <font color="#808080"># не была вызванна из функции. Python в этом случае создает </font>
<br>    <font color="#808080"># одноименную переменную внутри этой функции и доступную</font>
<br>    <font color="#808080"># только для операторов этой функции.</font>
<br>    <font color="#66D9EF"><b>print</b></font> number
<br>    number = <font color="#AE81BC">3</font>
<br> 
<br><font color="#66D9EF"><b>def</b></font> yetanotherfunc<span style="color: white;">()</font>:
<br>    <font color="#66D9EF"><b>global</b></font> number
<br>    <font color="#808080"># И только из этой функции значение переменной изменяется.</font>
<br>    number = <font color="#AE81BC">3</font></blockquote>
<br>
<br>
<br>Эпилог
<br>Разумеется в этой статье не описываются все возможности Python. Я надеюсь что эта статья поможет вам, если вы захотите и в дальнейшем изучать этот язык программирования. 
<br>
<br>Преимущества Python
<br>Скорость выполнения программ написанных на Python очень высока. Это связанно с тем, что основные библиотеки Python 
<br>написаны на C++ и выполнение задач занимает меньше времени, чем на других языках высокого уровня.
<br>В связи с этим вы можете писать свои собственные модули для Python на C или C++
<br>В стандартныx библиотеках Python вы можете найти средства для работы с электронной почтой, протоколами 
<br>Интернета, FTP, HTTP, базами данных, и пр.
<br>Скрипты, написанные при помощи Python выполняются на большинстве современных ОС. Такая переносимость обеспечивает Python применение в самых различных областях.
<br>Python подходит для любых решений в области программирования, будь то офисные программы, вэб-приложения, GUI-приложения и т.д.
<br>Над разработкой Python трудились тысячи энтузиастов со всего мира. Поддержкой современных технологий в стандартных библиотеках мы можем быть обязаны именно тому, что Python был открыт для всех желающих.
<br>
<p><a id="user-content-settingup"></a></p>
    </div>

</div>