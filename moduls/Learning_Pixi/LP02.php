<style type="text/css">

</style>
<div class=titlebar> Learning Pixi </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
	echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
	<div class=contentInBox>


<a href="?LP_Content">Содержание </a>
<br>

<h2>Настройка</h2>

<p>Перед тем, как начать писать код, создать папку для вашего проекта, и запустить веб-сервер в корневой директории проекта. Если вы не используете веб-сервер, Pixi не будет работать.</p>

<p>Далее, вам нужно установить Pixi. Есть два способа это сделать: <strong>простой</strong> способ, с <strong>Git</strong> или с <strong>Gulp и Node</strong>. </p>

<p><a id="installingpixithesimpleway"></a></p>

<h3>Установка Pixi, простой способ</h3>

<p>Получить последнюю версию <code>pixi.min.js</code> файла из <a href="https://github.com/pixijs/pixi.js/tree/master/bin">Pixi's GitHub Repo</a>.</p>

<p>Этот файл все, что вам нужно для того чтобы использовать Pixi. Вы можете игнорировать все остальные файлы в хранилище: <strong>они вам не нужны.</strong></p>

<p>Далее, создать базовую HTML-страницу и используйте <code>&lt;script&gt;</code> тег, чтобы связать <code>pixi.min.js</code> файл, который вы только что загрузили. <code>&lt;script&gt;</code> тег <code>src</code> должен быть расположе по отношению к корневой директории, где находиться веб-сервер. Ваш <code>&lt;script&gt;</code> тег может выглядеть следующим образом:</p>

<div class="highlight highlight-text-html-basic"><pre>&lt;<span class="pl-ent">script</span> <span class="pl-e">src</span>=<span class="pl-s"><span class="pl-pds">"</span>pixi.min.js<span class="pl-pds">"</span></span>&gt;&lt;/<span class="pl-ent">script</span>&gt;</pre></div>

<p>Вот основной вид HTML-страницы, которую можно использовать, чтобы связать Pixi и проверить, что он работает:</p>

<div class="highlight highlight-text-html-basic"><pre>&lt;!doctype html&gt;
&lt;<span class="pl-ent">meta</span> <span class="pl-e">charset</span>=<span class="pl-s"><span class="pl-pds">"</span>utf-8<span class="pl-pds">"</span></span>&gt;
&lt;<span class="pl-ent">title</span>&gt;Hello World&lt;/<span class="pl-ent">title</span>&gt;
&lt;<span class="pl-ent">body</span>&gt;
&lt;<span class="pl-ent">script</span> <span class="pl-e">src</span>=<span class="pl-s"><span class="pl-pds">"</span>pixi.min.js<span class="pl-pds">"</span></span>&gt;&lt;/<span class="pl-ent">script</span>&gt;
<span class="pl-s1">&lt;<span class="pl-ent">script</span>&gt;</span>
<span class="pl-s1"></span>
<span class="pl-s1"><span class="pl-c">//Тест, что Pixi работает</span></span>
<span class="pl-s1"><span class="pl-en">console</span>.<span class="pl-c1">log</span>(<span class="pl-c1">PIXI</span>);</span>
<span class="pl-s1"></span>
<span class="pl-s1">&lt;/<span class="pl-ent">script</span>&gt;</span>
&lt;/<span class="pl-ent">body</span>&gt;</pre></div>

<p>Это <a href="http://stackoverflow.com/questions/9797046/whats-a-valid-html5-document">минимальное количество HTML кода</a> которое вам нужно, чтобы начать создавать проекты с Pixi. Если Pixi правильно подключен, <code>console.log(PIXI)</code> покажет что-то вроде этого в консоли JavaScript веб-браузера:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c1">Object</span> { VERSION<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">"</span>3.0.7<span class="pl-pds">"</span></span> <span class="pl-k">...</span></pre></div>

<p>Если вы видите, это (или нечто подобное), значит все работает правильно.</p>

<p>Теперь вы можете начать работать с Pixi!</p>

<p><a id="installingpixiwithgit"></a></p>

<h3>Установка Pixi с Git</h3>

<p> Вы можете также использовать Git для установки Pixi. (Что такое <strong>git</strong>? Если вы не знаете, <a href="https://github.com/kittykatattack/learningGit">можете узнать здесь</a>.) У него есть некоторые преимущества: вы можете просто запустить <code>git pull origin master</code> из командной строки, чтобы обновленный Pixi до последней версии.  И, если вы думаете, что вы нашли ошибку в Pixi, вы можете исправить ошибку и отправить запрос pull request чтобы добавлен изменения в основной репозиторий.</p>

<p>Для клонирования репозитория Pixi из Git, <code>cd</code> в корневой директории проекта и введите:</p>

<pre><code>git clone git@github.com:pixijs/pixi.js.git</code></pre>

<p>Команда автоматически создаст папку с именем <code>pixi.js</code> и загрузит в нее последнюю версию Pixi.</p>

<p>После установки Pixi, создать базовый HTML-документ, а также использовать <code>&lt;script&gt;</code>  тег, чтобы включить <code>pixi.js</code> Файл из Pixi в <code>bin</code> папки. <code>&lt;script&gt;</code> тег <code>src</code>  должен быть расположен по отношению к корневой директории, где работает ваш веб-сервер. Ваш  <code>&lt;script&gt;</code> тег может выглядеть следующим образом:</p>

<div class="highlight highlight-text-html-basic"><pre>&lt;<span class="pl-ent">script</span> <span class="pl-e">src</span>=<span class="pl-s"><span class="pl-pds">"</span>pixi.js/bin/pixi.js<span class="pl-pds">"</span></span>&gt;&lt;/<span class="pl-ent">script</span>&gt;</pre></div>

<p>(Если хотите, вы можете поделюсить <code>pixi.min.js</code> файл вместо того, что, я предложил в предыдущем разделе. Минимизированный файл может работать немного быстрее, и безусловно, загружаться быстрее. Преимущество по сравнению с использованием не-уменьшеным файлом JS в том, что компилятор думает, что есть ошибка в исходном коде Pixi, он выдаст сообщение об ошибке, которое отображает сомнительный код в читаемом формате. Это полезно, в том случае, когд вы работаете над проект, потому что даже если ошибка не в Pixi, ошибка может дать вам подсказку относительно того, что случилось с вашим собственным кодом.)</p>

<p>In this <strong>Learning Pixi</strong> repository (what you're reading now!) you'll find a folder called <code>examples</code>. Open it and you'll find a file called <code>helloWorld.html</code>. Assuming that the webserver is running in this repository's root directory, this is how the <code>helloWorld.html</code> file correctly links to Pixi and checks that it's working:

В этом <strong>Обучение Pixi</strong>(то, что вы сейчас читаете!) Вы найдете папку с именем <code>examples</code>. Откройте его, и вы найдете файл с именем <code>helloWorld.html</code>. Предполагаю, что веб-сервер работает в корневом каталоге этого репозитория, где находится <code>helloWorld.html</code>, что бы подключить правильно ссылки на Pixi и проверить, что он работает: </ p>
</p>

<div class="highlight highlight-text-html-basic"><pre>&lt;!doctype html&gt;
&lt;<span class="pl-ent">meta</span> <span class="pl-e">charset</span>=<span class="pl-s"><span class="pl-pds">"</span>utf-8<span class="pl-pds">"</span></span>&gt;
&lt;<span class="pl-ent">title</span>&gt;Hello World&lt;/<span class="pl-ent">title</span>&gt;
&lt;<span class="pl-ent">body</span>&gt;
&lt;<span class="pl-ent">script</span> <span class="pl-e">src</span>=<span class="pl-s"><span class="pl-pds">"</span>../pixi.js/bin/pixi.js<span class="pl-pds">"</span></span>&gt;&lt;/<span class="pl-ent">script</span>&gt;
<span class="pl-s1">&lt;<span class="pl-ent">script</span>&gt;</span>
<span class="pl-s1"></span>
<span class="pl-s1"><span class="pl-c">//Тест, что Pixi работает</span></span>
<span class="pl-s1"><span class="pl-en">console</span>.<span class="pl-c1">log</span>(<span class="pl-c1">PIXI</span>);</span>
<span class="pl-s1"></span>
<span class="pl-s1">&lt;/<span class="pl-ent">script</span>&gt;</span>
&lt;/<span class="pl-ent">body</span>&gt;</pre></div>

<p>Если Pixi подключен правильно, <code>console.log(PIXI)</code> покажет что-то вроде этого в консоли JavaScript веб-браузера: </p> 
<pre><code>Object { VERSION: "3.0.7" ...
</code></pre>

<p><a id="installingpixiwithnodeandgulp"></a></p>

<h3>Установка Pixi с Node и Gulp</h3>

<p>Вы можете также установить Pixi с помощью <a href="https://nodejs.org">Node</a> и <a href="http://gulpjs.com">Gulp</a>. Если вам нужно сделать пользовательские сборки Pixi, чтобы включить или исключить определенные функции, вы должны принять это маршрут. <a href="https://github.com/GoodBoyDigital/pixi.js">Смотреть Pixi GitHub</a> для подробной информации. Но, в общем говоря, нет никакой необходимости делать это.</p> 

</div>
<a href="?LP_Content"> Содержание </a>
</div>
