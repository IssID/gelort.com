<style type="text/css">

</style>
<div class=titlebar> Learning Pixi </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
  echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
  <div class=contentInBox>


<a href="?LP_Content">Содержание </a>
<br>

<p><a id="user-content-movingsprites"></a></p>

<h2><a id="user-content-moving-sprites" class="anchor" href="#moving-sprites" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Moving Sprites</h2>

<p>You now know how to display sprites, but how do you make them move?
That's easy: create a looping function using <code>requestAnimationFrame</code>.
This is called a <strong>game loop</strong>.
Any code you put inside the game loop will update 60 times per
second. Here's some code you could write to make the <code>cat</code> sprite move
at a rate  of 1 pixel per frame.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">gameLoop</span>() {

  <span class="pl-c">//Loop this function at 60 frames per second</span>
  <span class="pl-en">requestAnimationFrame</span>(gameLoop);

  <span class="pl-c">//Move the cat 1 pixel to the right each frame</span>
  <span class="pl-smi">cat</span>.<span class="pl-c1">x</span> <span class="pl-k">+=</span> <span class="pl-c1">1</span>;

  <span class="pl-c">//Render the stage to see the animation</span>
  <span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);
}

<span class="pl-c">//Start the game loop</span>
<span class="pl-en">gameLoop</span>();</pre></div>

<p>If you run this bit of code, you'll see the sprite gradually move to
the right side of the stage.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s15.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/15.png" alt="Moving sprites" style="max-width:100%;"></a></p>

<p>And that's really all there is to it! Just change any sprite property by small
increments inside the loop, and they'll animate over time. If you want
the sprite to animate in the opposite direction (to the left), just give it a
negative value, like <code>-1</code>.
You'll find this code in the <code>movingSprites.html</code> file - here's the
complete code:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c">//Aliases</span>
<span class="pl-k">var</span> Container <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">Container</span>,
    autoDetectRenderer <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">autoDetectRenderer</span>,
    loader <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>,
    resources <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>.<span class="pl-smi">resources</span>,
    Sprite <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">Sprite</span>;

<span class="pl-c">//Create a Pixi stage and renderer </span>
<span class="pl-k">var</span> stage <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Container</span>(),
    renderer <span class="pl-k">=</span> <span class="pl-en">autoDetectRenderer</span>(<span class="pl-c1">256</span>, <span class="pl-c1">256</span>);
<span class="pl-c1">document</span>.<span class="pl-c1">body</span>.<span class="pl-c1">appendChild</span>(<span class="pl-smi">renderer</span>.<span class="pl-smi">view</span>);

<span class="pl-c">//Load an image and the run the `setup` function</span>
loader
  .<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>)
  .<span class="pl-c1">load</span>(setup);

<span class="pl-c">//Define any variables that are used in more than one function</span>
<span class="pl-k">var</span> cat;

<span class="pl-k">function</span> <span class="pl-en">setup</span>() {

  <span class="pl-c">//Create the `cat` sprite </span>
  cat <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(resources[<span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>].<span class="pl-smi">texture</span>);
  <span class="pl-smi">cat</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">96</span>; 
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(cat);

  <span class="pl-c">//Start the game loop</span>
  <span class="pl-en">gameLoop</span>();
}

<span class="pl-k">function</span> <span class="pl-en">gameLoop</span>(){

  <span class="pl-c">//Loop this function 60 times per second</span>
  <span class="pl-en">requestAnimationFrame</span>(gameLoop);

  <span class="pl-c">//Move the cat 1 pixel per frame</span>
  <span class="pl-smi">cat</span>.<span class="pl-c1">x</span> <span class="pl-k">+=</span> <span class="pl-c1">1</span>;

  <span class="pl-c">//Render the stage</span>
  <span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);
}</pre></div>

<p>(Notice that the <code>cat</code> variable needs to be defined outside the
<code>setup</code> and
<code>gameLoop</code> functions so that you can access it inside both of them.)</p>

<p>You can animate a sprite's scale, rotation, or size - whatever! You'll see
many more examples of how to animate sprites ahead.</p>
