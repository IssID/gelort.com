<style type="text/css">

</style>
<div class=titlebar> Learning Pixi </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
	echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
	<div class=contentInBox>


<a href="?LP_Content">Содержание </a>
<br>

<p><a id="user-content-hexi"></a></p>

<h3><a id="user-content-hexi" class="anchor" href="#hexi" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Hexi</h3>

<p>Do you want to use all the functionality of those libraries, but don't
want the hassle of integrating them yourself? Use <strong>Hexi</strong>: a complete
development environment for building games and interactive
applications:</p>

<p><a href="https://github.com/kittykatattack/hexi">https://github.com/kittykatattack/hexi</a></p>

<p>It bundles the best version of Pixi (the latest stable one) with all these 
libraries (and more!) for a simple and fun way to make games. Hexi also
lets you access the global <code>PIXI</code> object directly, so you can write
low-level Pixi code directly in a Hexi application, and optionally choose to use as many or
as few of Hexi's extra conveniences as you need.</p>
