<style type="text/css">

</style>
<div class=titlebar> Learning Pixi </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
  echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
  <div class=contentInBox>


<a href="?LP_Content">Содержание </a>
<br>

<p><a id="user-content-creatingsprites"></a></p>

<h2><a id="user-content-creating-sprites-from-a-loaded-texture-atlas" class="anchor" href="#creating-sprites-from-a-loaded-texture-atlas" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Creating sprites from a loaded texture atlas</h2>

<p>Pixi gives you three general ways to create a sprite from a texture atlas:</p>

<ol>
<li> Using <code>TextureCache</code>:</li>
</ol>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> texture <span class="pl-k">=</span> TextureCache[<span class="pl-s"><span class="pl-pds">"</span>frameId.png<span class="pl-pds">"</span></span>],
    sprite <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(texture);</pre></div>

<ol>
<li> If you’ve used Pixi’s <code>loader</code> to load the texture atlas, use the 
loader’s <code>resources</code>:</li>
</ol>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> sprite <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(
  resources[<span class="pl-s"><span class="pl-pds">"</span>images/treasureHunter.json<span class="pl-pds">"</span></span>].<span class="pl-smi">textures</span>[<span class="pl-s"><span class="pl-pds">"</span>frameId.png<span class="pl-pds">"</span></span>]
);</pre></div>

<ol>
<li>That’s way too much to typing to have to do just to create a sprite! 
So I suggest you create an alias called <code>id</code> that points to texture’s 
altas’s <code>textures</code> object, like this:</li>
</ol>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> id <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>.<span class="pl-smi">resources</span>[<span class="pl-s"><span class="pl-pds">"</span>images/treasureHunter.json<span class="pl-pds">"</span></span>].<span class="pl-smi">textures</span>; </pre></div>

<p>Then you can just create each new sprite like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">let</span> sprite <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(id[<span class="pl-s"><span class="pl-pds">"</span>frameId.png<span class="pl-pds">"</span></span>]);</pre></div>

<p>Much better!</p>

<p>Here's how you could use these three different sprite creation
techniques in the <code>setup</code> function to create and display the
<code>dungeon</code>, <code>explorer</code>, and <code>treasure</code> sprites.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c">//Define variables that might be used in more </span>
<span class="pl-c">//than one function</span>
<span class="pl-k">var</span> dungeon, explorer, treasure, door, id;

<span class="pl-k">function</span> <span class="pl-en">setup</span>() {

  <span class="pl-c">//There are 3 ways to make sprites from textures atlas frames</span>

  <span class="pl-c">//1. Access the `TextureCache` directly</span>
  <span class="pl-k">var</span> dungeonTexture <span class="pl-k">=</span> TextureCache[<span class="pl-s"><span class="pl-pds">"</span>dungeon.png<span class="pl-pds">"</span></span>];
  dungeon <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(dungeonTexture);
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(dungeon);

  <span class="pl-c">//2. Access the texture using throuhg the loader's `resources`:</span>
  explorer <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(
    resources[<span class="pl-s"><span class="pl-pds">"</span>images/treasureHunter.json<span class="pl-pds">"</span></span>].<span class="pl-smi">textures</span>[<span class="pl-s"><span class="pl-pds">"</span>explorer.png<span class="pl-pds">"</span></span>]
  );
  <span class="pl-smi">explorer</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">68</span>;

  <span class="pl-c">//Center the explorer vertically</span>
  <span class="pl-smi">explorer</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-smi">stage</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span> <span class="pl-k">-</span> <span class="pl-smi">explorer</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(explorer);

  <span class="pl-c">//3. Create an optional alias called `id` for all the texture atlas </span>
  <span class="pl-c">//frame id textures.</span>
  id <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>.<span class="pl-smi">resources</span>[<span class="pl-s"><span class="pl-pds">"</span>images/treasureHunter.json<span class="pl-pds">"</span></span>].<span class="pl-smi">textures</span>; 

  <span class="pl-c">//Make the treasure box using the alias</span>
  treasure <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(id[<span class="pl-s"><span class="pl-pds">"</span>treasure.png<span class="pl-pds">"</span></span>]);
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(treasure);

  <span class="pl-c">//Position the treasure next to the right edge of the canvas</span>
  <span class="pl-smi">treasure</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-smi">stage</span>.<span class="pl-c1">width</span> <span class="pl-k">-</span> <span class="pl-smi">treasure</span>.<span class="pl-c1">width</span> <span class="pl-k">-</span> <span class="pl-c1">48</span>;
  <span class="pl-smi">treasure</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-smi">stage</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span> <span class="pl-k">-</span> <span class="pl-smi">treasure</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(treasure);


  <span class="pl-c">//Render the stage</span>
  <span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);
}</pre></div>

<p>Here's what this code displays:</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s13.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/13.png" alt="Explorer, dungeon and treasure" style="max-width:100%;"></a></p>

<p>The stage dimensions are 512 by 512 pixels, and you can see in the
code above that the <code>stage.height</code> and <code>stage.width</code> properties are used
to align the sprites. Here's how the <code>explorer</code>'s <code>y</code> position is
vertically centered:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">explorer</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-smi">stage</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span> <span class="pl-k">-</span> <span class="pl-smi">explorer</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;</pre></div>

<p>Learning to create and display sprites using a texture atlas is an
important benchmark. So before we continue, let's take a look at the
code you
could write to add the remaining
sprites: the <code>blob</code>s and <code>exit</code> door, so that you can produce a scene
that looks like this:</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s14.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/14.png" alt="All the texture atlas sprites" style="max-width:100%;"></a></p>

<p>Here's the entire code that does all this. I've also included the HTML
code so you can see everything in its proper context.
(You'll find this working code in the
<code>examples/spriteFromTextureAtlas.html</code> file in this repository.)
Notice that the <code>blob</code> sprites are created and added to the stage in a
loop, and assigned random positions.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">&lt;</span><span class="pl-k">!</span>doctype html<span class="pl-k">&gt;</span>
<span class="pl-k">&lt;</span>meta charset<span class="pl-k">=</span><span class="pl-s"><span class="pl-pds">"</span>utf-8<span class="pl-pds">"</span></span><span class="pl-k">&gt;</span>
<span class="pl-k">&lt;</span>title<span class="pl-k">&gt;</span>Make a sprite from a texture atlas<span class="pl-k">&lt;</span><span class="pl-k">/</span>title<span class="pl-k">&gt;</span>
<span class="pl-k">&lt;</span>body<span class="pl-k">&gt;</span>
<span class="pl-k">&lt;</span>script src<span class="pl-k">=</span><span class="pl-s"><span class="pl-pds">"</span>../pixi.js/bin/pixi.js<span class="pl-pds">"</span></span><span class="pl-k">&gt;&lt;</span><span class="pl-k">/</span>script<span class="pl-k">&gt;</span>
<span class="pl-k">&lt;</span>script<span class="pl-k">&gt;</span>

<span class="pl-c">//Aliases</span>
<span class="pl-k">var</span> Container <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">Container</span>,
    autoDetectRenderer <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">autoDetectRenderer</span>,
    loader <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>,
    resources <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>.<span class="pl-smi">resources</span>,
    TextureCache <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">utils</span>.<span class="pl-smi">TextureCache</span>,
    Texture <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">Texture</span>,
    Sprite <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">Sprite</span>;

<span class="pl-c">//Create a Pixi stage and renderer and add the </span>
<span class="pl-c">//renderer.view to the DOM</span>
<span class="pl-k">var</span> stage <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Container</span>(),
    renderer <span class="pl-k">=</span> <span class="pl-en">autoDetectRenderer</span>(<span class="pl-c1">512</span>, <span class="pl-c1">512</span>);
<span class="pl-c1">document</span>.<span class="pl-c1">body</span>.<span class="pl-c1">appendChild</span>(<span class="pl-smi">renderer</span>.<span class="pl-smi">view</span>);

<span class="pl-c">//load a JSON file and run the `setup` function when it's done</span>
loader
  .<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">"</span>images/treasureHunter.json<span class="pl-pds">"</span></span>)
  .<span class="pl-c1">load</span>(setup);

<span class="pl-c">//Define variables that might be used in more </span>
<span class="pl-c">//than one function</span>
<span class="pl-k">var</span> dungeon, explorer, treasure, door, id;

<span class="pl-k">function</span> <span class="pl-en">setup</span>() {

  <span class="pl-c">//There are 3 ways to make sprites from textures atlas frames</span>

  <span class="pl-c">//1. Access the `TextureCache` directly</span>
  <span class="pl-k">var</span> dungeonTexture <span class="pl-k">=</span> TextureCache[<span class="pl-s"><span class="pl-pds">"</span>dungeon.png<span class="pl-pds">"</span></span>];
  dungeon <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(dungeonTexture);
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(dungeon);

  <span class="pl-c">//2. Access the texture using throuhg the loader's `resources`:</span>
  explorer <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(
    resources[<span class="pl-s"><span class="pl-pds">"</span>images/treasureHunter.json<span class="pl-pds">"</span></span>].<span class="pl-smi">textures</span>[<span class="pl-s"><span class="pl-pds">"</span>explorer.png<span class="pl-pds">"</span></span>]
  );
  <span class="pl-smi">explorer</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">68</span>;

  <span class="pl-c">//Center the explorer vertically</span>
  <span class="pl-smi">explorer</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-smi">stage</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span> <span class="pl-k">-</span> <span class="pl-smi">explorer</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(explorer);

  <span class="pl-c">//3. Create an optional alias called `id` for all the texture atlas </span>
  <span class="pl-c">//frame id textures.</span>
  id <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>.<span class="pl-smi">resources</span>[<span class="pl-s"><span class="pl-pds">"</span>images/treasureHunter.json<span class="pl-pds">"</span></span>].<span class="pl-smi">textures</span>; 

  <span class="pl-c">//Make the treasure box using the alias</span>
  treasure <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(id[<span class="pl-s"><span class="pl-pds">"</span>treasure.png<span class="pl-pds">"</span></span>]);
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(treasure);

  <span class="pl-c">//Position the treasure next to the right edge of the canvas</span>
  <span class="pl-smi">treasure</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-smi">stage</span>.<span class="pl-c1">width</span> <span class="pl-k">-</span> <span class="pl-smi">treasure</span>.<span class="pl-c1">width</span> <span class="pl-k">-</span> <span class="pl-c1">48</span>;
  <span class="pl-smi">treasure</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-smi">stage</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span> <span class="pl-k">-</span> <span class="pl-smi">treasure</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(treasure);

  <span class="pl-c">//Make the exit door</span>
  door <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(id[<span class="pl-s"><span class="pl-pds">"</span>door.png<span class="pl-pds">"</span></span>]); 
  <span class="pl-smi">door</span>.<span class="pl-smi">position</span>.<span class="pl-c1">set</span>(<span class="pl-c1">32</span>, <span class="pl-c1">0</span>);
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(door);

  <span class="pl-c">//Make the blobs</span>
  <span class="pl-k">var</span> numberOfBlobs <span class="pl-k">=</span> <span class="pl-c1">6</span>,
      spacing <span class="pl-k">=</span> <span class="pl-c1">48</span>,
      xOffset <span class="pl-k">=</span> <span class="pl-c1">150</span>;

  <span class="pl-c">//Make as many blobs as there are `numberOfBlobs`</span>
  <span class="pl-k">for</span> (<span class="pl-k">var</span> i <span class="pl-k">=</span> <span class="pl-c1">0</span>; i <span class="pl-k">&lt;</span> numberOfBlobs; i<span class="pl-k">++</span>) {

    <span class="pl-c">//Make a blob</span>
    <span class="pl-k">var</span> blob <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(id[<span class="pl-s"><span class="pl-pds">"</span>blob.png<span class="pl-pds">"</span></span>]);

    <span class="pl-c">//Space each blob horizontally according to the `spacing` value.</span>
    <span class="pl-c">//`xOffset` determines the point from the left of the screen</span>
    <span class="pl-c">//at which the first blob should be added.</span>
    <span class="pl-k">var</span> x <span class="pl-k">=</span> spacing <span class="pl-k">*</span> i <span class="pl-k">+</span> xOffset;

    <span class="pl-c">//Give the blob a random y position</span>
    <span class="pl-c">//(`randomInt` is a custom function - see below)</span>
    <span class="pl-k">var</span> y <span class="pl-k">=</span> <span class="pl-en">randomInt</span>(<span class="pl-c1">0</span>, <span class="pl-smi">stage</span>.<span class="pl-c1">height</span> <span class="pl-k">-</span> <span class="pl-smi">blob</span>.<span class="pl-c1">height</span>);

    <span class="pl-c">//Set the blob's position</span>
    <span class="pl-smi">blob</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> x;
    <span class="pl-smi">blob</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> y;

    <span class="pl-c">//Add the blob sprite to the stage</span>
    <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(blob);
  }

  <span class="pl-c">//Render the stage   </span>
  <span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);
}

<span class="pl-c">//The `randomInt` helper function</span>
<span class="pl-k">function</span> <span class="pl-en">randomInt</span>(<span class="pl-smi">min</span>, <span class="pl-smi">max</span>) {
  <span class="pl-k">return</span> <span class="pl-c1">Math</span>.<span class="pl-c1">floor</span>(<span class="pl-c1">Math</span>.<span class="pl-c1">random</span>() <span class="pl-k">*</span> (max <span class="pl-k">-</span> min <span class="pl-k">+</span> <span class="pl-c1">1</span>)) <span class="pl-k">+</span> min;
}

<span class="pl-k">&lt;</span><span class="pl-k">/</span>script<span class="pl-k">&gt;</span>
<span class="pl-k">&lt;</span><span class="pl-k">/</span>body<span class="pl-k">&gt;</span></pre></div>

<p>You can see in the code above that all the blobs are created using a
<code>for</code> loop. Each <code>blob</code> is spaced evenly along the <code>x</code> axis like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> x <span class="pl-k">=</span> spacing <span class="pl-k">*</span> i <span class="pl-k">+</span> xOffset;
<span class="pl-smi">blob</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> x;</pre></div>

<p><code>spacing</code> has a value 48, and <code>xOffset</code> has a value of 150. What this
means that that the first <code>blob</code> will have an <code>x</code> position of 150.
This offsets it from the left side of the stage by 150 pixel. Each
subsequent <code>blob</code> will have an <code>x</code> value that's 48 pixels greater than
the <code>blob</code> created in the previous iteration of the loop. This creates
an evenly spaced line of blob monsters, from left to right, along the dungeon floor.</p>

<p>Each <code>blob</code> is also given a random <code>y</code> position. Here's the code that
does this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> y <span class="pl-k">=</span> <span class="pl-en">randomInt</span>(<span class="pl-c1">0</span>, <span class="pl-smi">stage</span>.<span class="pl-c1">height</span> <span class="pl-k">-</span> <span class="pl-smi">blob</span>.<span class="pl-c1">height</span>);
<span class="pl-smi">blob</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> y;</pre></div>

<p>The <code>blob</code>'s <code>y</code> position could be assigned any random number between 0 and
512, which is the value of <code>stage.height</code>. This works with the help of
a custom function called <code>randomInt</code>. <code>randomInt</code> returns a random number
that's within a range between any two numbers you supply.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-en">randomInt</span>(lowestNumber, highestNumber)</pre></div>

<p>That means if you want a random number between 1 and 10, you can get
one like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> randomNumber <span class="pl-k">=</span> <span class="pl-en">randomInt</span>(<span class="pl-c1">1</span>, <span class="pl-c1">10</span>);</pre></div>

<p>Here's the <code>randomInt</code> function definition that does all this work:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">randomInt</span>(<span class="pl-smi">min</span>, <span class="pl-smi">max</span>) {
  <span class="pl-k">return</span> <span class="pl-c1">Math</span>.<span class="pl-c1">floor</span>(<span class="pl-c1">Math</span>.<span class="pl-c1">random</span>() <span class="pl-k">*</span> (max <span class="pl-k">-</span> min <span class="pl-k">+</span> <span class="pl-c1">1</span>)) <span class="pl-k">+</span> min;
}</pre></div>

<p><code>randomInt</code> is a great little function to keep in your back pocket for
making games - I use it all the time.</p>
