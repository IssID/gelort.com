<style type="text/css">

</style>
<div class=titlebar> Learning Pixi </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
	echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
	<div class=contentInBox>


<a href="?LP_Content">Содержание </a>
<br>

<p><a id="user-content-grouping"></a></p>

<h2><a id="user-content-grouping-sprites" class="anchor" href="#grouping-sprites" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Grouping Sprites</h2>

<p>Groups let you create game scenes, and manage similar sprites together
as single units. Pixi has an object called a <code>Container</code>
that lets you do this. Let's find out how it works.</p>

<p>Imagine that you want to display three sprites: a cat, hedgehog and
tiger. Create them, and set their positions - <em>but don't add them to the
stage</em>.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c">//The cat</span>
<span class="pl-k">var</span> cat <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(id[<span class="pl-s"><span class="pl-pds">"</span>cat.png<span class="pl-pds">"</span></span>]);
<span class="pl-smi">cat</span>.<span class="pl-smi">position</span>.<span class="pl-c1">set</span>(<span class="pl-c1">16</span>, <span class="pl-c1">16</span>);

<span class="pl-c">//The hedgehog</span>
<span class="pl-k">var</span> hedgehog <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(id[<span class="pl-s"><span class="pl-pds">"</span>hedgehog.png<span class="pl-pds">"</span></span>]);
<span class="pl-smi">hedgehog</span>.<span class="pl-smi">position</span>.<span class="pl-c1">set</span>(<span class="pl-c1">32</span>, <span class="pl-c1">32</span>);

<span class="pl-c">//The tiger</span>
<span class="pl-k">var</span> tiger <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(id[<span class="pl-s"><span class="pl-pds">"</span>tiger.png<span class="pl-pds">"</span></span>]);
<span class="pl-smi">tiger</span>.<span class="pl-smi">position</span>.<span class="pl-c1">set</span>(<span class="pl-c1">64</span>, <span class="pl-c1">64</span>);</pre></div>

<p>Next, create an <code>animals</code> container to group them all together like
this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> animals <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Container</span>();</pre></div>

<p>Then use <code>addChild</code> to <em>add the sprites to the group</em>.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">animals</span>.<span class="pl-en">addChild</span>(cat);
<span class="pl-smi">animals</span>.<span class="pl-en">addChild</span>(hedgehog);
<span class="pl-smi">animals</span>.<span class="pl-en">addChild</span>(tiger);</pre></div>

<p>Finally add the group to the stage.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(animals);
<span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);</pre></div>

<p>(As you know, the <code>stage</code> object is also a <code>Container</code>. It’s the root
container for all Pixi sprites.)</p>

<p>Here's what this code produces:</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s18.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/18.png" alt="Grouping sprites" style="max-width:100%;"></a></p>

<p>What you can't see in that image is the invisible <code>animals</code> group
that's containing the sprites.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s19.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/19.png" alt="Grouping sprites" style="max-width:100%;"></a></p>

<p>You can now treat the <code>animals</code> group as a single unit. You can think
of a <code>Container</code> as a special kind of sprite that doesn’t
have a texture.</p>

<p>If you need a list of all the child sprites that <code>animals</code> contains,
use its <code>children</code> array to find out.</p>

<pre><code>console.log(animals.chidren}
//Displays: Array [Object, Object, Object]
</code></pre>

<p>This tells you that <code>animals</code> has three sprites as children.</p>

<p>Because the <code>animals</code> group is just like any other sprite, you can
change its <code>x</code> and <code>y</code> values, <code>alpha</code>, <code>scale</code> and
all the other sprite properties. Any property value you change on the
parent container will affect the child sprites in a relative way. So if you
set the group's <code>x</code> and <code>y</code> position, all the child sprites will
be repositioned relative to the group's top left corner. What would
happen if you set the <code>animals</code>'s <code>x</code> and <code>y</code> position to 64?</p>

<pre><code>animals.position.set(64, 64);
</code></pre>

<p>The whole group of sprites will move 64 pixels right and 64 pixels to
the down.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s20.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/20.png" alt="Grouping sprites" style="max-width:100%;"></a></p>

<p>The <code>animals</code> group also has its own dimensions, which is based on the area
occupied by the containing sprites. You can find its <code>width</code> and
<code>height</code> values like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-en">console</span>.<span class="pl-c1">log</span>(<span class="pl-smi">animals</span>.<span class="pl-c1">width</span>);
<span class="pl-c">//Displays: 112</span>

<span class="pl-en">console</span>.<span class="pl-c1">log</span>(<span class="pl-smi">animals</span>.<span class="pl-c1">height</span>);
<span class="pl-c">//Displays: 112</span>
</pre></div>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s21.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/21.png" alt="Group width and height" style="max-width:100%;"></a></p>

<p>What happens if you change a group's width or height?</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">animals</span>.<span class="pl-c1">width</span> <span class="pl-k">=</span> <span class="pl-c1">200</span>;
<span class="pl-smi">animals</span>.<span class="pl-c1">height</span> <span class="pl-k">=</span> <span class="pl-c1">200</span>;</pre></div>

<p>All the child
sprites will scale to match that change.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s22.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/22.png" alt="Group width and height" style="max-width:100%;"></a></p>

<p>You can nest as many <code>Container</code>s inside other
<code>Container</code>s as you like, to create deep hierarchies if
you need to. However, a <code>DisplayObject</code> (like a <code>Sprite</code> or another
<code>Container</code>) can only belong to one parent at a time. If
you use <code>addChild</code> to make a sprite the child of another object, Pixi
will automatically remove it from its current parent. That’s a useful
bit of management that you don’t have to worry about.</p>

<p><a id="user-content-localnglobal"></a></p>

<h3><a id="user-content-local-and-global-positions" class="anchor" href="#local-and-global-positions" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Local and global positions</h3>

<p>When you add a sprite to a <code>Container</code>, its <code>x</code> and <code>y</code>
position is <em>relative to the group’s top left corner</em>. That's the
sprite's <strong>local position</strong> For example, what do you think the cat's
position is in this image?</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s20.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/20.png" alt="Grouping sprites" style="max-width:100%;"></a></p>

<p>Let's find out:</p>

<pre><code>console.log(cat.x);
//Displays: 16
</code></pre>

<p>16? Yes! That's because the cat is offset by only 16 pixel's from the
group's top left corner. 16 is the cat's local position.</p>

<p>Sprites also have a <strong>global position</strong>. The global position is the
distance from the top left corner of the stage, to the sprite's anchor
point (usually the sprite's top left corner.) You can find a sprite's global
position with the help of the <code>toGlobal</code> method.  Here's how:</p>

<pre><code>parentSprite.toGlobal(childSprite.position)
</code></pre>

<p>That means you can find the cat's global position inside the <code>animals</code>
group like this:</p>

<pre><code>console.log(animals.toGlobal(cat.position));
//Displays: Object {x: 80, y: 80...};
</code></pre>

<p>That gives you an <code>x</code> and <code>y</code> position of 80. That's exactly the cat's
global position relative to the top left corner of the stage. </p>

<p>What if you want to find the global position of a sprite, but don't
know what the sprite's parent container
is? Every sprite has a property called <code>parent</code> that will tell you what the
sprite's parent is. If you add a sprite directly to the <code>stage</code>, then
<code>stage</code> will be the sprite's parent. In the example above, the <code>cat</code>'s
parent is <code>animals</code>. That means you can alternatively get the cat's global position
by writing code like this:</p>

<pre><code>cat.parent.toGlobal(cat.position);
</code></pre>

<p>And it will work even if you don't know what the cat's parent
container currently is.</p>

<p>There's one more way to calculate the global position! And, it's
actually the best way, so heads up! If you want to know the distance
from the top left corner of the canvas to the sprite, and don't know
or care what the sprite's parent containers are, use the
<code>getGlobalPosition</code> method. Here's how to use it to find the tiger's global position:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">tiger</span>.<span class="pl-en">getGlobalPosition</span>().<span class="pl-c1">x</span>
<span class="pl-smi">tiger</span>.<span class="pl-en">getGlobalPosition</span>().<span class="pl-c1">y</span></pre></div>

<p>This will give you <code>x</code> and <code>y</code> values of 128 in the example that we've
been using.
The special thing about <code>getGlobalPosition</code> is that it's highly
precise: it will give you the sprite's accurate global position as
soon as its local position changes. I asked the Pixi development team
to add this feature specifically for accurate collision detection for
games.</p>

<p>What if you want to convert a global position to a local position? you
can use the <code>toLocal</code> method. It works in a similar way, but uses this
general format:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">sprite</span>.<span class="pl-en">toLocal</span>(<span class="pl-smi">sprite</span>.<span class="pl-smi">position</span>, anyOtherSprite)</pre></div>

<p>Use <code>toLocal</code> to find the distance between a sprite and any other
sprite. Here's how you could find out the tiger's local
position, relative to the hedgehog.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">tiger</span>.<span class="pl-en">toLocal</span>(<span class="pl-smi">tiger</span>.<span class="pl-smi">position</span>, hedgehog).<span class="pl-c1">x</span>
<span class="pl-smi">tiger</span>.<span class="pl-en">toLocal</span>(<span class="pl-smi">tiger</span>.<span class="pl-smi">position</span>, hedgehog).<span class="pl-c1">y</span></pre></div>

<p>This gives you an <code>x</code> value of 32 and a <code>y</code> value of 32. You can see
in the example images that the tiger's top left corner is 32 pixels
down and to the left of the hedgehog's top left corner.</p>

<p><a id="user-content-spritebatch"></a></p>

<h3><a id="user-content-using-a-particlecontainer-to-group-sprites" class="anchor" href="#using-a-particlecontainer-to-group-sprites" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Using a ParticleContainer to group sprites</h3>

<p>Pixi has an alternative, high-performance way to group sprites called
a <code>ParticleContainer</code> (<code>PIXI.ParticleContainer</code>). Any sprites inside a
<code>ParticleContainer</code> will render 2 to 5
times faster than they would if they were in a regular
<code>Container</code>. It’s a great performance boost for games.</p>

<p>Create a <code>ParticleContainer</code> like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> superFastSprites <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">ParticleContainer</span>();</pre></div>

<p>Then use <code>addChild</code> to add sprites to it, just like you would with any
ordinary <code>Container</code>.</p>

<p>You have to make some compromises if you decide to use a
<code>ParticleContainer</code>. Sprites inside a <code>ParticleContainer</code> only have a few  basic properties:
<code>x</code>, <code>y</code>, <code>width</code>, <code>height</code>, <code>scale</code>, <code>pivot</code>, <code>alpha</code>, <code>visible</code> – and that’s
about it. Also, the sprites that it contains can’t have nested
children of their own. A <code>ParticleContainer</code> also can’t use Pixi’s advanced
visual effects like filters and blend modes. But for the huge performance boost that you get, those
compromises are usually worth it. And you can use
<code>Container</code>s and <code>ParticleContainer</code>s simultaneously in the same project, so you can fine-tune your optimization.</p>

<p>Why are sprites in a <code>Particle Container</code> so fast? Because the positions of
the sprites are being calculated directly on the GPU. The Pixi
development team is working to offload as much sprite processing as
possible on the GPU, so it’s likely that the latest version of Pixi
that you’re using will have much more feature-rich <code>ParticleContainer</code> than
what I've described here. Check the current <a href="http://pixijs.github.io/docs/PIXI.ParticleContainer.html"><code>ParticleContainer</code>
documentation</a> for details.</p>

<p>Where you create a <code>ParticleContainer</code>, there are two optional
arguments you can provide: the maximum number of sprites the container
can hold, and an options object.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> superFastSprites <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">ParticleContainer</span>(size, options);</pre></div>

<p>The default value for size is 15,000. So, if you need to contain more
sprites, set it to a higher number. The options argument is an object
with 5 Boolean values you can set: <code>scale</code>, <code>position</code>, <code>rotation</code>, <code>uvs</code> and
<code>alpha</code>. The default value of <code>position</code> is <code>true</code>, but all the others
are set to <code>false</code>. That means that if you want change the <code>rotation</code>,
<code>scale</code>, <code>alpha</code>, or <code>uvs</code> of sprite in the <code>ParticleContainer</code>, you
have to set those properties to <code>true</code>, like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> superFastSprites <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">ParticleContainer</span>(
  size, 
  {
    rotation<span class="pl-k">:</span> <span class="pl-c1">true</span>,
    alpha<span class="pl-k">:</span> <span class="pl-c1">true</span>,
    scale<span class="pl-k">:</span> <span class="pl-c1">true</span>,
    uvs<span class="pl-k">:</span> <span class="pl-c1">true</span>
  }
);</pre></div>

<p>But, if you don't think you'll need to use these properties, keep them
set to <code>false</code> to squeeze out the maximum amount of performance.</p>

<p>What's the <code>uvs</code> option? Only set it to <code>true</code> if you have particles
which change their textures while they're being animated. (All the
sprite's textures will also need to be on the same tileset image for
this to work.) </p>

<p>(Note: <strong>UV mapping</strong> is a 3D graphics display term that refers to
the <code>x</code> and <code>y</code> coordinates of the texture (the image) that is being
mapped onto a 3D surface. <code>U</code> is the <code>x</code> axis and <code>V</code> is the <code>y</code> axis.
WebGL already uses <code>x</code>, <code>y</code> and <code>z</code> for 3D spatial positioning, so <code>U</code>
and <code>V</code> were chosen to represent <code>x</code> and <code>y</code> for 2D image textures.)</p>
