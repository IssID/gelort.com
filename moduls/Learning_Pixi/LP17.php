<style type="text/css">

</style>
<div class=titlebar> Learning Pixi </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
  echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
  <div class=contentInBox>


<a href="?LP_Content">Содержание </a>
<br>

<p><a id="user-content-keyboard"></a></p>

<h2><a id="user-content-keyboard-movement" class="anchor" href="#keyboard-movement" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Keyboard Movement</h2>

<p>With just a little more work you can build a simple system to control
a sprite using the keyboard. To simplify your code, I suggest you use
this custom function called <code>keyboard</code> that listens for and captures
keyboard events.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">keyboard</span>(<span class="pl-smi">keyCode</span>) {
  <span class="pl-k">var</span> key <span class="pl-k">=</span> {};
  <span class="pl-smi">key</span>.<span class="pl-c1">code</span> <span class="pl-k">=</span> keyCode;
  <span class="pl-smi">key</span>.<span class="pl-smi">isDown</span> <span class="pl-k">=</span> <span class="pl-c1">false</span>;
  <span class="pl-smi">key</span>.<span class="pl-smi">isUp</span> <span class="pl-k">=</span> <span class="pl-c1">true</span>;
  <span class="pl-smi">key</span>.<span class="pl-smi">press</span> <span class="pl-k">=</span> <span class="pl-c1">undefined</span>;
  <span class="pl-smi">key</span>.<span class="pl-smi">release</span> <span class="pl-k">=</span> <span class="pl-c1">undefined</span>;
  <span class="pl-c">//The `downHandler`</span>
  <span class="pl-smi">key</span>.<span class="pl-en">downHandler</span> <span class="pl-k">=</span> <span class="pl-k">function</span>(<span class="pl-c1">event</span>) {
    <span class="pl-k">if</span> (<span class="pl-c1">event</span>.<span class="pl-smi">keyCode</span> <span class="pl-k">===</span> <span class="pl-smi">key</span>.<span class="pl-c1">code</span>) {
      <span class="pl-k">if</span> (<span class="pl-smi">key</span>.<span class="pl-smi">isUp</span> <span class="pl-k">&amp;&amp;</span> <span class="pl-smi">key</span>.<span class="pl-smi">press</span>) <span class="pl-smi">key</span>.<span class="pl-en">press</span>();
      <span class="pl-smi">key</span>.<span class="pl-smi">isDown</span> <span class="pl-k">=</span> <span class="pl-c1">true</span>;
      <span class="pl-smi">key</span>.<span class="pl-smi">isUp</span> <span class="pl-k">=</span> <span class="pl-c1">false</span>;
    }
    <span class="pl-c1">event</span>.<span class="pl-c1">preventDefault</span>();
  };

  <span class="pl-c">//The `upHandler`</span>
  <span class="pl-smi">key</span>.<span class="pl-en">upHandler</span> <span class="pl-k">=</span> <span class="pl-k">function</span>(<span class="pl-c1">event</span>) {
    <span class="pl-k">if</span> (<span class="pl-c1">event</span>.<span class="pl-smi">keyCode</span> <span class="pl-k">===</span> <span class="pl-smi">key</span>.<span class="pl-c1">code</span>) {
      <span class="pl-k">if</span> (<span class="pl-smi">key</span>.<span class="pl-smi">isDown</span> <span class="pl-k">&amp;&amp;</span> <span class="pl-smi">key</span>.<span class="pl-smi">release</span>) <span class="pl-smi">key</span>.<span class="pl-en">release</span>();
      <span class="pl-smi">key</span>.<span class="pl-smi">isDown</span> <span class="pl-k">=</span> <span class="pl-c1">false</span>;
      <span class="pl-smi">key</span>.<span class="pl-smi">isUp</span> <span class="pl-k">=</span> <span class="pl-c1">true</span>;
    }
    <span class="pl-c1">event</span>.<span class="pl-c1">preventDefault</span>();
  };

  <span class="pl-c">//Attach event listeners</span>
  <span class="pl-c1">window</span>.<span class="pl-c1">addEventListener</span>(
    <span class="pl-s"><span class="pl-pds">"</span>keydown<span class="pl-pds">"</span></span>, <span class="pl-smi">key</span>.<span class="pl-smi">downHandler</span>.<span class="pl-en">bind</span>(key), <span class="pl-c1">false</span>
  );
  <span class="pl-c1">window</span>.<span class="pl-c1">addEventListener</span>(
    <span class="pl-s"><span class="pl-pds">"</span>keyup<span class="pl-pds">"</span></span>, <span class="pl-smi">key</span>.<span class="pl-smi">upHandler</span>.<span class="pl-en">bind</span>(key), <span class="pl-c1">false</span>
  );
  <span class="pl-k">return</span> key;
}</pre></div>

<p>The <code>keyboard</code> function is easy to use. Create a new keyboard object like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> keyObject <span class="pl-k">=</span> <span class="pl-en">keyboard</span>(asciiKeyCodeNumber);</pre></div>

<p>It's one argument is the ASCII key code number of the keyboad key that you
want to listen for.
<a href="http://help.adobe.com/en_US/AS2LCR/Flash_10.0/help.html?content=00000520.html">Here's a list of ASCII keyboard code
numbers</a>.</p>

<p>Then assign <code>press</code> and <code>release</code> methods to the keyboard object like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">keyObject</span>.<span class="pl-en">press</span> <span class="pl-k">=</span> <span class="pl-k">function</span>() {
  <span class="pl-c">//key object pressed</span>
};
<span class="pl-smi">keyObject</span>.<span class="pl-en">release</span> <span class="pl-k">=</span> <span class="pl-k">function</span>() {
  <span class="pl-c">//key object released</span>
};</pre></div>

<p>Keyboard objects also have <code>isDown</code> and <code>isUp</code> Boolean properties that
you can use to check the state of each key.</p>

<p>Take a look at the
<code>keyboardMovement.html</code> file in the <code>examples</code> folder to see how you
can use this <code>keyboard</code> function to control a sprite using your
keyboard's arrow keys. Run it and use the left, up, down, and right
arrow keys to move the cat around the stage.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s17.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/17.png" alt="Keyboard movement" style="max-width:100%;"></a></p>

<p>Here's the code that does all this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">setup</span>() {

  <span class="pl-c">//Create the `cat` sprite</span>
  cat <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(<span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>);
  <span class="pl-smi">cat</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">96</span>;
  <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
  <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(cat);

  <span class="pl-c">//Capture the keyboard arrow keys</span>
  <span class="pl-k">var</span> left <span class="pl-k">=</span> <span class="pl-en">keyboard</span>(<span class="pl-c1">37</span>),
      up <span class="pl-k">=</span> <span class="pl-en">keyboard</span>(<span class="pl-c1">38</span>),
      right <span class="pl-k">=</span> <span class="pl-en">keyboard</span>(<span class="pl-c1">39</span>),
      down <span class="pl-k">=</span> <span class="pl-en">keyboard</span>(<span class="pl-c1">40</span>);

  <span class="pl-c">//Left arrow key `press` method</span>
  <span class="pl-smi">left</span>.<span class="pl-en">press</span> <span class="pl-k">=</span> <span class="pl-k">function</span>() {

    <span class="pl-c">//Change the cat's velocity when the key is pressed</span>
    <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">=</span> <span class="pl-k">-</span><span class="pl-c1">5</span>;
    <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
  };

  <span class="pl-c">//Left arrow key `release` method</span>
  <span class="pl-smi">left</span>.<span class="pl-en">release</span> <span class="pl-k">=</span> <span class="pl-k">function</span>() {

    <span class="pl-c">//If the left arrow has been released, and the right arrow isn't down,</span>
    <span class="pl-c">//and the cat isn't moving vertically:</span>
    <span class="pl-c">//Stop the cat</span>
    <span class="pl-k">if</span> (<span class="pl-k">!</span><span class="pl-smi">right</span>.<span class="pl-smi">isDown</span> <span class="pl-k">&amp;&amp;</span> <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span> <span class="pl-k">===</span> <span class="pl-c1">0</span>) {
      <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
    }
  };

  <span class="pl-c">//Up</span>
  <span class="pl-smi">up</span>.<span class="pl-en">press</span> <span class="pl-k">=</span> <span class="pl-k">function</span>() {
    <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span> <span class="pl-k">=</span> <span class="pl-k">-</span><span class="pl-c1">5</span>;
    <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
  };
  <span class="pl-smi">up</span>.<span class="pl-en">release</span> <span class="pl-k">=</span> <span class="pl-k">function</span>() {
    <span class="pl-k">if</span> (<span class="pl-k">!</span><span class="pl-smi">down</span>.<span class="pl-smi">isDown</span> <span class="pl-k">&amp;&amp;</span> <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">===</span> <span class="pl-c1">0</span>) {
      <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
    }
  };

  <span class="pl-c">//Right</span>
  <span class="pl-smi">right</span>.<span class="pl-en">press</span> <span class="pl-k">=</span> <span class="pl-k">function</span>() {
    <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">=</span> <span class="pl-c1">5</span>;
    <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
  };
  <span class="pl-smi">right</span>.<span class="pl-en">release</span> <span class="pl-k">=</span> <span class="pl-k">function</span>() {
    <span class="pl-k">if</span> (<span class="pl-k">!</span><span class="pl-smi">left</span>.<span class="pl-smi">isDown</span> <span class="pl-k">&amp;&amp;</span> <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span> <span class="pl-k">===</span> <span class="pl-c1">0</span>) {
      <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
    }
  };

  <span class="pl-c">//Down</span>
  <span class="pl-smi">down</span>.<span class="pl-en">press</span> <span class="pl-k">=</span> <span class="pl-k">function</span>() {
    <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span> <span class="pl-k">=</span> <span class="pl-c1">5</span>;
    <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
  };
  <span class="pl-smi">down</span>.<span class="pl-en">release</span> <span class="pl-k">=</span> <span class="pl-k">function</span>() {
    <span class="pl-k">if</span> (<span class="pl-k">!</span><span class="pl-smi">up</span>.<span class="pl-smi">isDown</span> <span class="pl-k">&amp;&amp;</span> <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">===</span> <span class="pl-c1">0</span>) {
      <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
    }
  };

  <span class="pl-c">//Set the game state</span>
  state <span class="pl-k">=</span> play;

  <span class="pl-c">//Start the game loop</span>
  <span class="pl-en">gameLoop</span>();
}

<span class="pl-k">function</span> <span class="pl-en">gameLoop</span>() {
  <span class="pl-en">requestAnimationFrame</span>(gameLoop);
  <span class="pl-en">state</span>();
  <span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);
}

<span class="pl-k">function</span> <span class="pl-en">play</span>() {

  <span class="pl-c">//Use the cat's velocity to make it move</span>
  <span class="pl-smi">cat</span>.<span class="pl-c1">x</span> <span class="pl-k">+=</span> <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span>;
  <span class="pl-smi">cat</span>.<span class="pl-c1">y</span> <span class="pl-k">+=</span> <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span>
}</pre></div>