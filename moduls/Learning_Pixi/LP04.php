<style type="text/css">

</style>
<div class=titlebar> Learning Pixi </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
	echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
	<div class=contentInBox>


<a href="?LP_Content">Содержание </a>
<br>

<p><a id="user-content-sprites"></a></p>

<h2><a id="user-content-pixi-sprites" class="anchor" href="#pixi-sprites" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Pixi sprites</h2>

<p>В предыдущем разделе вы узнали, как создать <code>stage</code> объект, как это:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> stage <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.Container</span>();</pre></div>

<p><code>stage</code> является Pixi <code>Container</code> объектом. Вы можете думать о контейнере как своего рода пустой ящик, который будет группироваться и хранить все, что вы положили в него. <code>stage</code> объект, который мы создали является корневой контейнер для всех видимых вещей в вашей сцене. Pixi требует, чтобы у вас есть один объект корневого контейнера, так как <code>renderer</code> нужно что-то, чтобы сделать:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);</pre></div>

<p>Все, что вы положили внутри <code>stage</code> будет оказано на холсте. Прямо сейчас <code>stage</code> пуст, но скоро мы начнем складывать вещи внутри него.</p>

<p>(Note: You can give your root container any name you like. Call it <code>scene</code> or <code>root</code> if you prefer. The name <code>stage</code> is just an old but useful convention, and one we'll be sticking to in this tutorial.)</p>

<p>So what do you put on the stage? Special image objects called <strong>sprites</strong>. Sprites are basically just images that you can control with code. You can control their position, size, and a host of other properties that are useful for making interactive and animated graphics. Learning to make and control sprites is really the most important thing about learning to use Pixi. If you know how to make sprites and add them to the stage, you're just a small step away from starting to make games.</p>

<p>Pixi has a <code>Sprite</code> class that is a versatile way to make game sprites. There are three main ways to create them:</p>

<ul>
<li>From a single image file.</li>
<li>From a sub-image on a <strong>tileset</strong>. A tileset is a single, big image that
includes all the images you'll need in your game.</li>
<li>From a <strong>texture atlas</strong> (A JSON file that defines the size and position of an image on a tileset.)</li>
</ul>

<p>You’re going to learn all three ways, but, before you do, let’s find out what you need to know about images before you can display them with Pixi.</p>

</div>
<a href="?LP_Content"> Содержание </a>
</div>
