
<div class=titlebar> Learning Pixi </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
	echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
	<div class=contentInBox>


<p>Перед тем как начать, хочу попросить тебя. Если ты собираешся копировать эту статью, будь вежлив, не забудь указать ссылку на оригинал как английской так и русской версии. 
</p> 
https://github.com/kittykatattack/learningPixi <br>
http://gelort.com/?LP_Content <br>

<article class="markdown-body entry-content" itemprop="text"><h1>Изучение Pixi</h1>

<p>Введение шаг за шагом к созданию игр и интерактивных медиа с <a href="https://github.com/pixijs/pixi.js">Pixi rendering engine</a>. <strong>Версия Pixi v4.0</strong>. Если вам понравилась это руководство, <a href="http://www.springer.com/us/book/9781484210956">вам понравиться и книга!</a></p>
<h3>Содержание:</h3>

<ol>
<li><a href="?LP01">Введение</a></li>
<li><a href="?LP02">Настройка</a>

<ol>
<li><a href="?LP02#installingpixithesimpleway">Установка Pixi простой способ</a></li>
<li><a href="?LP02#installingpixiwithgit">Установка Pixi с помощью Git</a></li>
<li><a href="?LP02#installingpixiwithnodeandgulp">Установка Pixi с помощью Node и Gulp</a></li>
</ol></li>
<li><a href="?LP03">Создание сцены и визуализации</a></li><strike>
<li><a href="?LP04">Pixi спрайты</a></li>
<li><a href="?LP05">Загрузка изображений в кэш текстур</a></li>
<li><a href="?LP06">Отображение спрайтов</a>

<ol>
<li><a href="?LP06#usingaliases">Использование псевдонимов</a></li>
<li><a href="?LP06#alittlemoreaboutloadingthings">Чуть больше о загрузке вещей</a>
<li><a href="?LP06#makeaspritefromanordinaryjavascriptimageobject">Сделать спрайт из обычного JavaScript Image object или Canvas</a>
<li><a href="?LP06#assigninganametoaloadingfile">Присвоения имени загруженного файла</a>
<li><a href="?LP06#monitoringloadprogress">Мониторинг прогресса загрузки</a>
<li><a href="?LP06#moreaboutpixisloader">Больше о Pixi's loader</a></li>
</ol></li>
<li><a href="?LP07">Позиционирования спрайтов</a></li>
<li><a href="?LP08">Размер и масштаб</a></li>
<li><a href="?LP09">Вращение (Rotation)</a></li>
<li><a href="?LP10">Сделать спрайт из суб-изображения</a></li>
<li><a href="?LP11">С помощью текстурного атласа</a></li>
<li><a href="?LP12">Загрузки текстурного атласа</a></li>
<li><a href="?LP13">Создание спрайтов из загруженной текстуры атлас</a></li>
<li><a href="?LP14">Перемещение Спрайтов</a></li>
<li><a href="?LP15">Используя свойства скорость</a></li>
<li><a href="?LP16">Игровые состояния (Game states)</a></li>
<li><a href="?LP17">Клавиатура и Движение</a></li>
<li><a href="?LP18">Группировка спрайтов</a>

<ol>
<li><a href="?LP18#localnglobal">Локальные и глобальные позиции</a></li>
<li><a href="?LP18#spritebatch">Используя ParticleContainer в группу спрайтов</a></li>
</ol></li>
<li><a href="?LP18">Pixi's Графические Примитивы</a>

<ol>
<li><a href="?LP19#rectangle">Прямоугольник (Rectangle)</a></li>
<li><a href="?LP19#circles">Круги (Circles)</a></li>
<li><a href="?LP19#ellipses">Эллипс (Ellipses)</a></li>
<li><a href="?LP19#roundedrects">Прямоугольники со скругленными углами</a></li>
<li><a href="?LP19#lines">Линии (Lines)</a></li>
<li><a href="?LP19#polygons">Полигоны (Polygons)</a></li>
</ol></li>
<li><a href="?LP20">Отображение текста</a></li>
<li><a href="?LP21">Обнаружение столкновений</a>

<ol>
<li><a href="/kittykatattack/learningPixi/blob/master/hittest">Функция hitTestRectangle</a></li>
</ol></li>
<li><a href="?LP22">Пример: Охотник за сокровищами</a>

<ol>
<li><a href="?LP22#initialize">Инициализация игры в функция установки</a>

<ol>
<li><a href="?LP22#gamescene">Создание игровых сцен</a></li>
<li><a href="?LP22#makingdungon">Делая подземелье, дверь, проводник и сокровище</a></li>
<li><a href="?LP22#makingblob">Создаем капля (blob) монстров</a></li>
<li><a href="?LP22#healthbar">Создаем здоровья (health bar)</a></li>
<li><a href="?LP22#message">Создаем текст сообщения</a></li>
</ol></li>
<li><a href="?LP22#playing">Играя в игры (Playing the game)</a></li>
<li><a href="?LP22#movingexplorer">Перемещение исследователя (Moving the explorer)</a>

<ol>
<li><a href="?LP22#containingmovement">Containing movement</a></li>
</ol></li>
<li><a href="?LP22#movingmonsters">Перемещение монстров</a></li>
<li><a href="?LP22#checkingcollisions">Проверка на столкновения</a></li>
<li><a href="?LP22#reachingexit">Дойти до двери выхода и закончить игру</a></li>
</ol></li>
<li><a href="?LP23">Подробнее о спрайтов</a></li>
<li><a href="?LP24">Принимая его дальше (Taking it further)</a><br>
<li><a href="?LP25">Hexi</a><br></li>
<li><a href="?LP26">Supporting this project</a></li>
</ol>
</strike>
<p><a id="user-content-introduction"></a></p>



	</div>
</div>
