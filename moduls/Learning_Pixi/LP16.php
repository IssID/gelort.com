<style type="text/css">

</style>
<div class=titlebar> Learning Pixi </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
  echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
  <div class=contentInBox>


<a href="?LP_Content">Содержание </a>
<br>

<p><a id="user-content-gamestates"></a></p>

<h2><a id="user-content-game-states" class="anchor" href="#game-states" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Game states</h2>

<p>As a matter of style, and to help modularize your code, I
recommend structuring your game loop like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c">//Set the game's current state to `play`:</span>
<span class="pl-k">var</span> state <span class="pl-k">=</span> play;

<span class="pl-k">function</span> <span class="pl-en">gameLoop</span>() {

  <span class="pl-c">//Loop this function at 60 frames per second</span>
  <span class="pl-en">requestAnimationFrame</span>(gameLoop);

  <span class="pl-c">//Update the current game state:</span>
  <span class="pl-en">state</span>();

  <span class="pl-c">//Render the stage to see the animation</span>
  <span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);
}

<span class="pl-k">function</span> <span class="pl-en">play</span>() {

  <span class="pl-c">//Move the cat 1 pixel to the right each frame</span>
  <span class="pl-smi">cat</span>.<span class="pl-c1">x</span> <span class="pl-k">+=</span> <span class="pl-c1">1</span>;
}</pre></div>

<p>You can see that the <code>gameLoop</code> is calling a function called <code>state</code> 60 times
per second. What is the <code>state</code> function? It's been assigned to
<code>play</code>. That means all the code in the <code>play</code> function will also run at 60
times per second.</p>

<p>Here's how the code from the previous example can be re-factored to
this new model:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c">//Define any variables that are used in more than one function</span>
<span class="pl-k">var</span> cat, state;

<span class="pl-k">function</span> <span class="pl-en">setup</span>() {

  <span class="pl-c">//Create the `cat` sprite </span>
  cat <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(resources[<span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>].<span class="pl-smi">texture</span>);
  <span class="pl-smi">cat</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">96</span>; 
  <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
  <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(cat);

  <span class="pl-c">//Set the game state</span>
  state <span class="pl-k">=</span> play;

  <span class="pl-c">//Start the game loop</span>
  <span class="pl-en">gameLoop</span>();
}

<span class="pl-k">function</span> <span class="pl-en">gameLoop</span>(){

  <span class="pl-c">//Loop this function 60 times per second</span>
  <span class="pl-en">requestAnimationFrame</span>(gameLoop);

  <span class="pl-c">//Update the current game state:</span>
  <span class="pl-en">state</span>();

  <span class="pl-c">//Render the stage</span>
  <span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);
}

<span class="pl-k">function</span> <span class="pl-en">play</span>() {

  <span class="pl-c">//Move the cat 1 pixel to the right each frame</span>
  <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">=</span> <span class="pl-c1">1</span>
  <span class="pl-smi">cat</span>.<span class="pl-c1">x</span> <span class="pl-k">+=</span> <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span>;
}</pre></div>

<p>Yes, I know, this is a bit of <a href="http://www.amazon.com/Electric-Psychedelic-Sitar-Headswirlers-1-5/dp/B004HZ14VS">head-swirler</a>! But, don't let it scare
and you and spend a minute or two walking through in your mind how those
functions are connected. As you'll see ahead, structuring your game
loop like this will make it much, much easier to do things like switching
game scenes and levels.</p>