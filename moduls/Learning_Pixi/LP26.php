<style type="text/css">

</style>
<div class=titlebar> Learning Pixi </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
	echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
	<div class=contentInBox>


<a href="?LP_Content">Содержание </a>
<br>

<p><a id="user-content-supportingthisproject"></a></p>

<h2><a id="user-content-please-help-to-support-this-project" class="anchor" href="#please-help-to-support-this-project" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Please help to support this project!</h2>

<p>Buy the book! Incredibly, someone actually paid me to finish writing this tutorial
and turn it into a book! </p>

<p><a href="http://www.springer.com/us/book/9781484210956">Learn PixiJS</a></p>

<p>(And it's not just some junky "e-book", but a real, heavy, paper book, published by Springer,
the world's largest publisher! That means you can invite your friends
over, set it on fire, and roast marshmallows!!) There's 80% more
content than what's in this tutorial, and it's
packed full of all the essential techniques you need to know to use
Pixi to make all kinds of interactive applications and games.</p>

<p>Find out how to:</p>

<ul>
<li>Make animated game characters.</li>
<li>Create a full-featured animation state player.</li>
<li>Dynamically animate lines and shapes.</li>
<li>Use tiling sprites for infinite parallax scrolling.</li>
<li>Use blend modes, filters, tinting, masks, video, and render textures.</li>
<li>Produce content for multiple resolutions.</li>
<li>Create interactive buttons.</li>
<li>Create a flexible drag and drop interface for Pixi.</li>
<li>Create particle effects.</li>
<li>Build a stable software architectural model that will scale to any size.</li>
<li>Make complete games.</li>
</ul>

<p>And, as a bonus, all the code is written entirely in the latest version of
JavaScript: ES6/2015.</p>

<p>If you want to support this project, please buy a copy of this book,
and buy another copy for your mom!</p>
</article>