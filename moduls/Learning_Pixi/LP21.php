<style type="text/css">

</style>
<div class=titlebar> Learning Pixi </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
  echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
  <div class=contentInBox>


<a href="?LP_Content">Содержание </a>
<br>

<p><a id="user-content-collision"></a></p>

<h2><a id="user-content-collision-detection" class="anchor" href="#collision-detection" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Collision detection</h2>

<p>You now know how to make a huge variety of graphics objects, but what
can you do with them? A fun thing to do is to build a simple <strong>collision
detection</strong> system. You can use a custom function called
<code>hitTestRectangle</code> that checks whether any two rectangular Pixi sprites are
touching.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-en">hitTestRectangle</span>(spriteOne, spriteTwo)</pre></div>

<p>if they overlap, <code>hitTestRectangle</code> will return <code>true</code>. You can use <code>hitTestRectangle</code> with an <code>if</code> statement to check for a collision between two sprites like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">if</span> (<span class="pl-en">hitTestRectangle</span>(cat, box)) {
  <span class="pl-c">//There's a collision</span>
} <span class="pl-k">else</span> {
  <span class="pl-c">//There's no collision</span>
}</pre></div>

<p>As you'll see, <code>hitTestRectangle</code> is the front door into the vast universe of game design.</p>

<p>Run the <code>collisionDetection.html</code> file in the <code>examples</code> folder for a
working example of how to use <code>hitTestRectangle</code>. Use the arrow keys
to move the cat. If the cat hits the box, the box becomes red
and "Hit!" is displayed by the text object.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s25.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/25.png" alt="Displaying text" style="max-width:100%;"></a></p>

<p>You've already seen all the code that creates all these elements, as
well as the
keyboard control system that makes the cat move. The only new thing is the
way <code>hitTestRectangle</code> is used inside the <code>play</code> function to check for a
collision.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">play</span>() {

  <span class="pl-c">//use the cat's velocity to make it move</span>
  <span class="pl-smi">cat</span>.<span class="pl-c1">x</span> <span class="pl-k">+=</span> <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span>;
  <span class="pl-smi">cat</span>.<span class="pl-c1">y</span> <span class="pl-k">+=</span> <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span>;

  <span class="pl-c">//check for a collision between the cat and the box</span>
  <span class="pl-k">if</span> (<span class="pl-en">hitTestRectangle</span>(cat, box)) {

    <span class="pl-c">//if there's a collision, change the message text</span>
    <span class="pl-c">//and tint the box red</span>
    <span class="pl-smi">message</span>.<span class="pl-c1">text</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">"</span>hit!<span class="pl-pds">"</span></span>;
    <span class="pl-smi">box</span>.<span class="pl-smi">tint</span> <span class="pl-k">=</span> <span class="pl-c1">0xff3300</span>;

  } <span class="pl-k">else</span> {

    <span class="pl-c">//if there's no collision, reset the message</span>
    <span class="pl-c">//text and the box's color</span>
    <span class="pl-smi">message</span>.<span class="pl-c1">text</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">"</span>No collision...<span class="pl-pds">"</span></span>;
    <span class="pl-smi">box</span>.<span class="pl-smi">tint</span> <span class="pl-k">=</span> <span class="pl-c1">0xccff99</span>;
  }
}</pre></div>

<p>Because the <code>play</code> function is being called by the game loop 60 times
per second, this <code>if</code> statement is constantly checking for a collision
between the cat and the box. If <code>hitTestRectangle</code> is <code>true</code>, the
text <code>message</code> object uses <code>setText</code> to display "Hit":</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">message</span>.<span class="pl-c1">text</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">"</span>hit!<span class="pl-pds">"</span></span>;</pre></div>

<p>The color of the box is then changed from green to red by setting the
box's <code>tint</code> property to the hexadecimal red value.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">box</span>.<span class="pl-smi">tint</span> <span class="pl-k">=</span> <span class="pl-c1">0xff3300</span>;</pre></div>

<p>If there's no collision, the message and box are maintained in their
original states:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">message</span>.<span class="pl-c1">text</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">"</span>no collision...<span class="pl-pds">"</span></span>;
<span class="pl-smi">box</span>.<span class="pl-smi">tint</span> <span class="pl-k">=</span> <span class="pl-c1">0xccff99</span>;</pre></div>

<p>This code is pretty simple, but suddenly you've created an interactive
world that seems to be completely alive. It's almost like magic! And, perhaps
surprisingly, you now have all the skills you need to start making
games with Pixi!</p>

<p><a id="user-content-hittest"></a></p>

<h3><a id="user-content-the-hittestrectangle-function" class="anchor" href="#the-hittestrectangle-function" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>The hitTestRectangle function</h3>

<p>But what about the <code>hitTestRectangle</code> function? What does it do, and
how does it work? The details of how collision detection algorithms
like this work is a little bit outside the scope of this tutorial.
The most important thing is that you know how to use it. But, just for
your reference, and in case you're curious, here's the complete
<code>hitTestRectangle</code> function definition. Can you figure out from the
comments what it's doing?</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">hitTestRectangle</span>(<span class="pl-smi">r1</span>, <span class="pl-smi">r2</span>) {

  <span class="pl-c">//Define the variables we'll need to calculate</span>
  <span class="pl-k">var</span> hit, combinedHalfWidths, combinedHalfHeights, vx, vy;

  <span class="pl-c">//hit will determine whether there's a collision</span>
  hit <span class="pl-k">=</span> <span class="pl-c1">false</span>;

  <span class="pl-c">//Find the center points of each sprite</span>
  <span class="pl-smi">r1</span>.<span class="pl-smi">centerX</span> <span class="pl-k">=</span> <span class="pl-smi">r1</span>.<span class="pl-c1">x</span> <span class="pl-k">+</span> <span class="pl-smi">r1</span>.<span class="pl-c1">width</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;
  <span class="pl-smi">r1</span>.<span class="pl-smi">centerY</span> <span class="pl-k">=</span> <span class="pl-smi">r1</span>.<span class="pl-c1">y</span> <span class="pl-k">+</span> <span class="pl-smi">r1</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;
  <span class="pl-smi">r2</span>.<span class="pl-smi">centerX</span> <span class="pl-k">=</span> <span class="pl-smi">r2</span>.<span class="pl-c1">x</span> <span class="pl-k">+</span> <span class="pl-smi">r2</span>.<span class="pl-c1">width</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;
  <span class="pl-smi">r2</span>.<span class="pl-smi">centerY</span> <span class="pl-k">=</span> <span class="pl-smi">r2</span>.<span class="pl-c1">y</span> <span class="pl-k">+</span> <span class="pl-smi">r2</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;

  <span class="pl-c">//Find the half-widths and half-heights of each sprite</span>
  <span class="pl-smi">r1</span>.<span class="pl-smi">halfWidth</span> <span class="pl-k">=</span> <span class="pl-smi">r1</span>.<span class="pl-c1">width</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;
  <span class="pl-smi">r1</span>.<span class="pl-smi">halfHeight</span> <span class="pl-k">=</span> <span class="pl-smi">r1</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;
  <span class="pl-smi">r2</span>.<span class="pl-smi">halfWidth</span> <span class="pl-k">=</span> <span class="pl-smi">r2</span>.<span class="pl-c1">width</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;
  <span class="pl-smi">r2</span>.<span class="pl-smi">halfHeight</span> <span class="pl-k">=</span> <span class="pl-smi">r2</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;

  <span class="pl-c">//Calculate the distance vector between the sprites</span>
  vx <span class="pl-k">=</span> <span class="pl-smi">r1</span>.<span class="pl-smi">centerX</span> <span class="pl-k">-</span> <span class="pl-smi">r2</span>.<span class="pl-smi">centerX</span>;
  vy <span class="pl-k">=</span> <span class="pl-smi">r1</span>.<span class="pl-smi">centerY</span> <span class="pl-k">-</span> <span class="pl-smi">r2</span>.<span class="pl-smi">centerY</span>;

  <span class="pl-c">//Figure out the combined half-widths and half-heights</span>
  combinedHalfWidths <span class="pl-k">=</span> <span class="pl-smi">r1</span>.<span class="pl-smi">halfWidth</span> <span class="pl-k">+</span> <span class="pl-smi">r2</span>.<span class="pl-smi">halfWidth</span>;
  combinedHalfHeights <span class="pl-k">=</span> <span class="pl-smi">r1</span>.<span class="pl-smi">halfHeight</span> <span class="pl-k">+</span> <span class="pl-smi">r2</span>.<span class="pl-smi">halfHeight</span>;

  <span class="pl-c">//Check for a collision on the x axis</span>
  <span class="pl-k">if</span> (<span class="pl-c1">Math</span>.<span class="pl-c1">abs</span>(vx) <span class="pl-k">&lt;</span> combinedHalfWidths) {

    <span class="pl-c">//A collision might be occuring. Check for a collision on the y axis</span>
    <span class="pl-k">if</span> (<span class="pl-c1">Math</span>.<span class="pl-c1">abs</span>(vy) <span class="pl-k">&lt;</span> combinedHalfHeights) {

      <span class="pl-c">//There's definitely a collision happening</span>
      hit <span class="pl-k">=</span> <span class="pl-c1">true</span>;
    } <span class="pl-k">else</span> {

      <span class="pl-c">//There's no collision on the y axis</span>
      hit <span class="pl-k">=</span> <span class="pl-c1">false</span>;
    }
  } <span class="pl-k">else</span> {

    <span class="pl-c">//There's no collision on the x axis</span>
    hit <span class="pl-k">=</span> <span class="pl-c1">false</span>;
  }

  <span class="pl-c">//`hit` will be either `true` or `false`</span>
  <span class="pl-k">return</span> hit;
};
</pre></div>