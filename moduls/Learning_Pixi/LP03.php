<style type="text/css">

</style>
<div class=titlebar> Learning Pixi </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
	echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
	<div class=contentInBox>


<a href="?LP_Content">Содержание </a>
<br>

<p><a id="user-content-renderer"></a></p>
<h2>Создание сцены и визуализации</h2>

<p>Теперь вы можете начать использовать Pixi!</p>

<p>Но как? </p>

<p>Первый шаг заключается в создании прямоугольной области экрана, что бы можно было начать отображать изображения на нем. Pixi имеет <code>renderer</code> объект, который создает это для вас. Он автоматически генерирует HTML <code>&lt;canvas&gt;</code>  элемент и выясняет, как отображать изображения в canvas. Затем необходимо создать специальный Pixi <code>Container</code> объект называется <code>stage</code>. Как вы увидите дальше, этот объект этап будет использоваться в качестве корневого контейнера, который содержит все, что вы хотите отобразить в Pixi.</p> 

<p>Вот код, вам нужно написать, чтобы создать <code>renderer</code> и <code>stage</code>. Добавьте этот код в ваш HTML-документа между <code>&lt;script&gt;</code> теги:</p> 
 
<div class="highlight highlight-source-js"><pre><span class="pl-c">//Create the renderer</span> 
<span class="pl-k">var</span> renderer <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-en">autoDetectRenderer</span>(<span class="pl-c1">256</span>, <span class="pl-c1">256</span>);

<span class="pl-c">//Добавьте холст в HTML-документе</span>
<span class="pl-c1">document</span>.<span class="pl-c1">body</span>.<span class="pl-c1">appendChild</span>(<span class="pl-smi">renderer</span>.<span class="pl-smi">view</span>);

<span class="pl-c">//Создать объект-контейнер называется `stage`</span>
<span class="pl-k">var</span> stage <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.Container</span>();

<span class="pl-c">//Tell the `renderer` to `render` the `stage`</span>
<span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);</pre></div>

<p>Это самый основной код, который нужно написать, чтобы начать использовать Pixi. Она производит черный 256 пикселей на 256 пикселей canvas элемент и добавляет его в HTML-документе.  Вот как это выглядит в браузере при выполнении этого кода..</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s01.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/01.png" alt="Basic display" style="max-width:100%;"></a></p>

<p>Да, <a href="http://rampantgames.com/blog/?p=7745">черный квадрат</a>!</p>

<p>Pixi’s <code>autoDetectRenderer</code> метод выясняет, следует ли использовать Canvas Drawing API или WebGL для отображения графики, в зависимости от того, который доступен. Это первый и второй аргументы ширина и высота canvas. Тем не менее, вы можете добавить необязательный третий аргумент, с некоторыми дополнительными значениями, которые можно настроить. Этот третий аргумент является объектом буквальным, и вот как можно использовать это установить сглаживание, прозрачность и разрешение:</p>

<div class="highlight highlight-source-js"><pre>renderer <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-en">autoDetectRenderer</span>(
  <span class="pl-c1">256</span>, <span class="pl-c1">256</span>,
  {antialias<span class="pl-k">:</span> <span class="pl-c1">false</span>, transparent<span class="pl-k">:</span> <span class="pl-c1">false</span>, resolution<span class="pl-k">:</span> <span class="pl-c1">1</span>}
);</pre></div>

<p>Это третий аргумент (объект опции) не является обязательным - если вы счастливы с настройками Pixi По умолчанию вы можете оставить его, и как правило, нет необходимости менять их. (Но, если вам нужно, обратитесь к документации по Pixi на <a href="http://pixijs.github.io/docs/PIXI.CanvasRenderer.html">Canvas Renderer</a> и <a href="http://pixijs.github.io/docs/PIXI.WebGLRenderer.html">WebGLRenderer</a> для получения дополнительной информации.)</p>

<p>Что эти варианты делают?</p>

<div class="highlight highlight-source-js"><pre>{antialias<span class="pl-k">:</span> <span class="pl-c1">false</span>, transparent<span class="pl-k">:</span> <span class="pl-c1">false</span>, resolution<span class="pl-k">:</span> <span class="pl-c1">1</span>}</pre></div>

<p><code>antialias</code> сглаживает края шрифтов и графических примитивов. (WebGL anti-aliasing не доступен на всех платформах, так что вам нужно, проверить это на целевой платформе вашей игры.) <code>transparent</code> делает canvas фон прозрачным. <code>resolution</code> легче работать с дисплеями с различным разрешением и плотностью пикселей. Установка resolutions немного выходит за рамки данного руководства, но посмотрите <a href="http://www.goodboydigital.com/pixi-js-v2-fastest-2d-webgl-renderer/">Mat Grove's объяснение</a> о том, как использовать <code>resolution</code> во всех деталях. Но, как правило, просто держите <code>resolution</code> 1 для большинства проектов, и все будет в порядке. </p>

<p>(Примечание: renderer  имеет дополнительный, четвертый, вариант под названием <code>preserveDrawingBuffer</code> который по умолчанию <code>false</code>. Единственная причина, установить его в <code>true</code> если вам когда-нибудь нужно вызвать Pixi в специализированный <code>dataToURL</code> метод на WebGL canvas context.)</p>

<p>Pixi's renderer object будет по умолчанию WebGL, который хорош, потому что WebGL невероятно быстро, и позволяет использовать некоторые впечатляющие визуальные эффекты, которые вы узнаете все о будущем. Но если вам нужно, Canvas Drawing API rendering через WebGL, вы можете сделать это так:</p>

<div class="highlight highlight-source-js"><pre>renderer <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.CanvasRenderer</span>(<span class="pl-c1">256</span>, <span class="pl-c1">256</span>);</pre></div>

<p>Только первые два аргумента являются обязательными: <code>width</code> и <code>height</code>.</p>

<p>Вы можете заставить WebGL rendering так:</p>

<div class="highlight highlight-source-js"><pre>renderer <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.WebGLRenderer</span>(<span class="pl-c1">256</span>, <span class="pl-c1">256</span>);</pre></div>

<p><code>renderer.view</code> объект, просто обычный старый обычный <code>&lt;canvas&gt;</code> объект, так что вы можете управлять им так же, как вы бы контролировать любой другой объект canvas. Вот так canvas добавляем пунктирную границу (border):</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">renderer</span>.<span class="pl-smi">view</span>.<span class="pl-c1">style</span>.<span class="pl-c1">border</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">"</span>1px dashed black<span class="pl-pds">"</span></span>;</pre></div>

<p>Если вам нужно изменить цвет фона холста после того, как вы создали его, установите <code>renderer</code> объект <code>backgroundColor</code> свойство на любое значение цвета ы шестнадцатеричном значении:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">renderer</span>.<span class="pl-c1">backgroundColor</span> <span class="pl-k">=</span> <span class="pl-c1">0x061639</span>;</pre></div>

<p>Если вы хотите, найти ширину или высоту <code>renderer</code>, используйте <code>renderer.view.width</code> и <code>renderer.view.height</code>.</p>

<p>(Важно отметить: хотя <code>stage</code> также имеет <code>width</code> и <code>height</code>, <em>ни не относятся к размеру rendering window</em>. В сцене  <code>width</code> и <code>height</code> просто сказать вам площадь, занимаемую вещами вы положили в нем - больше на том, что впереди!)</p>

<p>Чтобы изменить размер canvas, используйте <code>renderer</code>’s <code>resize</code> метод, и поставить любой новый <code>width</code> и <code>height</code> значения. Но, чтобы убедиться, что полотно изменяется в соответствии с разрешением, установите <code>autoResize</code> в <code>true</code>.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">renderer</span>.<span class="pl-smi">autoResize</span> <span class="pl-k">=</span> <span class="pl-c1">true</span>;
<span class="pl-smi">renderer</span>.<span class="pl-c1">resize</span>(<span class="pl-c1">512</span>, <span class="pl-c1">512</span>);</pre></div>

<p>Если вы хотите, чтобы сделать холст заполнить все окно, вы можете применить этот CSS стиль и размер renderer к размеру окна браузера.</p>

<pre><code>renderer.view.style.position = "absolute";
renderer.view.style.display = "block";
renderer.autoResize = true;
renderer.resize(window.innerWidth, window.innerHeight);
</code></pre>

<p>Но, если вы сделаете это, убедитесь, что вы также установить отступы по умолчанию и поля 0 на всех ваших HTML-элементов с этим битом кода css:</p>

<div class="highlight highlight-text-html-basic"><pre><span class="pl-s1">&lt;<span class="pl-ent">style</span>&gt;<span class="pl-ent">*</span> {<span class="pl-c1"><span class="pl-c1">padding</span></span>: <span class="pl-c1">0</span>; <span class="pl-c1"><span class="pl-c1">margin</span></span>: <span class="pl-c1">0</span>}&lt;/<span class="pl-ent">style</span>&gt;</span></pre></div>

<p>(Звездочка, *, в приведенном выше коде, это CSS "универсальный селектор", который просто означает, "все теги в HTML-документе".)</p>

<p>Если вы хотите, масштабировать canvas пропорционально любого размера окна браузера, вы можете использовать <a href="https://github.com/kittykatattack/scaleToWindow">эту пользовательскую функцию scaleToWindow.</a>.</p>


</div>
<a href="?LP_Content"> Содержание </a>
</div>
