<article class="markdown-body entry-content" itemprop="text"><h1><a id="user-content-learning-pixi" class="anchor" href="#learning-pixi" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Learning Pixi</h1>

<p>A step-by-step introduction to making games and interactive media with
the <a href="https://github.com/pixijs/pixi.js">Pixi rendering engine</a>. <strong>Updated for Pixi v4.0</strong>. If you like this
tutorial, <a href="http://www.springer.com/us/book/9781484210956">you'll love the book, which contains 80% more content!</a></p>

<h3><a id="user-content-table-of-contents" class="anchor" href="#table-of-contents" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Table of contents:</h3>

<ol>
<li><a href="#introduction">Introduction</a></li>
<li><a href="#settingup">Setting up</a>

<ol>
<li><a href="#installingpixithesimpleway">Installing Pixi the simple way</a></li>
<li><a href="#installingpixiwithgit">Installing Pixi with Git</a></li>
<li><a href="#installingpixiwithnodeandgulp">Installing Pixi with Node and Gulp</a></li>
</ol></li>
<li><a href="#renderer">Creating the stage and renderer</a></li>
<li><a href="#sprites">Pixi sprites</a></li>
<li><a href="#loading">Loading images into the texture cache</a></li>
<li><a href="#displaying">Displaying sprites</a>

<ol>
<li><a href="#usingaliases">Using Aliases</a></li>
<li><a href="#alittlemoreaboutloadingthings">A little more about loading things</a>
1.<a href="#makeaspritefromanordinaryjavascriptimageobject">Make a sprite from an ordinary JavaScript Image object or Canvas</a>
2.<a href="#assigninganametoaloadingfile">Assigning a name to a loaded file</a>
3.<a href="#monitoringloadprogress">Monitoring load progress</a>
4.<a href="#moreaboutpixisloader">More about Pixi's loader</a></li>
</ol></li>
<li><a href="#positioning">Positioning sprites</a></li>
<li><a href="#sizenscale">Size and scale</a></li>
<li><a href="#rotation">Rotation</a></li>
<li><a href="#tileset">Make a sprite from a tileset sub-image</a></li>
<li><a href="#textureatlas">Using a texture atlas</a></li>
<li><a href="#loadingatlas">Loading the texture atlas</a></li>
<li><a href="#createsprites">Creating sprites from a loaded texture atlas</a></li>
<li><a href="#movingsprites">Moving Sprites</a></li>
<li><a href="#velocity">Using velocity properties</a></li>
<li><a href="#gamestates">Game states</a></li>
<li><a href="#keyboard">Keyboard Movement</a></li>
<li><a href="#grouping">Grouping Sprites</a>

<ol>
<li><a href="#localnglobal">Local and global positions</a></li>
<li><a href="#spritebatch">Using a ParticleContainer to group sprites</a></li>
</ol></li>
<li><a href="#graphic">Pixi's Graphic Primitives</a>

<ol>
<li><a href="#rectangle">Rectangle</a></li>
<li><a href="#circles">Circles</a></li>
<li><a href="#ellipses">Ellipses</a></li>
<li><a href="#roundedrects">Rounded rectangles</a></li>
<li><a href="#lines">Lines</a></li>
<li><a href="#polygons">Polygons</a></li>
</ol></li>
<li><a href="#text">Displaying text</a></li>
<li><a href="#collision">Collision detection</a>

<ol>
<li><a href="/kittykatattack/learningPixi/blob/master/hittest">The hitTestRectangle function</a></li>
</ol></li>
<li><a href="#casestudy">Case study: Treasure Hunter</a>

<ol>
<li><a href="#initialize">Initialize the game in the setup function</a>

<ol>
<li><a href="#gamescene">Creating the game scenes</a></li>
<li><a href="#makingdungon">Making the dungeon, door, explorer and treasure</a></li>
<li><a href="#makingblob">Making the blob monsters</a></li>
<li><a href="#healthbar">Making health bar</a></li>
<li><a href="#message">Making message text</a></li>
</ol></li>
<li><a href="#playing">Playing the game</a></li>
<li><a href="#movingexplorer">Moving the explorer</a>

<ol>
<li><a href="#containingmovement">Containing movement</a></li>
</ol></li>
<li><a href="#movingmonsters">Moving the monsters</a></li>
<li><a href="#checkingcollisions">Checking for collisions</a></li>
<li><a href="#reachingexit">Reaching the exit door and ending game</a></li>
</ol></li>
<li><a href="#spriteproperties">More about sprites</a></li>
<li><a href="#takingitfurther">Taking it further</a><br>
i.<a href="#hexi">Hexi</a><br></li>
<li><a href="#supportingthisproject">Supporting this project</a></li>
</ol>

<p><a id="user-content-introduction"></a></p>

<h2><a id="user-content-introduction" class="anchor" href="#introduction" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Introduction</h2>

<p>Pixi’s is an extremely fast 2D sprite rendering engine. What does that
mean? It means that it helps you to display, animate and manage
interactive graphics so that it's easy for you to make games and
applications using
JavaScript and other HTML5 technologies. It has a sensible,
uncluttered API and includes many useful features, like supporting
texture atlases and providing a streamlined system for animating
sprites (interactive images). It also gives you a complete scene graph so that you can
create hierarchies of nested sprites (sprites inside sprites), as well
as letting you attach mouse and touch events directly to sprites. And,
most
importantly, Pixi gets out of your way so that you can use as much or
as little of it as you want to, adapt it to your personal coding
style, and integrate it seamlessly with other useful frameworks.</p>

<p>Pixi’s API is actually a refinement of a well-worn and battle-tested
API pioneered by Macromedia/Adobe Flash. Old-skool Flash developers
will feel right at home. Other current sprite rendering frameworks use
a similar API: CreateJS, Starling, Sparrow and Apple’s SpriteKit. The
strength of Pixi’s API is that it’s general-purpose: it’s not a game
engine. That’s good because it gives you total expressive freedom to make anything you like, and wrap your own custom game engine around it.</p>

<p>In this tutorial you’re going to find out how to combine Pixi’s
powerful image rendering features and scene graph to start making
games. You’re also going to learn how to prepare your game graphics
with a texture atlas, how to make particle effects using the Proton
particle engine, and how to integrate Pixi into your own custom game
engine. But Pixi isn't just for games - you can use these same
techniques to create any interactive media applications. That means
apps for phones!</p>

<p>What do you need to know before you get started with this tutorial?</p>

<p>You should have a reasonable understanding of HTML and
JavaScript. You don't have to be an expert, just an ambitious beginner
with an eagerness to learn. If you don't know HTML and JavaScript, the
best place to start learning it is this book:</p>

<p><a href="http://www.apress.com/9781430247166">Foundation Game Design with HTML5 and JavaScript</a></p>

<p>I know for a fact that it's the best book, because I wrote it!</p>

<p>There are also some good internet resources to help get you started:</p>

<p><a href="http://www.khanacademy.org/computing/cs">Khan Academy: Computer
Programming</a></p>

<p><a href="http://www.codecademy.com/tracks/javascript">Code Academy:
JavaScript</a></p>

<p>Choose whichever best suits your learning style.</p>

<p>Ok, got it?
Do you know what JavaScript variables, functions, arrays and objects are and how to
use them? Do you know what <a href="http://www.copterlabs.com/blog/json-what-it-is-how-it-works-how-to-use-it/">JSON data
files</a>
are? Have you used the <a href="https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Drawing_graphics_with_canvas">Canvas Drawing API</a>?</p>

<p>To use Pixi, you'll also need to run a webserver in your root project
directory. Do you know what a webserver is and
how to launch one in your project folder? The best way is to use
<a href="http://nodejs.org">node.js</a> and then to install the extremely easy to use
<a href="https://github.com/nodeapps/http-server">http-server</a>. However, you need to be comfortable working with the Unix
command line if you want to do that. You can learn how to use
Unix <a href="https://www.youtube.com/watch?feature=player_embedded&amp;v=cX9ASUE3YAQ">in this
video</a>
and, when you're finished, follow it with <a href="https://www.youtube.com/watch?v=INk0ATBbclc">this
video</a>. You should learn
how to use Unix - it only takes a couple of hours to learn and is a
really fun and easy way to interact with your computer.</p>

<p>But if you don't want to mess around with the command line just yet, try the Mongoose
webserver:</p>

<p><a href="http://cesanta.com/mongoose.shtml">Mongoose</a></p>

<p>Or, just use write your all your code using the excellent <a href="http://brackets.io">Brackets text
editor</a>. Brackets automatically launches a webserver
and browser for you when you click the lightening bolt button in its
main workspace.</p>

<p>Now if you think you're ready, read on!</p>

<p>(Request to readers: this is a <em>living document</em>. If you have any
questions about specific details or need any of the content clarified, please
create an <strong>issue</strong> in this GitHub repository and I'll update the text
with more information.)</p>

<p><a id="user-content-settingup"></a></p>

<h2><a id="user-content-setting-up" class="anchor" href="#setting-up" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Setting up</h2>

<p>Before you start writing any code, create a folder for your project, and launch a
webserver in the project's root directory. If you aren't running a
webserver, Pixi won't work.</p>

<p>Next, you need to install Pixi. There are two ways to do it: the
<strong>simple</strong> way, with <strong>Git</strong> or with <strong>Gulp and Node</strong>. </p>

<p><a id="user-content-installingpixithesimpleway"></a></p>

<h3><a id="user-content-installing-pixi-the-simple-way" class="anchor" href="#installing-pixi-the-simple-way" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Installing Pixi the simple way</h3>

<p>Get the latest version of the <code>pixi.min.js</code> file and from
<a href="https://github.com/pixijs/pixi.js/tree/master/bin">Pixi's GitHub
Repo</a>.</p>

<p>This one file is all you need to use Pixi. You can ignore all the
other files in the repository: <strong>you don't need them.</strong></p>

<p>Next, create a basic HTML page, and use a
<code>&lt;script&gt;</code> tag to link the
<code>pixi.min.js</code> file that you've just downloaded. The <code>&lt;script&gt;</code> tag's <code>src</code>
should be relative to your root directory where your webserver is
running. Your <code>&lt;script&gt;</code> tag might look something like this:</p>

<div class="highlight highlight-text-html-basic"><pre>&lt;<span class="pl-ent">script</span> <span class="pl-e">src</span>=<span class="pl-s"><span class="pl-pds">"</span>pixi.min.js<span class="pl-pds">"</span></span>&gt;&lt;/<span class="pl-ent">script</span>&gt;</pre></div>

<p>Here's a basic HTML page that you could use to link Pixi and test that
it's working:</p>

<div class="highlight highlight-text-html-basic"><pre>&lt;!doctype html&gt;
&lt;<span class="pl-ent">meta</span> <span class="pl-e">charset</span>=<span class="pl-s"><span class="pl-pds">"</span>utf-8<span class="pl-pds">"</span></span>&gt;
&lt;<span class="pl-ent">title</span>&gt;Hello World&lt;/<span class="pl-ent">title</span>&gt;
&lt;<span class="pl-ent">body</span>&gt;
&lt;<span class="pl-ent">script</span> <span class="pl-e">src</span>=<span class="pl-s"><span class="pl-pds">"</span>pixi.min.js<span class="pl-pds">"</span></span>&gt;&lt;/<span class="pl-ent">script</span>&gt;
<span class="pl-s1">&lt;<span class="pl-ent">script</span>&gt;</span>
<span class="pl-s1"></span>
<span class="pl-s1"><span class="pl-c">//Test that Pixi is working</span></span>
<span class="pl-s1"><span class="pl-en">console</span>.<span class="pl-c1">log</span>(<span class="pl-c1">PIXI</span>);</span>
<span class="pl-s1"></span>
<span class="pl-s1">&lt;/<span class="pl-ent">script</span>&gt;</span>
&lt;/<span class="pl-ent">body</span>&gt;</pre></div>

<p>This is the <a href="http://stackoverflow.com/questions/9797046/whats-a-valid-html5-document">minimal amount of HTML</a> you need to start creating projects
with Pixi. If Pixi is linking correctly, <code>console.log(PIXI)</code> will
display something like this in your web browser's JavaScript console:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c1">Object</span> { VERSION<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">"</span>3.0.7<span class="pl-pds">"</span></span> <span class="pl-k">...</span></pre></div>

<p>If you see that (or something similar) you know everything is working properly.</p>

<p>Now you can start working with Pixi!</p>

<p><a id="user-content-installingpixiwithgit"></a></p>

<h3><a id="user-content-installing-pixi-with-git" class="anchor" href="#installing-pixi-with-git" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Installing Pixi with Git</h3>

<p>You can also use Git to install use Pixi. (What is <strong>git</strong>? If you don't know <a href="https://github.com/kittykatattack/learningGit">you can find out
here</a>.) This has some advantages:
you can just run <code>git pull origin master</code> from the command line to updated Pixi to
the latest version. And, if you think you've found a bug in Pixi, 
you can fix it and submit a pull request to have the bug fix added to the main repository.</p>

<p>To clone the Pixi repository with Git,  <code>cd</code>
into your root project directory and type:</p>

<pre><code>git clone git@github.com:pixijs/pixi.js.git
</code></pre>

<p>This automatically creates
a folder called <code>pixi.js</code> and loads the latest version of Pixi into it.</p>

<p>After Pixi is installed, create a basic HTML document, and use a
<code>&lt;script&gt;</code> tag to include the
<code>pixi.js</code> file from Pixi's <code>bin</code> folder. The <code>&lt;script&gt;</code> tag's <code>src</code>
should be relative to your root directory where your webserver is
running. Your <code>&lt;script&gt;</code> tag might look something like this:</p>

<div class="highlight highlight-text-html-basic"><pre>&lt;<span class="pl-ent">script</span> <span class="pl-e">src</span>=<span class="pl-s"><span class="pl-pds">"</span>pixi.js/bin/pixi.js<span class="pl-pds">"</span></span>&gt;&lt;/<span class="pl-ent">script</span>&gt;</pre></div>

<p>(If you prefer, you could link to the <code>pixi.min.js</code> file instead as I
suggested in the previous section. The
minified file might actually run slightly faster, and it will
certainly load faster. The advantage to using the
un-minified plain JS file is that if the compiler thinks there's a bug in Pixi's
source code, it will give you an error message that displays the questionable code
in a readable format. This is useful while you're working on a
project, because even if the bug isn't in Pixi, the error might give
you a hint as to what's wrong with your own code.)</p>

<p>In this <strong>Learning Pixi</strong> repository (what you're reading now!) you'll find a folder called
<code>examples</code>. Open it and you'll find a file called <code>helloWorld.html</code>.
Assuming that the webserver is running in this repository's root directory, this is
how the <code>helloWorld.html</code> file correctly links to Pixi and checks that it's
working:</p>

<div class="highlight highlight-text-html-basic"><pre>&lt;!doctype html&gt;
&lt;<span class="pl-ent">meta</span> <span class="pl-e">charset</span>=<span class="pl-s"><span class="pl-pds">"</span>utf-8<span class="pl-pds">"</span></span>&gt;
&lt;<span class="pl-ent">title</span>&gt;Hello World&lt;/<span class="pl-ent">title</span>&gt;
&lt;<span class="pl-ent">body</span>&gt;
&lt;<span class="pl-ent">script</span> <span class="pl-e">src</span>=<span class="pl-s"><span class="pl-pds">"</span>../pixi.js/bin/pixi.js<span class="pl-pds">"</span></span>&gt;&lt;/<span class="pl-ent">script</span>&gt;
<span class="pl-s1">&lt;<span class="pl-ent">script</span>&gt;</span>
<span class="pl-s1"></span>
<span class="pl-s1"><span class="pl-c">//Test that Pixi is working</span></span>
<span class="pl-s1"><span class="pl-en">console</span>.<span class="pl-c1">log</span>(<span class="pl-c1">PIXI</span>);</span>
<span class="pl-s1"></span>
<span class="pl-s1">&lt;/<span class="pl-ent">script</span>&gt;</span>
&lt;/<span class="pl-ent">body</span>&gt;</pre></div>

<p>If Pixi is linking correctly, <code>console.log(PIXI)</code> will
display something like this in your web browser's JavaScript console:</p>

<pre><code>Object { VERSION: "3.0.7" ...
</code></pre>

<p><a id="user-content-installingpixiwithnodeandgulp"></a></p>

<h3><a id="user-content-installing-pixi-with-node-and-gulp" class="anchor" href="#installing-pixi-with-node-and-gulp" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Installing Pixi with Node and Gulp</h3>

<p>You can also install Pixi using <a href="https://nodejs.org">Node</a> and <a href="http://gulpjs.com">Gulp</a>. If you need
to do a custom build of Pixi to include or exclude certain features,
this is the route you should take. <a href="https://github.com/GoodBoyDigital/pixi.js">See Pixi's GitHub repository for
details on how</a>. But, in general
there's no need to do this.</p>

<p><a id="user-content-renderer"></a></p>

<h2><a id="user-content-creating-the-renderer-and-stage" class="anchor" href="#creating-the-renderer-and-stage" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Creating the renderer and stage</h2>

<p>Now you can start using Pixi!</p>

<p>But how? </p>

<p>The first step is to create a rectangular
display area that you can start displaying images on. Pixi has a
<code>renderer</code> object that creates this for you. It
automatically generates an HTML <code>&lt;canvas&gt;</code> element and figures out how
to display your images on the canvas. You then need to create a
special Pixi <code>Container</code> object called the <code>stage</code>. As you'll see
ahead, this stage object is going to be used as the root container
that holds all the things you want Pixi to display. </p>

<p>Here’s the code you need to write to create a <code>renderer</code>
and <code>stage</code>. Add this code to your HTML document between the <code>&lt;script&gt;</code> tags:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c">//Create the renderer</span>
<span class="pl-k">var</span> renderer <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-en">autoDetectRenderer</span>(<span class="pl-c1">256</span>, <span class="pl-c1">256</span>);

<span class="pl-c">//Add the canvas to the HTML document</span>
<span class="pl-c1">document</span>.<span class="pl-c1">body</span>.<span class="pl-c1">appendChild</span>(<span class="pl-smi">renderer</span>.<span class="pl-smi">view</span>);

<span class="pl-c">//Create a container object called the `stage`</span>
<span class="pl-k">var</span> stage <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.Container</span>();

<span class="pl-c">//Tell the `renderer` to `render` the `stage`</span>
<span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);</pre></div>

<p>This is the most basic code you need write to get started using Pixi. 
It produces a black 256 pixel by 256 pixel canvas element and adds it to your
HTML document. Here's what this looks like in a browser when you run this code.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s01.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/01.png" alt="Basic display" style="max-width:100%;"></a></p>

<p>Yay, a <a href="http://rampantgames.com/blog/?p=7745">black square</a>!</p>

<p>Pixi’s <code>autoDetectRenderer</code> method figures out whether to use the
Canvas Drawing API or WebGL to render graphics, depending on which is
available. It's first and second arguments are the width and height of the
canvas. However, you can include an optional third argument with some
additional values you can set. This third argument is an object
literal, and here's how could use it set anti-aliasing, transparency
and resolution:</p>

<div class="highlight highlight-source-js"><pre>renderer <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-en">autoDetectRenderer</span>(
  <span class="pl-c1">256</span>, <span class="pl-c1">256</span>,
  {antialias<span class="pl-k">:</span> <span class="pl-c1">false</span>, transparent<span class="pl-k">:</span> <span class="pl-c1">false</span>, resolution<span class="pl-k">:</span> <span class="pl-c1">1</span>}
);</pre></div>

<p>This third argument (the options object) is optional - if you're happy with Pixi's default
settings you can leave it out, and there's usually no need to change
them. (But, if you need to, see Pixi's documentation on the <a href="http://pixijs.github.io/docs/PIXI.CanvasRenderer.html">Canvas
Renderer</a>
and
<a href="http://pixijs.github.io/docs/PIXI.WebGLRenderer.html">WebGLRenderer</a>
for more information.)</p>

<p>What do those options do?</p>

<div class="highlight highlight-source-js"><pre>{antialias<span class="pl-k">:</span> <span class="pl-c1">false</span>, transparent<span class="pl-k">:</span> <span class="pl-c1">false</span>, resolution<span class="pl-k">:</span> <span class="pl-c1">1</span>}</pre></div>

<p><code>antialias</code> smoothes the edges of fonts and graphic primitives. (WebGL
anti-aliasing isn’t available on all platforms, so you’ll need to test
this on your game’s target platform.) <code>transparent</code> makes the canvas
background transparent. <code>resolution</code> makes it easier to work with
displays of varying resolutions and pixel densities. Setting
the resolutions is a little
outside the scope of this tutorial, but check out <a href="http://www.goodboydigital.com/pixi-js-v2-fastest-2d-webgl-renderer/">Mat Grove's
explanation</a>
about how to use <code>resolution</code> for all the details. But usually, just keep <code>resolution</code>
at 1 for most projects and you'll be fine. </p>

<p>(Note: The renderer has an additional, fourth, option called <code>preserveDrawingBuffer</code> that
defaults to <code>false</code>. The only reason to it set it
to <code>true</code> is if you ever need to call Pixi's specialized <code>dataToURL</code>
method on a WebGL canvas context.)</p>

<p>Pixi's renderer object will default to WebGL, which is good, because WebGL is
incredibly fast, and lets you use some spectacular visual effects that
you’ll learn all about ahead. But if you need to force Canvas Drawing
API rendering over WebGL, you can do it like this:</p>

<div class="highlight highlight-source-js"><pre>renderer <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.CanvasRenderer</span>(<span class="pl-c1">256</span>, <span class="pl-c1">256</span>);</pre></div>

<p>Only the first two arguments are required: <code>width</code> and <code>height</code>.</p>

<p>You can force WebGL rendering like this:</p>

<div class="highlight highlight-source-js"><pre>renderer <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.WebGLRenderer</span>(<span class="pl-c1">256</span>, <span class="pl-c1">256</span>);</pre></div>

<p>The <code>renderer.view</code> object is just a plain old ordinary <code>&lt;canvas&gt;</code>
object, so you can control it the same way you would control any other
canvas object. Here's how to give the canvas an optional dashed
border:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">renderer</span>.<span class="pl-smi">view</span>.<span class="pl-c1">style</span>.<span class="pl-c1">border</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">"</span>1px dashed black<span class="pl-pds">"</span></span>;</pre></div>

<p>If you need to change the background color of the canvas after you’ve
created it, set the <code>renderer</code> object’s <code>backgroundColor</code> property to
any hexadecimal color value:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">renderer</span>.<span class="pl-c1">backgroundColor</span> <span class="pl-k">=</span> <span class="pl-c1">0x061639</span>;</pre></div>

<p>If you want to find the width or the height of the <code>renderer</code>, use
<code>renderer.view.width</code> and <code>renderer.view.height</code>.</p>

<p>(Importantly: although the <code>stage</code> also has <code>width</code> and <code>height</code> properties, <em>they don't refer to
the size of the rendering window</em>. The stage's <code>width</code> and <code>height</code>
just tell you the area occupied by the things you put inside it - more
on that ahead!)</p>

<p>To change the size of the canvas, use the <code>renderer</code>’s <code>resize</code>
method, and supply any new <code>width</code> and <code>height</code> values. But, to make
sure the canvas is resized to match the resolution, set <code>autoResize</code>
to <code>true</code>.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">renderer</span>.<span class="pl-smi">autoResize</span> <span class="pl-k">=</span> <span class="pl-c1">true</span>;
<span class="pl-smi">renderer</span>.<span class="pl-c1">resize</span>(<span class="pl-c1">512</span>, <span class="pl-c1">512</span>);</pre></div>

<p>If you want to make the canvas fill the entire window, you can apply this
CSS styling and resize the renderer to the size of the browser window.</p>

<pre><code>renderer.view.style.position = "absolute";
renderer.view.style.display = "block";
renderer.autoResize = true;
renderer.resize(window.innerWidth, window.innerHeight);
</code></pre>

<p>But, if you do that, make sure you also set the default padding and
margins to 0 on all your HTML elements with this bit of CSS code:</p>

<div class="highlight highlight-text-html-basic"><pre><span class="pl-s1">&lt;<span class="pl-ent">style</span>&gt;<span class="pl-ent">*</span> {<span class="pl-c1"><span class="pl-c1">padding</span></span>: <span class="pl-c1">0</span>; <span class="pl-c1"><span class="pl-c1">margin</span></span>: <span class="pl-c1">0</span>}&lt;/<span class="pl-ent">style</span>&gt;</span></pre></div>

<p>(The asterisk, *, in the code above, is the CSS "universal selector",
which just means "all the tags in the the HTML document".)</p>

<p>If you want the canvas to scale proportionally to any browser window
size, you can use <a href="https://github.com/kittykatattack/scaleToWindow">this custom scaleToWindow function.</a>.</p>

<p><a id="user-content-sprites"></a></p>

<h2><a id="user-content-pixi-sprites" class="anchor" href="#pixi-sprites" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Pixi sprites</h2>

<p>In the previous section you learned how to create a <code>stage</code> object,
like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> stage <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.Container</span>();</pre></div>

<p>The <code>stage</code> is a Pixi <code>Container</code> object. You can think of a container
as a kind of empty box that will group together and store whatever you
put inside it.
The <code>stage</code> object that we created is the root container for all the visible
things in your scene. Pixi requires that you have one root container
object, because the <code>renderer</code> needs something to render:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);</pre></div>

<p>Whatever you put inside the <code>stage</code> will be
rendered on the canvas. Right now the <code>stage</code> is empty, but soon we're going to
start putting things inside it.</p>

<p>(Note: You can give your root container any name you like. Call it
<code>scene</code> or <code>root</code> if you prefer. The name
<code>stage</code> is just an old but useful convention, and one we'll be
sticking to in this tutorial.)</p>

<p>So what do you put on the stage? Special image objects called
<strong>sprites</strong>. Sprites are basically just images that you can control
with code. You can control their position, size, and a host of other
properties that are useful for making interactive and animated graphics. Learning to make and control sprites is really the most
important thing about learning to use Pixi. If you know how to make
sprites and add them to the stage, you're just a small step away from
starting to make games.</p>

<p>Pixi has a <code>Sprite</code> class that is a versatile way to make game
sprites. There are three main ways to create them:</p>

<ul>
<li>From a single image file.</li>
<li>From a sub-image on a <strong>tileset</strong>. A tileset is a single, big image that
includes all the images you'll need in your game.</li>
<li>From a <strong>texture atlas</strong> (A JSON file that defines the size and position of an image on a tileset.)</li>
</ul>

<p>You’re going to learn all three ways, but, before you do, let’s find
out what you need to know about images before you can display them
with Pixi.</p>

<p><a id="user-content-loading"></a></p>

<h2><a id="user-content-loading-images-into-the-texture-cache" class="anchor" href="#loading-images-into-the-texture-cache" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Loading images into the texture cache</h2>

<p>Because Pixi renders the image on the GPU with WebGL, the image needs
to be in a format that the GPU can process. A WebGL-ready image is
called a <strong>texture</strong>. Before you can make a sprite display an image,
you need to convert an ordinary image file into a WebGL texture. To
keep everything working fast and efficiently under the hood, Pixi uses
a <strong>texture cache</strong> to store and reference all the textures your
sprites will need. The names of the textures are strings that match
the file locations of the images they refer to. That means if you have
a texture that was loaded from <code>"images/cat.png"</code>, you could find it in the texture cache like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c1">PIXI</span>.<span class="pl-smi">utils</span>.<span class="pl-smi">TextureCache</span>[<span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>];</pre></div>

<p>The textures are stored in a WebGL compatible format that’s efficient
for Pixi’s renderer to work with. You can then use Pixi’s <code>Sprite</code> class to make a new sprite using the texture.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> texture <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">utils</span>.<span class="pl-smi">TextureCache</span>[<span class="pl-s"><span class="pl-pds">"</span>images/anySpriteImage.png<span class="pl-pds">"</span></span>];
<span class="pl-k">var</span> sprite <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.Sprite</span>(texture);</pre></div>

<p>But how do you load the image file and convert it into a texture? Use
Pixi’s built-in <code>loader</code> object. </p>

<p>Pixi's powerful <code>loader</code> object is all you need to load any kind of image. 
Here’s how to use it to load an image and call a function called <code>setup</code> when the 
image has finished loading:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>
  .<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">"</span>images/anyImage.png<span class="pl-pds">"</span></span>)
  .<span class="pl-c1">load</span>(setup);

<span class="pl-k">function</span> <span class="pl-en">setup</span>() {
  <span class="pl-c">//This code will run when the loader has finished loading the image</span>
}</pre></div>

<p><a href="http://www.html5gamedevs.com/topic/16019-preload-all-textures/?p=90907">Pixi’s development team
recommends</a>
that if you use the loader, you should create the sprite by
referencing the the texture in the <code>loader</code>’s <code>resources</code> object, like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> sprite <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.Sprite</span>(
  <span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>.<span class="pl-smi">resources</span>[<span class="pl-s"><span class="pl-pds">"</span>images/anyImage.png<span class="pl-pds">"</span></span>].<span class="pl-smi">texture</span>
);</pre></div>

<p>Here’s an example of some complete code you could write to load an image, 
call the <code>setup</code> function, and create a sprite from the loaded image:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>
  .<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">"</span>images/anyImage.png<span class="pl-pds">"</span></span>)
  .<span class="pl-c1">load</span>(setup);

<span class="pl-k">function</span> <span class="pl-en">setup</span>() {
  <span class="pl-k">var</span> sprite <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.Sprite</span>(
    <span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>.<span class="pl-smi">resources</span>[<span class="pl-s"><span class="pl-pds">"</span>images/anyImage.png<span class="pl-pds">"</span></span>].<span class="pl-smi">texture</span>
  );
}</pre></div>

<p>This is the general format we’ll be using to load images and create
sprites in this tutorial.</p>

<p>You can load multiple images at single time by listing them with
chainable <code>add</code> methods, like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>
  .<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">"</span>images/imageOne.png<span class="pl-pds">"</span></span>)
  .<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">"</span>images/imageTwo.png<span class="pl-pds">"</span></span>)
  .<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">"</span>images/imageThree.png<span class="pl-pds">"</span></span>)
  .<span class="pl-c1">load</span>(setup);</pre></div>

<p>Better yet, just list all the files you want to load in
an array inside a single <code>add</code> method, like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>
  .<span class="pl-c1">add</span>([
    <span class="pl-s"><span class="pl-pds">"</span>images/imageOne.png<span class="pl-pds">"</span></span>,
    <span class="pl-s"><span class="pl-pds">"</span>images/imageTwo.png<span class="pl-pds">"</span></span>,
    <span class="pl-s"><span class="pl-pds">"</span>images/imageThree.png<span class="pl-pds">"</span></span>
  ])
  .<span class="pl-c1">load</span>(setup);</pre></div>

<p>The <code>loader</code> also lets you load JSON files, which you'll learn
about ahead.</p>

<p><a id="user-content-displaying"></a></p>

<h2><a id="user-content-displaying-sprites" class="anchor" href="#displaying-sprites" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Displaying sprites</h2>

<p>After you've loaded an image, and used it to make a sprite, there
are two more things you have to do before you can actually see it on
the canvas:</p>

<p>-1. You need to add the sprite to Pixi's <code>stage</code> with the <code>stage.addChild</code> method, like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(cat);</pre></div>

<p>The stage is the main container that holds all of your sprites.</p>

<p>-2. You need to tell Pixi's <code>renderer</code> to render the stage.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);</pre></div>

<p><strong>None of your sprites will be visible before you do these two
things</strong>.</p>

<p>Before we continue, let's look at a practical example of how to use what
you've just learnt to display a single image. In the <code>examples/images</code>
folder you'll find a 64 by 64 pixel PNG image of a cat.</p>

<p><a href="https://github.com/kittykatattack/learningPixi/blob/master/examples/images/cat.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/cat.png" alt="Basic display" style="max-width:100%;"></a></p>

<p>Here's all the JavaScript code you need to load the image, create a
sprite, and display it on Pixi's stage:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> stage <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.Container</span>(),
    renderer <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-en">autoDetectRenderer</span>(<span class="pl-c1">256</span>, <span class="pl-c1">256</span>);
<span class="pl-c1">document</span>.<span class="pl-c1">body</span>.<span class="pl-c1">appendChild</span>(<span class="pl-smi">renderer</span>.<span class="pl-smi">view</span>);

<span class="pl-c">//Use Pixi's built-in `loader` object to load an image</span>
<span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>
  .<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>)
  .<span class="pl-c1">load</span>(setup);

<span class="pl-c">//This `setup` function will run when the image has loaded</span>
<span class="pl-k">function</span> <span class="pl-en">setup</span>() {

  <span class="pl-c">//Create the `cat` sprite from the texture</span>
  <span class="pl-k">var</span> cat <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.Sprite</span>(
    <span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>.<span class="pl-smi">resources</span>[<span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>].<span class="pl-smi">texture</span>
  );

  <span class="pl-c">//Add the cat to the stage</span>
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(cat);

  <span class="pl-c">//Render the stage   </span>
  <span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);
}</pre></div>

<p>When this code runs, here's what you'll see:</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s02.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/02.png" alt="Cat on the stage" style="max-width:100%;"></a></p>

<p>Now we're getting somewhere!</p>

<p>If you ever need to remove a sprite from the stage, use the <code>removeChild</code> method:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">stage</span>.<span class="pl-c1">removeChild</span>(anySprite)</pre></div>

<p>But usually setting a sprite’s <code>visible</code> property to <code>false</code> will be a simpler and more efficient way of making sprites disappear.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">anySprite</span>.<span class="pl-smi">visible</span> <span class="pl-k">=</span> <span class="pl-c1">false</span>;</pre></div>

<p><a id="user-content-usingaliases"></a></p>

<h3><a id="user-content-using-aliases" class="anchor" href="#using-aliases" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Using aliases</h3>

<p>You can save yourself a little typing and make your code more readable
by creating short-form aliases for the Pixi objects and methods that you
use frequently. For example, is <code>PIXI.utils.TextureCache</code> too much to
type? I think so, especially in a big project where you might use it
it dozens of times. So, create a shorter alias that points to it, like
this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> TextureCache <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">utils</span>.<span class="pl-smi">TextureCache</span></pre></div>

<p>Then, use that alias in place of the original, like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> texture <span class="pl-k">=</span> TextureCache[<span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>];</pre></div>

<p>In addition to letting you write more succinct code, using aliases has
an extra benefit: it helps to buffer you slightly from Pixi's frequently
changing API. If Pixi's API changes in future
versions - which it will! - you just need to update these aliases to
Pixi object and methods in one place, at the beginning of
your program, instead of every instance where they're used throughout
your code. So when Pixi's development team decides they want to
rearrange the furniture a bit, you'll be one step ahead of them!</p>

<p>To see how to do this, let's re-write the code we wrote to load an image and display it,
using aliases for all the Pixi objects and methods.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c">//Aliases</span>
<span class="pl-k">var</span> Container <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">Container</span>,
    autoDetectRenderer <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">autoDetectRenderer</span>,
    loader <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>,
    resources <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>.<span class="pl-smi">resources</span>,
    Sprite <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">Sprite</span>;

<span class="pl-c">//Create a Pixi stage and renderer and add the </span>
<span class="pl-c">//renderer.view to the DOM</span>
<span class="pl-k">var</span> stage <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Container</span>(),
    renderer <span class="pl-k">=</span> <span class="pl-en">autoDetectRenderer</span>(<span class="pl-c1">256</span>, <span class="pl-c1">256</span>);
<span class="pl-c1">document</span>.<span class="pl-c1">body</span>.<span class="pl-c1">appendChild</span>(<span class="pl-smi">renderer</span>.<span class="pl-smi">view</span>);

<span class="pl-c">//load an image and run the `setup` function when it's done</span>
loader
  .<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>)
  .<span class="pl-c1">load</span>(setup);

<span class="pl-k">function</span> <span class="pl-en">setup</span>() {

  <span class="pl-c">//Create the `cat` sprite, add it to the stage, and render it</span>
  <span class="pl-k">var</span> cat <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(resources[<span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>].<span class="pl-smi">texture</span>);  
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(cat);
  <span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);
}
</pre></div>

<p>Most of the examples in this tutorial will use aliases for Pixi
objects that follow this same model. Unless otherwise stated, you can
assume that all the code examples use aliases like these.</p>

<p>This is all you need to know to start loading images and creating
sprites.</p>

<p><a id="user-content-alittlemoreaboutloadingthings"></a></p>

<h3><a id="user-content-a-little-more-about-loading-things" class="anchor" href="#a-little-more-about-loading-things" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>A little more about loading things</h3>

<p>The format I've shown you above is what I suggest you use as your
standard template for loading images and displaying sprites. So, you
can safely ignore the next few paragraphs and jump straight to the
next section, "Positioning sprites." But Pixi's <code>loader</code> object is
quite sophisticated and includes a few features that you should be
aware of, even if you don't use them on a regular basis. Let's
look at some of the most useful.</p>

<p><a id="user-content-makeaspritefromanordinaryjavascriptimageobject"></a></p>

<h4><a id="user-content-make-a-sprite-from-an-ordinary-javascript-image-object-or-canvas" class="anchor" href="#make-a-sprite-from-an-ordinary-javascript-image-object-or-canvas" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Make a sprite from an ordinary JavaScript Image object or Canvas</h4>

<p>For optimization and efficiency it’s always best to make a sprite from
a texture that’s been pre-loaded into Pixi’s texture cache. But if for
some reason you need to make a texture from a regular JavaScript
<code>Image</code>
object, you can do that using Pixi’s <code>BaseTexture</code> and <code>Texture</code>
classes:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> base <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.BaseTexture</span>(anyImageObject),
    texture <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.Texture</span>(base),
    sprite <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.Sprite</span>(texture);</pre></div>

<p>You can use <code>BaseTexture.fromCanvas</code> if you want to make a texture
from any existing canvas element:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> base <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.BaseTexture.fromCanvas</span>(anyCanvasElement),</pre></div>

<p>If you want to change the texture the sprite is displaying, use the
<code>texture</code> property. Set it to any <code>Texture</code> object, like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">anySprite</span>.<span class="pl-smi">texture</span> <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">utils</span>.<span class="pl-smi">TextureCache</span>[<span class="pl-s"><span class="pl-pds">"</span>anyTexture.png<span class="pl-pds">"</span></span>];</pre></div>

<p>You can use this technique to interactively change the sprite’s
appearance if something significant happens to it in the game.</p>

<p><a id="user-content-assigninganametoaloadingfile"></a></p>

<h4><a id="user-content-assigning-a-name-to-a-loading-file" class="anchor" href="#assigning-a-name-to-a-loading-file" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Assigning a name to a loading file</h4>

<p>It's possible to assign a unique name to each resource you want to
load. Just supply the name (a string) as the first argument in the
<code>add</code> method. For example, here's how to name an image of a cat as
<code>catImage</code>.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>
  .<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">"</span>catImage<span class="pl-pds">"</span></span>, <span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>)
  .<span class="pl-c1">load</span>(setup);</pre></div>

<p>This creates an object called <code>catImage</code> in <code>loader.resources</code>.
That means you can create a sprite by referencing the <code>catImage</code> object, like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> cat <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.Sprite</span>(<span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>.<span class="pl-smi">resources</span>.<span class="pl-smi">catImage</span>.<span class="pl-smi">texture</span>);</pre></div>

<p>However, I recommend you don't use this feature! That's because you'll
have to remember all names you gave each loaded files, as well as make sure
you don't accidentally use the same name more than once. Using the file path name, as we've done in previous
examples is simpler and less error prone.</p>

<p><a id="user-content-monitoringloadprogress"></a></p>

<h4><a id="user-content-monitoring-load-progress" class="anchor" href="#monitoring-load-progress" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Monitoring load progress</h4>

<p>Pixi's loader has a special <code>progress</code> event that will call a
customizable function that will run each time a file loads. <code>progress</code> events are called by the
<code>loader</code>'s <code>on</code> method, like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>.<span class="pl-en">on</span>(<span class="pl-s"><span class="pl-pds">"</span>progress<span class="pl-pds">"</span></span>, loadProgressHandler);</pre></div>

<p>Here's how to include the <code>on</code> method in the loading chain, and call
a user-definable function called <code>loadProgressHandler</code> each time a file loads.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>
  .<span class="pl-c1">add</span>([
    <span class="pl-s"><span class="pl-pds">"</span>images/one.png<span class="pl-pds">"</span></span>,
    <span class="pl-s"><span class="pl-pds">"</span>images/two.png<span class="pl-pds">"</span></span>,
    <span class="pl-s"><span class="pl-pds">"</span>images/three.png<span class="pl-pds">"</span></span>
  ])
  .<span class="pl-en">on</span>(<span class="pl-s"><span class="pl-pds">"</span>progress<span class="pl-pds">"</span></span>, loadProgressHandler)
  .<span class="pl-c1">load</span>(setup);

<span class="pl-k">function</span> <span class="pl-en">loadProgressHandler</span>() {
  <span class="pl-en">console</span>.<span class="pl-c1">log</span>(<span class="pl-s"><span class="pl-pds">"</span>loading<span class="pl-pds">"</span></span>); 
}

<span class="pl-k">function</span> <span class="pl-en">setup</span>() {
  <span class="pl-en">console</span>.<span class="pl-c1">log</span>(<span class="pl-s"><span class="pl-pds">"</span>setup<span class="pl-pds">"</span></span>);
}</pre></div>

<p>Each time one of the files loads, the progress event calls
<code>loadProgressHandler</code> to display "loading" in the console. When all three files have loaded, the <code>setup</code>
function will run. Here's the output of the above code in the console:</p>

<div class="highlight highlight-source-js"><pre>loading
loading
loading
setup</pre></div>

<p>That's neat, but it gets better. You can also find out exactly which file
has loaded and what percentage of overall files are have currently
loaded. You can do this by adding optional <code>loader</code> and
<code>resource</code> parameters to the <code>loadProgressHandler</code>, like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">loadProgressHandler</span>(<span class="pl-smi">loader</span>, <span class="pl-smi">resource</span>) { <span class="pl-c">//...</span></pre></div>

<p>You can then use <code>resource.url</code> to find the file that's currently
loaded. (Use <code>resource.name</code> if you want to find the optional name
that you might have assigned to the file, as the first argument in the
<code>add</code> method.) And you can use <code>loader.progress</code> to find what
percentage of total resources have currently loaded. Here's some code
that does just that.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>
  .<span class="pl-c1">add</span>([
    <span class="pl-s"><span class="pl-pds">"</span>images/one.png<span class="pl-pds">"</span></span>,
    <span class="pl-s"><span class="pl-pds">"</span>images/two.png<span class="pl-pds">"</span></span>,
    <span class="pl-s"><span class="pl-pds">"</span>images/three.png<span class="pl-pds">"</span></span>
  ])
  .<span class="pl-en">on</span>(<span class="pl-s"><span class="pl-pds">"</span>progress<span class="pl-pds">"</span></span>, loadProgressHandler)
  .<span class="pl-c1">load</span>(setup);

<span class="pl-k">function</span> <span class="pl-en">loadProgressHandler</span>(<span class="pl-smi">loader</span>, <span class="pl-smi">resource</span>) {

  <span class="pl-c">//Display the file `url` currently being loaded</span>
  <span class="pl-en">console</span>.<span class="pl-c1">log</span>(<span class="pl-s"><span class="pl-pds">"</span>loading: <span class="pl-pds">"</span></span> <span class="pl-k">+</span> <span class="pl-smi">resource</span>.<span class="pl-smi">url</span>); 

  <span class="pl-c">//Display the precentage of files currently loaded</span>
  <span class="pl-en">console</span>.<span class="pl-c1">log</span>(<span class="pl-s"><span class="pl-pds">"</span>progress: <span class="pl-pds">"</span></span> <span class="pl-k">+</span> <span class="pl-smi">loader</span>.<span class="pl-smi">progress</span> <span class="pl-k">+</span> <span class="pl-s"><span class="pl-pds">"</span>%<span class="pl-pds">"</span></span>); 

  <span class="pl-c">//If you gave your files names as the first argument </span>
  <span class="pl-c">//of the `add` method, you can access them like this</span>
  <span class="pl-c">//console.log("loading: " + resource.name);</span>
}

<span class="pl-k">function</span> <span class="pl-en">setup</span>() {
  <span class="pl-en">console</span>.<span class="pl-c1">log</span>(<span class="pl-s"><span class="pl-pds">"</span>All files loaded<span class="pl-pds">"</span></span>);
}</pre></div>

<p>Here's what this code will display in the console when it runs:</p>

<div class="highlight highlight-source-js"><pre>loading<span class="pl-k">:</span> images<span class="pl-k">/</span><span class="pl-smi">one</span>.<span class="pl-smi">png</span>
progress<span class="pl-k">:</span> <span class="pl-c1">33.333333333333336</span><span class="pl-k">%</span>
loading<span class="pl-k">:</span> images<span class="pl-k">/</span><span class="pl-smi">two</span>.<span class="pl-smi">png</span>
progress<span class="pl-k">:</span> <span class="pl-c1">66.66666666666667</span><span class="pl-k">%</span>
loading<span class="pl-k">:</span> images<span class="pl-k">/</span><span class="pl-smi">three</span>.<span class="pl-smi">png</span>
progress<span class="pl-k">:</span> <span class="pl-c1">100</span><span class="pl-k">%</span>
All files loaded</pre></div>

<p>That's really cool, because you could use this as the basis for
creating a loading progress bar. </p>

<p>(Note: There are additional properties you can access on the
<code>resource</code> object. <code>resource.error</code> will tell you of any possible
error that happened while
trying to load a file. <code>resource.data</code> lets you
access the file's raw binary data.)</p>

<p><a id="user-content-moreaboutpixisloader"></a></p>

<h4><a id="user-content-more-about-pixis-loader" class="anchor" href="#more-about-pixis-loader" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>More about Pixi's loader</h4>

<p>Pixi's loader is ridiculously feature-rich and configurable. Let's
take a quick bird's-eye view of it's usage to
get you started.</p>

<p>The loader's chainable <code>add</code> method takes 4 basic arguments:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-en">add</span>(name, url, optionObject, callbackFunction)</pre></div>

<p>Here's what the loader's source code documentation has to say about
these parameters:</p>

<p><code>name</code> (string): The name of the resource to load. If it's not passed, the <code>url</code> is used.
<code>url</code> (string): The url for this resource, relative to the <code>baseUrl</code> of the loader.
<code>options</code> (object literal): The options for the load.
<code>options.crossOrigin</code> (Boolean): Is the request cross-origin? The default is to determine automatically.
<code>options.loadType</code>: How should the resource be loaded? The default value is <code>Resource.LOAD_TYPE.XHR</code>.
<code>options.xhrType</code>: How should the data being loaded be interpreted
when using XHR? The default value is <code>Resource.XHR_RESPONSE_TYPE.DEFAULT</code>
<code>callbackFunction</code>: The function to call when this specific resource completes loading.</p>

<p>The only one of these arguments that's required is the <code>url</code> (the file that you want to
load.) </p>

<p>Here are some examples of some ways you could use the <code>add</code>
method to load files. These first ones are what the docs call the loader's "normal syntax":</p>

<div class="highlight highlight-source-js"><pre>.<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">'</span>key<span class="pl-pds">'</span></span>, <span class="pl-s"><span class="pl-pds">'</span>http://...<span class="pl-pds">'</span></span>, <span class="pl-k">function</span> () {})
.<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">'</span>http://...<span class="pl-pds">'</span></span>, <span class="pl-k">function</span> () {})
.<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">'</span>http://...<span class="pl-pds">'</span></span>)</pre></div>

<p>And these are examples of the loader's "object syntax":</p>

<div class="highlight highlight-source-js"><pre>.<span class="pl-c1">add</span>({
  name<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">'</span>key2<span class="pl-pds">'</span></span>,
  url<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">'</span>http://...<span class="pl-pds">'</span></span>
}, <span class="pl-k">function</span> () {})

.<span class="pl-c1">add</span>({
  url<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">'</span>http://...<span class="pl-pds">'</span></span>
}, <span class="pl-k">function</span> () {})

.<span class="pl-c1">add</span>({
  name<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">'</span>key3<span class="pl-pds">'</span></span>,
  url<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">'</span>http://...<span class="pl-pds">'</span></span>
  <span class="pl-en">onComplete</span><span class="pl-k">:</span> <span class="pl-k">function</span> () {}
})

.<span class="pl-c1">add</span>({
  url<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">'</span>https://...<span class="pl-pds">'</span></span>,
  <span class="pl-en">onComplete</span><span class="pl-k">:</span> <span class="pl-k">function</span> () {},
  crossOrigin<span class="pl-k">:</span> <span class="pl-c1">true</span>
})</pre></div>

<p>You can also pass the <code>add</code> method an array of objects, or urls, or
both:</p>

<div class="highlight highlight-source-js"><pre>.<span class="pl-c1">add</span>([
  {name<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">'</span>key4<span class="pl-pds">'</span></span>, url<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">'</span>http://...<span class="pl-pds">'</span></span>, <span class="pl-en">onComplete</span><span class="pl-k">:</span> <span class="pl-k">function</span> () {} },
  {url<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">'</span>http://...<span class="pl-pds">'</span></span>, <span class="pl-en">onComplete</span><span class="pl-k">:</span> <span class="pl-k">function</span> () {} },
  <span class="pl-s"><span class="pl-pds">'</span>http://...<span class="pl-pds">'</span></span>
]);</pre></div>

<p>(Note: If you ever need to reset the loader to load a new batch of files, call the
loader's <code>reset</code> method: <code>PIXI.loader.reset();</code>)</p>

<p>Pixi's loader has many more advanced features, including options to
let you load and parse binary files of all types. This is not
something you'll need to do on a day-to-day basis, and way outside the
scope of this tutorial, so <a href="https://github.com/englercj/resource-loader">make sure to check out the loader's GitHub repository
for more information</a>.</p>

<p><a id="user-content-positioning"></a></p>

<h2><a id="user-content-positioning-sprites" class="anchor" href="#positioning-sprites" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Positioning sprites</h2>

<p>Now that you know how to create and display sprites, let's find out
how to position and resize them.</p>

<p>In the earlier example the cat sprite was added to stage at
the top left corner. The cat has an <code>x</code> position of
0 and a <code>y</code> position of 0. You can change the position of the cat by
changing the values of its <code>x</code> and <code>y</code> properties. Here's how you can center the cat in the stage by
setting its <code>x</code> and <code>y</code> property values to 96.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">cat</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">96</span>;
<span class="pl-smi">cat</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">96</span>;</pre></div>

<p>Add these two lines of code anywhere inside the <code>setup</code>
function, after you've created the sprite. </p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">setup</span>() {

  <span class="pl-c">//Create the `cat` sprite</span>
  <span class="pl-k">var</span> cat <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(resources[<span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>].<span class="pl-smi">texture</span>);

  <span class="pl-c">//Change the sprite's position</span>
  <span class="pl-smi">cat</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">96</span>;
  <span class="pl-smi">cat</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">96</span>;

  <span class="pl-c">//Add the cat to the stage so you can see it</span>
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(cat);

  <span class="pl-c">//Render the stage</span>
  <span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);
}</pre></div>

<p>(Note: In this example,
<code>Sprite</code> is an alias for <code>PIXI.Sprite</code>, <code>TextureCache</code> is an
alias for <code>PIXI.utils.TextureCache</code>, and <code>resources</code> is an alias for
<code>PIXI.loader.resources</code> as described earlier. I'll be
using alias that follow this same format for all Pixi objects and
methods in the example code from now on.)</p>

<p>These two new lines of code will move the cat 96 pixels to the right,
and 96 pixels down. Here's the result:</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s03.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/03.png" alt="Cat centered on the stage" style="max-width:100%;"></a></p>

<p>The cat's top left corner (its left ear) represents its <code>x</code> and <code>y</code>
anchor point. To make the cat move to the right, increase the
value of its <code>x</code> property. To make the cat move down, increase the
value of its <code>y</code> property. If the cat has an <code>x</code> value of 0, it will be at
the very left side of the stage. If it has a <code>y</code> value of 0, it will
be at the very top of the stage.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s04.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/04.png" alt="Cat centered on the stage - diagram" style="max-width:100%;"></a></p>

<p>Instead of setting the sprite's <code>x</code> and <code>y</code> properties independently,
you can set them together in a single line of code, like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">sprite</span>.<span class="pl-smi">position</span>.<span class="pl-c1">set</span>(x, y)</pre></div>

<p><a id="user-content-sizenscale"></a></p>

<h2><a id="user-content-size-and-scale" class="anchor" href="#size-and-scale" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Size and scale</h2>

<p>You can change a sprite's size by setting its <code>width</code> and <code>height</code>
properties. Here's how to give the cat a <code>width</code> of 80 pixels and a <code>height</code> of
120 pixels.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">cat</span>.<span class="pl-c1">width</span> <span class="pl-k">=</span> <span class="pl-c1">80</span>;
<span class="pl-smi">cat</span>.<span class="pl-c1">height</span> <span class="pl-k">=</span> <span class="pl-c1">120</span>;</pre></div>

<p>Add those two lines of code to the <code>setup</code> function, like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">setup</span>() {

  <span class="pl-c">//Create the `cat` sprite</span>
  <span class="pl-k">var</span> cat <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(resources[<span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>].<span class="pl-smi">texture</span>);

  <span class="pl-c">//Change the sprite's position</span>
  <span class="pl-smi">cat</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">96</span>;
  <span class="pl-smi">cat</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">96</span>;

  <span class="pl-c">//Change the sprite's size</span>
  <span class="pl-smi">cat</span>.<span class="pl-c1">width</span> <span class="pl-k">=</span> <span class="pl-c1">80</span>;
  <span class="pl-smi">cat</span>.<span class="pl-c1">height</span> <span class="pl-k">=</span> <span class="pl-c1">120</span>;

  <span class="pl-c">//Add the cat to the stage so you can see it</span>
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(cat);
}</pre></div>

<p>Here's the result:</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s05.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/05.png" alt="Cat's height and width changed" style="max-width:100%;"></a></p>

<p>You can see that the cat's position (its top left corner) didn't
change, only its width and height.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s06.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/06.png" alt="Cat's height and width changed - diagram" style="max-width:100%;"></a></p>

<p>Sprites also have <code>scale.x</code> and <code>scale.y</code> properties that change the
sprite's width and height proportionately. Here's how to set the cat's
scale to half size:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">cat</span>.<span class="pl-smi">scale</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">0.5</span>;
<span class="pl-smi">cat</span>.<span class="pl-smi">scale</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">0.5</span>;</pre></div>

<p>Scale values are numbers between 0 and 1 that represent a
percentage of the sprite's size. 1 means 100% (full size), while
0.5 means 50% (half size). You can double the sprite's size by setting
its scale values to 2, like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">cat</span>.<span class="pl-smi">scale</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">2</span>;
<span class="pl-smi">cat</span>.<span class="pl-smi">scale</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">2</span>;</pre></div>

<p>Pixi has an alternative, concise way for you set sprite's scale in one
line of code using the <code>scale.set</code> method.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">cat</span>.<span class="pl-smi">scale</span>.<span class="pl-c1">set</span>(<span class="pl-c1">0.5</span>, <span class="pl-c1">0.5</span>);</pre></div>

<p>If that appeals to you, use it!</p>

<p><a id="user-content-rotation"></a></p>

<h2><a id="user-content-rotation" class="anchor" href="#rotation" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Rotation</h2>

<p>You can make a sprite rotate by setting its <code>rotation</code> property to a
value in <a href="http://www.mathsisfun.com/geometry/radians.html">radians</a>.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">cat</span>.<span class="pl-smi">rotation</span> <span class="pl-k">=</span> <span class="pl-c1">0.5</span>;</pre></div>

<p>But around which point does that rotation happen?</p>

<p>You've seen that a sprite's top left corner represents its <code>x</code> and <code>y</code> position. That point is
called the <strong>anchor point</strong>. If you set the sprite’s <code>rotation</code>
property to something like <code>0.5</code>, the rotation will happen <em>around the
sprite’s anchor point</em>.
This diagram shows what effect this will have on our cat sprite.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s07.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/07.png" alt="Rotation around anchor point - diagram" style="max-width:100%;"></a></p>

<p>You can see that the anchor point, the cat’s left ear, is the center of the imaginary circle around which the cat is rotating.
What if you want the sprite to rotate around its center? Change the
sprite’s <code>anchor</code> point so that it’s centered inside the sprite, like
this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">cat</span>.<span class="pl-smi">anchor</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">0.5</span>;
<span class="pl-smi">cat</span>.<span class="pl-smi">anchor</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">0.5</span>;</pre></div>

<p>The <code>anchor.x</code> and <code>anchor.y</code> values represent a percentage of the texture’s dimensions, from 0 to 1 (0%
to 100%). Setting it to 0.5 centers the texture over the point. The location of the point
itself won’t change, just the way the texture is positioned over it.</p>

<p>This next diagram shows what happens to the rotated sprite if you center its anchor point.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s08.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/08.png" alt="Rotation around centered anchor point - diagram" style="max-width:100%;"></a></p>

<p>You can see that the sprite’s texture shifts up and to the left. This
is an important side-effect to remember!</p>

<p>Just like with <code>position</code> and <code>scale</code>, you can set the anchor’s x and
y values with one line of code like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">sprite</span>.<span class="pl-smi">anchor</span>.<span class="pl-c1">set</span>(x, y)</pre></div>

<p>Sprites also have a <code>pivot</code> property which works in a similar way to
<code>anchor</code>. <code>pivot</code> sets the position
of the sprite's x/y origin point. If you change the pivot point and
then rotate the sprite, it will
rotate around that origin point. For example, the following code will
set the sprite's <code>pivot.x</code> point to 32, and its <code>pivot.y</code> point to 32</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">cat</span>.<span class="pl-smi">pivot</span>.<span class="pl-c1">set</span>(<span class="pl-c1">32</span>, <span class="pl-c1">32</span>)</pre></div>

<p>Assuming that the sprite is 64x64 pixels, the sprite will now rotate
around its center point. But remember: if you change a sprite's pivot
point, you've also changed its x/y origin point. </p>

<p>So, what's the difference between <code>anchor</code> and <code>pivot</code>? They're really
similar! <code>anchor</code> shifts the origin point of the sprite's image texture, using a 0 to 1 normalized value.
<code>pivot</code> shifts the origin of the sprite's x and y point, using pixel
values. Which should you use? It's up to you. Just play around
with both of them and see which you prefer.</p>

<p><a id="user-content-tileset"></a></p>

<h2><a id="user-content-make-a-sprite-from-a-tileset-sub-image" class="anchor" href="#make-a-sprite-from-a-tileset-sub-image" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Make a sprite from a tileset sub-image</h2>

<p>You now know how to make a sprite from a single image file. But, as a
game designer, you’ll usually be making your sprites using
<strong>tilesets</strong> (also known as <strong>spritesheets</strong>.) Pixi has some convenient built-in ways to help you do this.
A tileset is a single image file that contains sub-images. The sub-images
represent all the graphics you want to use in your game. Here's an
example of a tileset image that contains game characters and game
objects as sub-images.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s09.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/09.png" alt="An example tileset" style="max-width:100%;"></a></p>

<p>The entire tileset is 192 by 192 pixels. Each image is in a its own 32 by 32
pixel grid cell. Storing and accessing all your game graphics on a
tileset is a very
processor and memory efficient way to work with graphics, and Pixi is
optimized for them.</p>

<p>You can capture a sub-image from a tileset by defining a rectangular
area
that's the same size and position as the sub-image you want to
extract. Here's an example of the rocket sub-image that’s been extracted from
the tileset.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s10.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/10.png" alt="Rocket extracted from tileset" style="max-width:100%;"></a></p>

<p>Let's look at the code that does this. First, load the <code>tileset.png</code> image
with Pixi’s <code>loader</code>, just as you've done in earlier examples.</p>

<div class="highlight highlight-source-js"><pre>loader
  .<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">"</span>images/tileset.png<span class="pl-pds">"</span></span>)
  .<span class="pl-c1">load</span>(setup);</pre></div>

<p>Next, when the image has loaded, use a rectangular sub-section of the tileset to create the
sprite’s image. Here's the code that extracts the sub image, creates
the rocket sprite, and positions and displays it on the canvas.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">setup</span>() {

  <span class="pl-c">//Create the `tileset` sprite from the texture</span>
  <span class="pl-k">var</span> texture <span class="pl-k">=</span> TextureCache[<span class="pl-s"><span class="pl-pds">"</span>images/tileset.png<span class="pl-pds">"</span></span>];

  <span class="pl-c">//Create a rectangle object that defines the position and</span>
  <span class="pl-c">//size of the sub-image you want to extract from the texture</span>
  <span class="pl-k">var</span> rectangle <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Rectangle</span>(<span class="pl-c1">192</span>, <span class="pl-c1">128</span>, <span class="pl-c1">64</span>, <span class="pl-c1">64</span>);

  <span class="pl-c">//Tell the texture to use that rectangular section</span>
  <span class="pl-smi">texture</span>.<span class="pl-c1">frame</span> <span class="pl-k">=</span> rectangle;

  <span class="pl-c">//Create the sprite from the texture</span>
  <span class="pl-k">var</span> rocket <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(texture);

  <span class="pl-c">//Position the rocket sprite on the canvas</span>
  <span class="pl-smi">rocket</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">32</span>;
  <span class="pl-smi">rocket</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">32</span>;

  <span class="pl-c">//Add the rocket to the stage</span>
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(rocket);

  <span class="pl-c">//Render the stage   </span>
  <span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);
}</pre></div>

<p>How does this work?</p>

<p>Pixi has a built-in <code>Rectangle</code> object (PIXI.Rectangle) that is a general-purpose
object for defining rectangular shapes. It takes four arguments. The
first two arguments define the rectangle's <code>x</code> and <code>y</code> position. The
last two define its <code>width</code> and <code>height</code>. Here's the format
for defining a new <code>Rectangle</code> object.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> rectangle <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.Rectangle</span>(x, y, width, height);</pre></div>

<p>The rectangle object is just a <em>data object</em>; it's up to you to decide how you want to use it. In
our example we're using it to define the position and area of the
sub-image on the tileset that we want to extract. Pixi textures have a useful
property called <code>frame</code> that can be set to any <code>Rectangle</code> objects. 
The <code>frame</code> crops the texture to the dimensions of the <code>Rectangle</code>.
Here's how to use <code>frame</code>
to crop the texture to the size and position of the rocket.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> rectangle <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Rectangle</span>(<span class="pl-c1">192</span>, <span class="pl-c1">128</span>, <span class="pl-c1">64</span>, <span class="pl-c1">64</span>);
<span class="pl-smi">texture</span>.<span class="pl-c1">frame</span> <span class="pl-k">=</span> rectangle;</pre></div>

<p>You can then use that cropped texture to create the sprite:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> rocket <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(texture);</pre></div>

<p>And that's how it works!</p>

<p>Because making sprite textures from a tileset
is something you’ll do with great frequency, Pixi has a more convenient way
to help you do this - let's find out what that is next.</p>

<p><a id="user-content-textureatlas"></a></p>

<h2><a id="user-content-using-a-texture-atlas" class="anchor" href="#using-a-texture-atlas" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Using a texture atlas</h2>

<p>If you’re working on a big, complex game, you’ll want a fast and
efficient way to create sprites from tilesets. This is where a
<strong>texture atlas</strong> becomes really useful. A texture atlas is a JSON
data file that contains the positions and sizes of sub-images on a
matching tileset PNG image. If you use a texture atlas, all you need
to know about the sub-image you want to display is its name. You
can arrange your tileset images in any order and the JSON file
will keep track of their sizes and positions for you. This is
really convenient because it means the sizes and positions of
tileset images aren’t hard-coded into your game program. If you
make changes to the tileset, like adding images, resizing them,
or removing them, just re-publish the JSON file and your game will
use that data to display the correct images. You won’t have to
make any changes to your game code.</p>

<p>Pixi is compatible with a standard JSON texture atlas format that is
output by a popular software tool called <a href="https://www.codeandweb.com/texturepacker">Texture
Packer</a>. Texture Packer’s
“Essential” license is free. Let’s find out how to use it to make a
texture atlas, and load the atlas into Pixi. (You don’t have to use
Texture Packer. Similar tools, like <a href="http://renderhjs.net/shoebox/">Shoebox</a> or <a href="https://github.com/krzysztof-o/spritesheet.js/">spritesheet.js</a>, output PNG and JSON files
in a standard format that is compatible with Pixi.)</p>

<p>First, start with a collection of individual image files that you'd
like to use in your game.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s11.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/11.png" alt="Image files" style="max-width:100%;"></a></p>

<p>(All the images in this section were created by Lanea Zimmerman. You
can find more of her artwork
<a href="http://opengameart.org/users/sharm">here</a>.
Thanks, Lanea!)</p>

<p>Next, open Texture Packer and choose <strong>JSON Hash</strong> as the framework
type. Drag your images into Texture Packer's workspace. (You can
also point Texture Packer to any folder that contains your images.)
It will automatically arrange the images on a single tileset image, and give them names that match their original image names.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s12.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/12.png" alt="Image files" style="max-width:100%;"></a></p>

<p>(If you're using the free version of
Texture Packer, set <strong>Algorithm</strong> to <code>Basic</code>, set <strong>Trim mode</strong> to
<code>None</code>, set <strong>Size constraints</strong> to <code>Any size</code> and slide the <strong>PNG Opt
Level</strong> all the way to the left to <code>0</code>. These are the basic
settings that will allow the free version of Texture Packer to create
your files without any warnings or errors.)</p>

<p>When you’re done, click the <strong>Publish</strong> button. Choose the file name and
location, and save the published files. You’ll end up with 2 files: a
PNG file and a JSON file. In this example my file names are
<code>treasureHunter.json</code> and <code>treasureHunter.png</code>. To make your life easier,
just keep both files in your project’s <code>images</code> folder. (You can think
of the JSON file as extra metadata for the image file, so it makes
sense to keep both files in the same folder.)
The JSON file describes the name, size and position of each of the
sub-images
in the tileset. Here’s an excerpt that describes the blob monster
sub-image.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-s"><span class="pl-pds">"</span>blob.png<span class="pl-pds">"</span></span><span class="pl-k">:</span>
{
    <span class="pl-s"><span class="pl-pds">"</span>frame<span class="pl-pds">"</span></span><span class="pl-k">:</span> {<span class="pl-s"><span class="pl-pds">"</span>x<span class="pl-pds">"</span></span><span class="pl-k">:</span><span class="pl-c1">55</span>,<span class="pl-s"><span class="pl-pds">"</span>y<span class="pl-pds">"</span></span><span class="pl-k">:</span><span class="pl-c1">2</span>,<span class="pl-s"><span class="pl-pds">"</span>w<span class="pl-pds">"</span></span><span class="pl-k">:</span><span class="pl-c1">32</span>,<span class="pl-s"><span class="pl-pds">"</span>h<span class="pl-pds">"</span></span><span class="pl-k">:</span><span class="pl-c1">24</span>},
    <span class="pl-s"><span class="pl-pds">"</span>rotated<span class="pl-pds">"</span></span><span class="pl-k">:</span> <span class="pl-c1">false</span>,
    <span class="pl-s"><span class="pl-pds">"</span>trimmed<span class="pl-pds">"</span></span><span class="pl-k">:</span> <span class="pl-c1">false</span>,
    <span class="pl-s"><span class="pl-pds">"</span>spriteSourceSize<span class="pl-pds">"</span></span><span class="pl-k">:</span> {<span class="pl-s"><span class="pl-pds">"</span>x<span class="pl-pds">"</span></span><span class="pl-k">:</span><span class="pl-c1">0</span>,<span class="pl-s"><span class="pl-pds">"</span>y<span class="pl-pds">"</span></span><span class="pl-k">:</span><span class="pl-c1">0</span>,<span class="pl-s"><span class="pl-pds">"</span>w<span class="pl-pds">"</span></span><span class="pl-k">:</span><span class="pl-c1">32</span>,<span class="pl-s"><span class="pl-pds">"</span>h<span class="pl-pds">"</span></span><span class="pl-k">:</span><span class="pl-c1">24</span>},
    <span class="pl-s"><span class="pl-pds">"</span>sourceSize<span class="pl-pds">"</span></span><span class="pl-k">:</span> {<span class="pl-s"><span class="pl-pds">"</span>w<span class="pl-pds">"</span></span><span class="pl-k">:</span><span class="pl-c1">32</span>,<span class="pl-s"><span class="pl-pds">"</span>h<span class="pl-pds">"</span></span><span class="pl-k">:</span><span class="pl-c1">24</span>},
    <span class="pl-s"><span class="pl-pds">"</span>pivot<span class="pl-pds">"</span></span><span class="pl-k">:</span> {<span class="pl-s"><span class="pl-pds">"</span>x<span class="pl-pds">"</span></span><span class="pl-k">:</span><span class="pl-c1">0.5</span>,<span class="pl-s"><span class="pl-pds">"</span>y<span class="pl-pds">"</span></span><span class="pl-k">:</span><span class="pl-c1">0.5</span>}
},</pre></div>

<p>The <code>treasureHunter.json</code> file also contains “dungeon.png”,
“door.png”, "exit.png", and "explorer.png" properties each with
similar data. Each of these sub-images are called <strong>frames</strong>. Having
this data is really helpful because now you don’t need to know the
size and position of each sub-image in the tileset. All you need to
know is the sprite’s <strong>frame id</strong>. The frame id is just the name
of the original image file, like "blob.png" or "explorer.png".</p>

<p>Among the many advantages to using a texture atlas is that you can
easily add 2 pixels of padding around each image (Texture Packer does
this by default.) This is important to prevent the possibility of
<strong>texture bleed</strong>. Texture bleed is an effect that happens when the
edge of an adjacent image on the tileset appears next to a sprite.
This happens because of the way your computer's GPU (Graphics
Processing Unit) decides how to round fractional pixels values. Should
it round them up or down? This will be different for each GPU.
Leaving 1 or 2 pixels spacing around images on a tilseset makes all
images display consistently.</p>

<p>(Note: If you have two pixels of padding around a graphic, and you still notice a strange "off by one pixel" glitch in the
way Pixi is displaying it, try changing the texture's scale mode
algorithm. Here's how: <code>texture.baseTexture.scaleMode =
PIXI.SCALE_MODES.NEAREST;</code>. These glitches can sometimes happen
because of GPU floating point rounding errors.)</p>

<p>Now that you know how to create a texture atlas, let's find out how to
load it into your game code.</p>

<p><a id="user-content-loadingatlas"></a></p>

<h2><a id="user-content-loading-the-texture-atlas" class="anchor" href="#loading-the-texture-atlas" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Loading the texture atlas</h2>

<p>To get the texture atlas into Pixi, load it using Pixi’s
<code>loader</code>. If the JSON file was made with Texture Packer, the
<code>loader</code> will interpret the data and create a texture from each
frame on the tileset automatically.  Here’s how to use the <code>loader</code> to load the <code>treasureHunter.json</code>
file. When it has loaded, the <code>setup</code> function will run.</p>

<div class="highlight highlight-source-js"><pre>loader
  .<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">"</span>images/treasureHunter.json<span class="pl-pds">"</span></span>)
  .<span class="pl-c1">load</span>(setup);</pre></div>

<p>Each image on the tileset is now an individual texture in Pixi’s
cache. You can access each texture in the cache with the same name it
had in Texture Packer (“blob.png”, “dungeon.png”, “explorer.png”,
etc.).</p>

<p><a id="user-content-creatingsprites"></a></p>

<h2><a id="user-content-creating-sprites-from-a-loaded-texture-atlas" class="anchor" href="#creating-sprites-from-a-loaded-texture-atlas" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Creating sprites from a loaded texture atlas</h2>

<p>Pixi gives you three general ways to create a sprite from a texture atlas:</p>

<ol>
<li> Using <code>TextureCache</code>:</li>
</ol>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> texture <span class="pl-k">=</span> TextureCache[<span class="pl-s"><span class="pl-pds">"</span>frameId.png<span class="pl-pds">"</span></span>],
    sprite <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(texture);</pre></div>

<ol>
<li> If you’ve used Pixi’s <code>loader</code> to load the texture atlas, use the 
loader’s <code>resources</code>:</li>
</ol>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> sprite <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(
  resources[<span class="pl-s"><span class="pl-pds">"</span>images/treasureHunter.json<span class="pl-pds">"</span></span>].<span class="pl-smi">textures</span>[<span class="pl-s"><span class="pl-pds">"</span>frameId.png<span class="pl-pds">"</span></span>]
);</pre></div>

<ol>
<li>That’s way too much to typing to have to do just to create a sprite! 
So I suggest you create an alias called <code>id</code> that points to texture’s 
altas’s <code>textures</code> object, like this:</li>
</ol>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> id <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>.<span class="pl-smi">resources</span>[<span class="pl-s"><span class="pl-pds">"</span>images/treasureHunter.json<span class="pl-pds">"</span></span>].<span class="pl-smi">textures</span>; </pre></div>

<p>Then you can just create each new sprite like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">let</span> sprite <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(id[<span class="pl-s"><span class="pl-pds">"</span>frameId.png<span class="pl-pds">"</span></span>]);</pre></div>

<p>Much better!</p>

<p>Here's how you could use these three different sprite creation
techniques in the <code>setup</code> function to create and display the
<code>dungeon</code>, <code>explorer</code>, and <code>treasure</code> sprites.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c">//Define variables that might be used in more </span>
<span class="pl-c">//than one function</span>
<span class="pl-k">var</span> dungeon, explorer, treasure, door, id;

<span class="pl-k">function</span> <span class="pl-en">setup</span>() {

  <span class="pl-c">//There are 3 ways to make sprites from textures atlas frames</span>

  <span class="pl-c">//1. Access the `TextureCache` directly</span>
  <span class="pl-k">var</span> dungeonTexture <span class="pl-k">=</span> TextureCache[<span class="pl-s"><span class="pl-pds">"</span>dungeon.png<span class="pl-pds">"</span></span>];
  dungeon <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(dungeonTexture);
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(dungeon);

  <span class="pl-c">//2. Access the texture using throuhg the loader's `resources`:</span>
  explorer <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(
    resources[<span class="pl-s"><span class="pl-pds">"</span>images/treasureHunter.json<span class="pl-pds">"</span></span>].<span class="pl-smi">textures</span>[<span class="pl-s"><span class="pl-pds">"</span>explorer.png<span class="pl-pds">"</span></span>]
  );
  <span class="pl-smi">explorer</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">68</span>;

  <span class="pl-c">//Center the explorer vertically</span>
  <span class="pl-smi">explorer</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-smi">stage</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span> <span class="pl-k">-</span> <span class="pl-smi">explorer</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(explorer);

  <span class="pl-c">//3. Create an optional alias called `id` for all the texture atlas </span>
  <span class="pl-c">//frame id textures.</span>
  id <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>.<span class="pl-smi">resources</span>[<span class="pl-s"><span class="pl-pds">"</span>images/treasureHunter.json<span class="pl-pds">"</span></span>].<span class="pl-smi">textures</span>; 

  <span class="pl-c">//Make the treasure box using the alias</span>
  treasure <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(id[<span class="pl-s"><span class="pl-pds">"</span>treasure.png<span class="pl-pds">"</span></span>]);
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(treasure);

  <span class="pl-c">//Position the treasure next to the right edge of the canvas</span>
  <span class="pl-smi">treasure</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-smi">stage</span>.<span class="pl-c1">width</span> <span class="pl-k">-</span> <span class="pl-smi">treasure</span>.<span class="pl-c1">width</span> <span class="pl-k">-</span> <span class="pl-c1">48</span>;
  <span class="pl-smi">treasure</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-smi">stage</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span> <span class="pl-k">-</span> <span class="pl-smi">treasure</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(treasure);


  <span class="pl-c">//Render the stage</span>
  <span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);
}</pre></div>

<p>Here's what this code displays:</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s13.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/13.png" alt="Explorer, dungeon and treasure" style="max-width:100%;"></a></p>

<p>The stage dimensions are 512 by 512 pixels, and you can see in the
code above that the <code>stage.height</code> and <code>stage.width</code> properties are used
to align the sprites. Here's how the <code>explorer</code>'s <code>y</code> position is
vertically centered:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">explorer</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-smi">stage</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span> <span class="pl-k">-</span> <span class="pl-smi">explorer</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;</pre></div>

<p>Learning to create and display sprites using a texture atlas is an
important benchmark. So before we continue, let's take a look at the
code you
could write to add the remaining
sprites: the <code>blob</code>s and <code>exit</code> door, so that you can produce a scene
that looks like this:</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s14.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/14.png" alt="All the texture atlas sprites" style="max-width:100%;"></a></p>

<p>Here's the entire code that does all this. I've also included the HTML
code so you can see everything in its proper context.
(You'll find this working code in the
<code>examples/spriteFromTextureAtlas.html</code> file in this repository.)
Notice that the <code>blob</code> sprites are created and added to the stage in a
loop, and assigned random positions.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">&lt;</span><span class="pl-k">!</span>doctype html<span class="pl-k">&gt;</span>
<span class="pl-k">&lt;</span>meta charset<span class="pl-k">=</span><span class="pl-s"><span class="pl-pds">"</span>utf-8<span class="pl-pds">"</span></span><span class="pl-k">&gt;</span>
<span class="pl-k">&lt;</span>title<span class="pl-k">&gt;</span>Make a sprite from a texture atlas<span class="pl-k">&lt;</span><span class="pl-k">/</span>title<span class="pl-k">&gt;</span>
<span class="pl-k">&lt;</span>body<span class="pl-k">&gt;</span>
<span class="pl-k">&lt;</span>script src<span class="pl-k">=</span><span class="pl-s"><span class="pl-pds">"</span>../pixi.js/bin/pixi.js<span class="pl-pds">"</span></span><span class="pl-k">&gt;&lt;</span><span class="pl-k">/</span>script<span class="pl-k">&gt;</span>
<span class="pl-k">&lt;</span>script<span class="pl-k">&gt;</span>

<span class="pl-c">//Aliases</span>
<span class="pl-k">var</span> Container <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">Container</span>,
    autoDetectRenderer <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">autoDetectRenderer</span>,
    loader <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>,
    resources <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>.<span class="pl-smi">resources</span>,
    TextureCache <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">utils</span>.<span class="pl-smi">TextureCache</span>,
    Texture <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">Texture</span>,
    Sprite <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">Sprite</span>;

<span class="pl-c">//Create a Pixi stage and renderer and add the </span>
<span class="pl-c">//renderer.view to the DOM</span>
<span class="pl-k">var</span> stage <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Container</span>(),
    renderer <span class="pl-k">=</span> <span class="pl-en">autoDetectRenderer</span>(<span class="pl-c1">512</span>, <span class="pl-c1">512</span>);
<span class="pl-c1">document</span>.<span class="pl-c1">body</span>.<span class="pl-c1">appendChild</span>(<span class="pl-smi">renderer</span>.<span class="pl-smi">view</span>);

<span class="pl-c">//load a JSON file and run the `setup` function when it's done</span>
loader
  .<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">"</span>images/treasureHunter.json<span class="pl-pds">"</span></span>)
  .<span class="pl-c1">load</span>(setup);

<span class="pl-c">//Define variables that might be used in more </span>
<span class="pl-c">//than one function</span>
<span class="pl-k">var</span> dungeon, explorer, treasure, door, id;

<span class="pl-k">function</span> <span class="pl-en">setup</span>() {

  <span class="pl-c">//There are 3 ways to make sprites from textures atlas frames</span>

  <span class="pl-c">//1. Access the `TextureCache` directly</span>
  <span class="pl-k">var</span> dungeonTexture <span class="pl-k">=</span> TextureCache[<span class="pl-s"><span class="pl-pds">"</span>dungeon.png<span class="pl-pds">"</span></span>];
  dungeon <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(dungeonTexture);
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(dungeon);

  <span class="pl-c">//2. Access the texture using throuhg the loader's `resources`:</span>
  explorer <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(
    resources[<span class="pl-s"><span class="pl-pds">"</span>images/treasureHunter.json<span class="pl-pds">"</span></span>].<span class="pl-smi">textures</span>[<span class="pl-s"><span class="pl-pds">"</span>explorer.png<span class="pl-pds">"</span></span>]
  );
  <span class="pl-smi">explorer</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">68</span>;

  <span class="pl-c">//Center the explorer vertically</span>
  <span class="pl-smi">explorer</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-smi">stage</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span> <span class="pl-k">-</span> <span class="pl-smi">explorer</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(explorer);

  <span class="pl-c">//3. Create an optional alias called `id` for all the texture atlas </span>
  <span class="pl-c">//frame id textures.</span>
  id <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>.<span class="pl-smi">resources</span>[<span class="pl-s"><span class="pl-pds">"</span>images/treasureHunter.json<span class="pl-pds">"</span></span>].<span class="pl-smi">textures</span>; 

  <span class="pl-c">//Make the treasure box using the alias</span>
  treasure <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(id[<span class="pl-s"><span class="pl-pds">"</span>treasure.png<span class="pl-pds">"</span></span>]);
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(treasure);

  <span class="pl-c">//Position the treasure next to the right edge of the canvas</span>
  <span class="pl-smi">treasure</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-smi">stage</span>.<span class="pl-c1">width</span> <span class="pl-k">-</span> <span class="pl-smi">treasure</span>.<span class="pl-c1">width</span> <span class="pl-k">-</span> <span class="pl-c1">48</span>;
  <span class="pl-smi">treasure</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-smi">stage</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span> <span class="pl-k">-</span> <span class="pl-smi">treasure</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(treasure);

  <span class="pl-c">//Make the exit door</span>
  door <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(id[<span class="pl-s"><span class="pl-pds">"</span>door.png<span class="pl-pds">"</span></span>]); 
  <span class="pl-smi">door</span>.<span class="pl-smi">position</span>.<span class="pl-c1">set</span>(<span class="pl-c1">32</span>, <span class="pl-c1">0</span>);
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(door);

  <span class="pl-c">//Make the blobs</span>
  <span class="pl-k">var</span> numberOfBlobs <span class="pl-k">=</span> <span class="pl-c1">6</span>,
      spacing <span class="pl-k">=</span> <span class="pl-c1">48</span>,
      xOffset <span class="pl-k">=</span> <span class="pl-c1">150</span>;

  <span class="pl-c">//Make as many blobs as there are `numberOfBlobs`</span>
  <span class="pl-k">for</span> (<span class="pl-k">var</span> i <span class="pl-k">=</span> <span class="pl-c1">0</span>; i <span class="pl-k">&lt;</span> numberOfBlobs; i<span class="pl-k">++</span>) {

    <span class="pl-c">//Make a blob</span>
    <span class="pl-k">var</span> blob <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(id[<span class="pl-s"><span class="pl-pds">"</span>blob.png<span class="pl-pds">"</span></span>]);

    <span class="pl-c">//Space each blob horizontally according to the `spacing` value.</span>
    <span class="pl-c">//`xOffset` determines the point from the left of the screen</span>
    <span class="pl-c">//at which the first blob should be added.</span>
    <span class="pl-k">var</span> x <span class="pl-k">=</span> spacing <span class="pl-k">*</span> i <span class="pl-k">+</span> xOffset;

    <span class="pl-c">//Give the blob a random y position</span>
    <span class="pl-c">//(`randomInt` is a custom function - see below)</span>
    <span class="pl-k">var</span> y <span class="pl-k">=</span> <span class="pl-en">randomInt</span>(<span class="pl-c1">0</span>, <span class="pl-smi">stage</span>.<span class="pl-c1">height</span> <span class="pl-k">-</span> <span class="pl-smi">blob</span>.<span class="pl-c1">height</span>);

    <span class="pl-c">//Set the blob's position</span>
    <span class="pl-smi">blob</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> x;
    <span class="pl-smi">blob</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> y;

    <span class="pl-c">//Add the blob sprite to the stage</span>
    <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(blob);
  }

  <span class="pl-c">//Render the stage   </span>
  <span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);
}

<span class="pl-c">//The `randomInt` helper function</span>
<span class="pl-k">function</span> <span class="pl-en">randomInt</span>(<span class="pl-smi">min</span>, <span class="pl-smi">max</span>) {
  <span class="pl-k">return</span> <span class="pl-c1">Math</span>.<span class="pl-c1">floor</span>(<span class="pl-c1">Math</span>.<span class="pl-c1">random</span>() <span class="pl-k">*</span> (max <span class="pl-k">-</span> min <span class="pl-k">+</span> <span class="pl-c1">1</span>)) <span class="pl-k">+</span> min;
}

<span class="pl-k">&lt;</span><span class="pl-k">/</span>script<span class="pl-k">&gt;</span>
<span class="pl-k">&lt;</span><span class="pl-k">/</span>body<span class="pl-k">&gt;</span></pre></div>

<p>You can see in the code above that all the blobs are created using a
<code>for</code> loop. Each <code>blob</code> is spaced evenly along the <code>x</code> axis like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> x <span class="pl-k">=</span> spacing <span class="pl-k">*</span> i <span class="pl-k">+</span> xOffset;
<span class="pl-smi">blob</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> x;</pre></div>

<p><code>spacing</code> has a value 48, and <code>xOffset</code> has a value of 150. What this
means that that the first <code>blob</code> will have an <code>x</code> position of 150.
This offsets it from the left side of the stage by 150 pixel. Each
subsequent <code>blob</code> will have an <code>x</code> value that's 48 pixels greater than
the <code>blob</code> created in the previous iteration of the loop. This creates
an evenly spaced line of blob monsters, from left to right, along the dungeon floor.</p>

<p>Each <code>blob</code> is also given a random <code>y</code> position. Here's the code that
does this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> y <span class="pl-k">=</span> <span class="pl-en">randomInt</span>(<span class="pl-c1">0</span>, <span class="pl-smi">stage</span>.<span class="pl-c1">height</span> <span class="pl-k">-</span> <span class="pl-smi">blob</span>.<span class="pl-c1">height</span>);
<span class="pl-smi">blob</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> y;</pre></div>

<p>The <code>blob</code>'s <code>y</code> position could be assigned any random number between 0 and
512, which is the value of <code>stage.height</code>. This works with the help of
a custom function called <code>randomInt</code>. <code>randomInt</code> returns a random number
that's within a range between any two numbers you supply.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-en">randomInt</span>(lowestNumber, highestNumber)</pre></div>

<p>That means if you want a random number between 1 and 10, you can get
one like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> randomNumber <span class="pl-k">=</span> <span class="pl-en">randomInt</span>(<span class="pl-c1">1</span>, <span class="pl-c1">10</span>);</pre></div>

<p>Here's the <code>randomInt</code> function definition that does all this work:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">randomInt</span>(<span class="pl-smi">min</span>, <span class="pl-smi">max</span>) {
  <span class="pl-k">return</span> <span class="pl-c1">Math</span>.<span class="pl-c1">floor</span>(<span class="pl-c1">Math</span>.<span class="pl-c1">random</span>() <span class="pl-k">*</span> (max <span class="pl-k">-</span> min <span class="pl-k">+</span> <span class="pl-c1">1</span>)) <span class="pl-k">+</span> min;
}</pre></div>

<p><code>randomInt</code> is a great little function to keep in your back pocket for
making games - I use it all the time.</p>

<p><a id="user-content-movingsprites"></a></p>

<h2><a id="user-content-moving-sprites" class="anchor" href="#moving-sprites" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Moving Sprites</h2>

<p>You now know how to display sprites, but how do you make them move?
That's easy: create a looping function using <code>requestAnimationFrame</code>.
This is called a <strong>game loop</strong>.
Any code you put inside the game loop will update 60 times per
second. Here's some code you could write to make the <code>cat</code> sprite move
at a rate  of 1 pixel per frame.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">gameLoop</span>() {

  <span class="pl-c">//Loop this function at 60 frames per second</span>
  <span class="pl-en">requestAnimationFrame</span>(gameLoop);

  <span class="pl-c">//Move the cat 1 pixel to the right each frame</span>
  <span class="pl-smi">cat</span>.<span class="pl-c1">x</span> <span class="pl-k">+=</span> <span class="pl-c1">1</span>;

  <span class="pl-c">//Render the stage to see the animation</span>
  <span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);
}

<span class="pl-c">//Start the game loop</span>
<span class="pl-en">gameLoop</span>();</pre></div>

<p>If you run this bit of code, you'll see the sprite gradually move to
the right side of the stage.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s15.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/15.png" alt="Moving sprites" style="max-width:100%;"></a></p>

<p>And that's really all there is to it! Just change any sprite property by small
increments inside the loop, and they'll animate over time. If you want
the sprite to animate in the opposite direction (to the left), just give it a
negative value, like <code>-1</code>.
You'll find this code in the <code>movingSprites.html</code> file - here's the
complete code:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c">//Aliases</span>
<span class="pl-k">var</span> Container <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">Container</span>,
    autoDetectRenderer <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">autoDetectRenderer</span>,
    loader <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>,
    resources <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>.<span class="pl-smi">resources</span>,
    Sprite <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">Sprite</span>;

<span class="pl-c">//Create a Pixi stage and renderer </span>
<span class="pl-k">var</span> stage <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Container</span>(),
    renderer <span class="pl-k">=</span> <span class="pl-en">autoDetectRenderer</span>(<span class="pl-c1">256</span>, <span class="pl-c1">256</span>);
<span class="pl-c1">document</span>.<span class="pl-c1">body</span>.<span class="pl-c1">appendChild</span>(<span class="pl-smi">renderer</span>.<span class="pl-smi">view</span>);

<span class="pl-c">//Load an image and the run the `setup` function</span>
loader
  .<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>)
  .<span class="pl-c1">load</span>(setup);

<span class="pl-c">//Define any variables that are used in more than one function</span>
<span class="pl-k">var</span> cat;

<span class="pl-k">function</span> <span class="pl-en">setup</span>() {

  <span class="pl-c">//Create the `cat` sprite </span>
  cat <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(resources[<span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>].<span class="pl-smi">texture</span>);
  <span class="pl-smi">cat</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">96</span>; 
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(cat);

  <span class="pl-c">//Start the game loop</span>
  <span class="pl-en">gameLoop</span>();
}

<span class="pl-k">function</span> <span class="pl-en">gameLoop</span>(){

  <span class="pl-c">//Loop this function 60 times per second</span>
  <span class="pl-en">requestAnimationFrame</span>(gameLoop);

  <span class="pl-c">//Move the cat 1 pixel per frame</span>
  <span class="pl-smi">cat</span>.<span class="pl-c1">x</span> <span class="pl-k">+=</span> <span class="pl-c1">1</span>;

  <span class="pl-c">//Render the stage</span>
  <span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);
}</pre></div>

<p>(Notice that the <code>cat</code> variable needs to be defined outside the
<code>setup</code> and
<code>gameLoop</code> functions so that you can access it inside both of them.)</p>

<p>You can animate a sprite's scale, rotation, or size - whatever! You'll see
many more examples of how to animate sprites ahead.</p>

<p><a id="user-content-velocity"></a></p>

<h2><a id="user-content-using-velocity-properties" class="anchor" href="#using-velocity-properties" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Using velocity properties</h2>

<p>To give you more flexibility, its a good idea control a sprite's
movement speed using two <strong>velocity properties</strong>: <code>vx</code> and <code>vy</code>. <code>vx</code>
is used to set the sprite's speed and direction on the x axis
(horizontally). <code>vy</code> is
used to set the sprite's speed and direction on the y axis (vertically). Instead of
changing a sprite's <code>x</code> and <code>y</code> values directly, first update the velocity
variables, and then assign those velocity values to the sprite. This is an
extra bit of modularity that you'll need for interactive game animation.</p>

<p>The first step is to create <code>vx</code> and <code>vy</code> properties on your sprite,
and give them an initial value.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
<span class="pl-smi">cat</span>.<span class="pl-smi">vy</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;</pre></div>

<p>Setting <code>vx</code> and <code>vy</code> to 0 means that the sprite isn't moving.</p>

<p>Next, in the game loop, update <code>vx</code> and <code>vy</code> with the velocity
that you want the sprite to move at. Then assign those values to the
sprite's <code>x</code> and <code>y</code> properties. Here's how you could use this
technique to make the cat sprite move down and to right at one pixel each
frame:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">setup</span>() {

  <span class="pl-c">//Create the `cat` sprite </span>
  cat <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(resources[<span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>].<span class="pl-smi">texture</span>);
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(cat);

  <span class="pl-c">//Initialize the cat's velocity variables</span>
  <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
  <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;

  <span class="pl-c">//Start the game loop</span>
  <span class="pl-en">gameLoop</span>();
}

<span class="pl-k">function</span> <span class="pl-en">gameLoop</span>(){

  <span class="pl-c">//Loop this function 60 times per second</span>
  <span class="pl-en">requestAnimationFrame</span>(gameLoop);

  <span class="pl-c">//Update the cat's velocity</span>
  <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">=</span> <span class="pl-c1">1</span>;
  <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span> <span class="pl-k">=</span> <span class="pl-c1">1</span>;

  <span class="pl-c">//Apply the velocity values to the cat's </span>
  <span class="pl-c">//position to make it move</span>
  <span class="pl-smi">cat</span>.<span class="pl-c1">x</span> <span class="pl-k">+=</span> <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span>;
  <span class="pl-smi">cat</span>.<span class="pl-c1">y</span> <span class="pl-k">+=</span> <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span>;

  <span class="pl-c">//Render the stage</span>
  <span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);
}

</pre></div>

<p>When you run this code, the cat will move down and to the right at one
pixel per frame:</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s16.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/16.png" alt="Moving sprites" style="max-width:100%;"></a></p>

<p>What if you want to make the cat move in a different direction? To
make the cat move to the left, give it a <code>vx</code> value of <code>-1</code>. To make
it move up, give the cat a <code>vy</code> value of <code>-1</code>. To make the cat move
faster, give it larger <code>vx</code> and <code>vy</code> values, like <code>3</code>, <code>5</code>, <code>-2</code>, or
<code>-4</code>.</p>

<p>You'll see ahead how modularizing a sprite's velocity with <code>vx</code> and
<code>vy</code> velocity properties helps with keyboard and mouse pointer
control systems for games, as well as making it easier to implement physics.</p>

<p><a id="user-content-gamestates"></a></p>

<h2><a id="user-content-game-states" class="anchor" href="#game-states" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Game states</h2>

<p>As a matter of style, and to help modularize your code, I
recommend structuring your game loop like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c">//Set the game's current state to `play`:</span>
<span class="pl-k">var</span> state <span class="pl-k">=</span> play;

<span class="pl-k">function</span> <span class="pl-en">gameLoop</span>() {

  <span class="pl-c">//Loop this function at 60 frames per second</span>
  <span class="pl-en">requestAnimationFrame</span>(gameLoop);

  <span class="pl-c">//Update the current game state:</span>
  <span class="pl-en">state</span>();

  <span class="pl-c">//Render the stage to see the animation</span>
  <span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);
}

<span class="pl-k">function</span> <span class="pl-en">play</span>() {

  <span class="pl-c">//Move the cat 1 pixel to the right each frame</span>
  <span class="pl-smi">cat</span>.<span class="pl-c1">x</span> <span class="pl-k">+=</span> <span class="pl-c1">1</span>;
}</pre></div>

<p>You can see that the <code>gameLoop</code> is calling a function called <code>state</code> 60 times
per second. What is the <code>state</code> function? It's been assigned to
<code>play</code>. That means all the code in the <code>play</code> function will also run at 60
times per second.</p>

<p>Here's how the code from the previous example can be re-factored to
this new model:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c">//Define any variables that are used in more than one function</span>
<span class="pl-k">var</span> cat, state;

<span class="pl-k">function</span> <span class="pl-en">setup</span>() {

  <span class="pl-c">//Create the `cat` sprite </span>
  cat <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(resources[<span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>].<span class="pl-smi">texture</span>);
  <span class="pl-smi">cat</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">96</span>; 
  <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
  <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(cat);

  <span class="pl-c">//Set the game state</span>
  state <span class="pl-k">=</span> play;

  <span class="pl-c">//Start the game loop</span>
  <span class="pl-en">gameLoop</span>();
}

<span class="pl-k">function</span> <span class="pl-en">gameLoop</span>(){

  <span class="pl-c">//Loop this function 60 times per second</span>
  <span class="pl-en">requestAnimationFrame</span>(gameLoop);

  <span class="pl-c">//Update the current game state:</span>
  <span class="pl-en">state</span>();

  <span class="pl-c">//Render the stage</span>
  <span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);
}

<span class="pl-k">function</span> <span class="pl-en">play</span>() {

  <span class="pl-c">//Move the cat 1 pixel to the right each frame</span>
  <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">=</span> <span class="pl-c1">1</span>
  <span class="pl-smi">cat</span>.<span class="pl-c1">x</span> <span class="pl-k">+=</span> <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span>;
}</pre></div>

<p>Yes, I know, this is a bit of <a href="http://www.amazon.com/Electric-Psychedelic-Sitar-Headswirlers-1-5/dp/B004HZ14VS">head-swirler</a>! But, don't let it scare
and you and spend a minute or two walking through in your mind how those
functions are connected. As you'll see ahead, structuring your game
loop like this will make it much, much easier to do things like switching
game scenes and levels.</p>

<p><a id="user-content-keyboard"></a></p>

<h2><a id="user-content-keyboard-movement" class="anchor" href="#keyboard-movement" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Keyboard Movement</h2>

<p>With just a little more work you can build a simple system to control
a sprite using the keyboard. To simplify your code, I suggest you use
this custom function called <code>keyboard</code> that listens for and captures
keyboard events.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">keyboard</span>(<span class="pl-smi">keyCode</span>) {
  <span class="pl-k">var</span> key <span class="pl-k">=</span> {};
  <span class="pl-smi">key</span>.<span class="pl-c1">code</span> <span class="pl-k">=</span> keyCode;
  <span class="pl-smi">key</span>.<span class="pl-smi">isDown</span> <span class="pl-k">=</span> <span class="pl-c1">false</span>;
  <span class="pl-smi">key</span>.<span class="pl-smi">isUp</span> <span class="pl-k">=</span> <span class="pl-c1">true</span>;
  <span class="pl-smi">key</span>.<span class="pl-smi">press</span> <span class="pl-k">=</span> <span class="pl-c1">undefined</span>;
  <span class="pl-smi">key</span>.<span class="pl-smi">release</span> <span class="pl-k">=</span> <span class="pl-c1">undefined</span>;
  <span class="pl-c">//The `downHandler`</span>
  <span class="pl-smi">key</span>.<span class="pl-en">downHandler</span> <span class="pl-k">=</span> <span class="pl-k">function</span>(<span class="pl-c1">event</span>) {
    <span class="pl-k">if</span> (<span class="pl-c1">event</span>.<span class="pl-smi">keyCode</span> <span class="pl-k">===</span> <span class="pl-smi">key</span>.<span class="pl-c1">code</span>) {
      <span class="pl-k">if</span> (<span class="pl-smi">key</span>.<span class="pl-smi">isUp</span> <span class="pl-k">&amp;&amp;</span> <span class="pl-smi">key</span>.<span class="pl-smi">press</span>) <span class="pl-smi">key</span>.<span class="pl-en">press</span>();
      <span class="pl-smi">key</span>.<span class="pl-smi">isDown</span> <span class="pl-k">=</span> <span class="pl-c1">true</span>;
      <span class="pl-smi">key</span>.<span class="pl-smi">isUp</span> <span class="pl-k">=</span> <span class="pl-c1">false</span>;
    }
    <span class="pl-c1">event</span>.<span class="pl-c1">preventDefault</span>();
  };

  <span class="pl-c">//The `upHandler`</span>
  <span class="pl-smi">key</span>.<span class="pl-en">upHandler</span> <span class="pl-k">=</span> <span class="pl-k">function</span>(<span class="pl-c1">event</span>) {
    <span class="pl-k">if</span> (<span class="pl-c1">event</span>.<span class="pl-smi">keyCode</span> <span class="pl-k">===</span> <span class="pl-smi">key</span>.<span class="pl-c1">code</span>) {
      <span class="pl-k">if</span> (<span class="pl-smi">key</span>.<span class="pl-smi">isDown</span> <span class="pl-k">&amp;&amp;</span> <span class="pl-smi">key</span>.<span class="pl-smi">release</span>) <span class="pl-smi">key</span>.<span class="pl-en">release</span>();
      <span class="pl-smi">key</span>.<span class="pl-smi">isDown</span> <span class="pl-k">=</span> <span class="pl-c1">false</span>;
      <span class="pl-smi">key</span>.<span class="pl-smi">isUp</span> <span class="pl-k">=</span> <span class="pl-c1">true</span>;
    }
    <span class="pl-c1">event</span>.<span class="pl-c1">preventDefault</span>();
  };

  <span class="pl-c">//Attach event listeners</span>
  <span class="pl-c1">window</span>.<span class="pl-c1">addEventListener</span>(
    <span class="pl-s"><span class="pl-pds">"</span>keydown<span class="pl-pds">"</span></span>, <span class="pl-smi">key</span>.<span class="pl-smi">downHandler</span>.<span class="pl-en">bind</span>(key), <span class="pl-c1">false</span>
  );
  <span class="pl-c1">window</span>.<span class="pl-c1">addEventListener</span>(
    <span class="pl-s"><span class="pl-pds">"</span>keyup<span class="pl-pds">"</span></span>, <span class="pl-smi">key</span>.<span class="pl-smi">upHandler</span>.<span class="pl-en">bind</span>(key), <span class="pl-c1">false</span>
  );
  <span class="pl-k">return</span> key;
}</pre></div>

<p>The <code>keyboard</code> function is easy to use. Create a new keyboard object like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> keyObject <span class="pl-k">=</span> <span class="pl-en">keyboard</span>(asciiKeyCodeNumber);</pre></div>

<p>It's one argument is the ASCII key code number of the keyboad key that you
want to listen for.
<a href="http://help.adobe.com/en_US/AS2LCR/Flash_10.0/help.html?content=00000520.html">Here's a list of ASCII keyboard code
numbers</a>.</p>

<p>Then assign <code>press</code> and <code>release</code> methods to the keyboard object like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">keyObject</span>.<span class="pl-en">press</span> <span class="pl-k">=</span> <span class="pl-k">function</span>() {
  <span class="pl-c">//key object pressed</span>
};
<span class="pl-smi">keyObject</span>.<span class="pl-en">release</span> <span class="pl-k">=</span> <span class="pl-k">function</span>() {
  <span class="pl-c">//key object released</span>
};</pre></div>

<p>Keyboard objects also have <code>isDown</code> and <code>isUp</code> Boolean properties that
you can use to check the state of each key.</p>

<p>Take a look at the
<code>keyboardMovement.html</code> file in the <code>examples</code> folder to see how you
can use this <code>keyboard</code> function to control a sprite using your
keyboard's arrow keys. Run it and use the left, up, down, and right
arrow keys to move the cat around the stage.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s17.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/17.png" alt="Keyboard movement" style="max-width:100%;"></a></p>

<p>Here's the code that does all this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">setup</span>() {

  <span class="pl-c">//Create the `cat` sprite</span>
  cat <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(<span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>);
  <span class="pl-smi">cat</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">96</span>;
  <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
  <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(cat);

  <span class="pl-c">//Capture the keyboard arrow keys</span>
  <span class="pl-k">var</span> left <span class="pl-k">=</span> <span class="pl-en">keyboard</span>(<span class="pl-c1">37</span>),
      up <span class="pl-k">=</span> <span class="pl-en">keyboard</span>(<span class="pl-c1">38</span>),
      right <span class="pl-k">=</span> <span class="pl-en">keyboard</span>(<span class="pl-c1">39</span>),
      down <span class="pl-k">=</span> <span class="pl-en">keyboard</span>(<span class="pl-c1">40</span>);

  <span class="pl-c">//Left arrow key `press` method</span>
  <span class="pl-smi">left</span>.<span class="pl-en">press</span> <span class="pl-k">=</span> <span class="pl-k">function</span>() {

    <span class="pl-c">//Change the cat's velocity when the key is pressed</span>
    <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">=</span> <span class="pl-k">-</span><span class="pl-c1">5</span>;
    <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
  };

  <span class="pl-c">//Left arrow key `release` method</span>
  <span class="pl-smi">left</span>.<span class="pl-en">release</span> <span class="pl-k">=</span> <span class="pl-k">function</span>() {

    <span class="pl-c">//If the left arrow has been released, and the right arrow isn't down,</span>
    <span class="pl-c">//and the cat isn't moving vertically:</span>
    <span class="pl-c">//Stop the cat</span>
    <span class="pl-k">if</span> (<span class="pl-k">!</span><span class="pl-smi">right</span>.<span class="pl-smi">isDown</span> <span class="pl-k">&amp;&amp;</span> <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span> <span class="pl-k">===</span> <span class="pl-c1">0</span>) {
      <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
    }
  };

  <span class="pl-c">//Up</span>
  <span class="pl-smi">up</span>.<span class="pl-en">press</span> <span class="pl-k">=</span> <span class="pl-k">function</span>() {
    <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span> <span class="pl-k">=</span> <span class="pl-k">-</span><span class="pl-c1">5</span>;
    <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
  };
  <span class="pl-smi">up</span>.<span class="pl-en">release</span> <span class="pl-k">=</span> <span class="pl-k">function</span>() {
    <span class="pl-k">if</span> (<span class="pl-k">!</span><span class="pl-smi">down</span>.<span class="pl-smi">isDown</span> <span class="pl-k">&amp;&amp;</span> <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">===</span> <span class="pl-c1">0</span>) {
      <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
    }
  };

  <span class="pl-c">//Right</span>
  <span class="pl-smi">right</span>.<span class="pl-en">press</span> <span class="pl-k">=</span> <span class="pl-k">function</span>() {
    <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">=</span> <span class="pl-c1">5</span>;
    <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
  };
  <span class="pl-smi">right</span>.<span class="pl-en">release</span> <span class="pl-k">=</span> <span class="pl-k">function</span>() {
    <span class="pl-k">if</span> (<span class="pl-k">!</span><span class="pl-smi">left</span>.<span class="pl-smi">isDown</span> <span class="pl-k">&amp;&amp;</span> <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span> <span class="pl-k">===</span> <span class="pl-c1">0</span>) {
      <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
    }
  };

  <span class="pl-c">//Down</span>
  <span class="pl-smi">down</span>.<span class="pl-en">press</span> <span class="pl-k">=</span> <span class="pl-k">function</span>() {
    <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span> <span class="pl-k">=</span> <span class="pl-c1">5</span>;
    <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
  };
  <span class="pl-smi">down</span>.<span class="pl-en">release</span> <span class="pl-k">=</span> <span class="pl-k">function</span>() {
    <span class="pl-k">if</span> (<span class="pl-k">!</span><span class="pl-smi">up</span>.<span class="pl-smi">isDown</span> <span class="pl-k">&amp;&amp;</span> <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">===</span> <span class="pl-c1">0</span>) {
      <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
    }
  };

  <span class="pl-c">//Set the game state</span>
  state <span class="pl-k">=</span> play;

  <span class="pl-c">//Start the game loop</span>
  <span class="pl-en">gameLoop</span>();
}

<span class="pl-k">function</span> <span class="pl-en">gameLoop</span>() {
  <span class="pl-en">requestAnimationFrame</span>(gameLoop);
  <span class="pl-en">state</span>();
  <span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);
}

<span class="pl-k">function</span> <span class="pl-en">play</span>() {

  <span class="pl-c">//Use the cat's velocity to make it move</span>
  <span class="pl-smi">cat</span>.<span class="pl-c1">x</span> <span class="pl-k">+=</span> <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span>;
  <span class="pl-smi">cat</span>.<span class="pl-c1">y</span> <span class="pl-k">+=</span> <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span>
}</pre></div>

<p><a id="user-content-grouping"></a></p>

<h2><a id="user-content-grouping-sprites" class="anchor" href="#grouping-sprites" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Grouping Sprites</h2>

<p>Groups let you create game scenes, and manage similar sprites together
as single units. Pixi has an object called a <code>Container</code>
that lets you do this. Let's find out how it works.</p>

<p>Imagine that you want to display three sprites: a cat, hedgehog and
tiger. Create them, and set their positions - <em>but don't add them to the
stage</em>.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c">//The cat</span>
<span class="pl-k">var</span> cat <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(id[<span class="pl-s"><span class="pl-pds">"</span>cat.png<span class="pl-pds">"</span></span>]);
<span class="pl-smi">cat</span>.<span class="pl-smi">position</span>.<span class="pl-c1">set</span>(<span class="pl-c1">16</span>, <span class="pl-c1">16</span>);

<span class="pl-c">//The hedgehog</span>
<span class="pl-k">var</span> hedgehog <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(id[<span class="pl-s"><span class="pl-pds">"</span>hedgehog.png<span class="pl-pds">"</span></span>]);
<span class="pl-smi">hedgehog</span>.<span class="pl-smi">position</span>.<span class="pl-c1">set</span>(<span class="pl-c1">32</span>, <span class="pl-c1">32</span>);

<span class="pl-c">//The tiger</span>
<span class="pl-k">var</span> tiger <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(id[<span class="pl-s"><span class="pl-pds">"</span>tiger.png<span class="pl-pds">"</span></span>]);
<span class="pl-smi">tiger</span>.<span class="pl-smi">position</span>.<span class="pl-c1">set</span>(<span class="pl-c1">64</span>, <span class="pl-c1">64</span>);</pre></div>

<p>Next, create an <code>animals</code> container to group them all together like
this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> animals <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Container</span>();</pre></div>

<p>Then use <code>addChild</code> to <em>add the sprites to the group</em>.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">animals</span>.<span class="pl-en">addChild</span>(cat);
<span class="pl-smi">animals</span>.<span class="pl-en">addChild</span>(hedgehog);
<span class="pl-smi">animals</span>.<span class="pl-en">addChild</span>(tiger);</pre></div>

<p>Finally add the group to the stage.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(animals);
<span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);</pre></div>

<p>(As you know, the <code>stage</code> object is also a <code>Container</code>. It’s the root
container for all Pixi sprites.)</p>

<p>Here's what this code produces:</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s18.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/18.png" alt="Grouping sprites" style="max-width:100%;"></a></p>

<p>What you can't see in that image is the invisible <code>animals</code> group
that's containing the sprites.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s19.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/19.png" alt="Grouping sprites" style="max-width:100%;"></a></p>

<p>You can now treat the <code>animals</code> group as a single unit. You can think
of a <code>Container</code> as a special kind of sprite that doesn’t
have a texture.</p>

<p>If you need a list of all the child sprites that <code>animals</code> contains,
use its <code>children</code> array to find out.</p>

<pre><code>console.log(animals.chidren}
//Displays: Array [Object, Object, Object]
</code></pre>

<p>This tells you that <code>animals</code> has three sprites as children.</p>

<p>Because the <code>animals</code> group is just like any other sprite, you can
change its <code>x</code> and <code>y</code> values, <code>alpha</code>, <code>scale</code> and
all the other sprite properties. Any property value you change on the
parent container will affect the child sprites in a relative way. So if you
set the group's <code>x</code> and <code>y</code> position, all the child sprites will
be repositioned relative to the group's top left corner. What would
happen if you set the <code>animals</code>'s <code>x</code> and <code>y</code> position to 64?</p>

<pre><code>animals.position.set(64, 64);
</code></pre>

<p>The whole group of sprites will move 64 pixels right and 64 pixels to
the down.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s20.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/20.png" alt="Grouping sprites" style="max-width:100%;"></a></p>

<p>The <code>animals</code> group also has its own dimensions, which is based on the area
occupied by the containing sprites. You can find its <code>width</code> and
<code>height</code> values like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-en">console</span>.<span class="pl-c1">log</span>(<span class="pl-smi">animals</span>.<span class="pl-c1">width</span>);
<span class="pl-c">//Displays: 112</span>

<span class="pl-en">console</span>.<span class="pl-c1">log</span>(<span class="pl-smi">animals</span>.<span class="pl-c1">height</span>);
<span class="pl-c">//Displays: 112</span>
</pre></div>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s21.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/21.png" alt="Group width and height" style="max-width:100%;"></a></p>

<p>What happens if you change a group's width or height?</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">animals</span>.<span class="pl-c1">width</span> <span class="pl-k">=</span> <span class="pl-c1">200</span>;
<span class="pl-smi">animals</span>.<span class="pl-c1">height</span> <span class="pl-k">=</span> <span class="pl-c1">200</span>;</pre></div>

<p>All the child
sprites will scale to match that change.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s22.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/22.png" alt="Group width and height" style="max-width:100%;"></a></p>

<p>You can nest as many <code>Container</code>s inside other
<code>Container</code>s as you like, to create deep hierarchies if
you need to. However, a <code>DisplayObject</code> (like a <code>Sprite</code> or another
<code>Container</code>) can only belong to one parent at a time. If
you use <code>addChild</code> to make a sprite the child of another object, Pixi
will automatically remove it from its current parent. That’s a useful
bit of management that you don’t have to worry about.</p>

<p><a id="user-content-localnglobal"></a></p>

<h3><a id="user-content-local-and-global-positions" class="anchor" href="#local-and-global-positions" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Local and global positions</h3>

<p>When you add a sprite to a <code>Container</code>, its <code>x</code> and <code>y</code>
position is <em>relative to the group’s top left corner</em>. That's the
sprite's <strong>local position</strong> For example, what do you think the cat's
position is in this image?</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s20.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/20.png" alt="Grouping sprites" style="max-width:100%;"></a></p>

<p>Let's find out:</p>

<pre><code>console.log(cat.x);
//Displays: 16
</code></pre>

<p>16? Yes! That's because the cat is offset by only 16 pixel's from the
group's top left corner. 16 is the cat's local position.</p>

<p>Sprites also have a <strong>global position</strong>. The global position is the
distance from the top left corner of the stage, to the sprite's anchor
point (usually the sprite's top left corner.) You can find a sprite's global
position with the help of the <code>toGlobal</code> method.  Here's how:</p>

<pre><code>parentSprite.toGlobal(childSprite.position)
</code></pre>

<p>That means you can find the cat's global position inside the <code>animals</code>
group like this:</p>

<pre><code>console.log(animals.toGlobal(cat.position));
//Displays: Object {x: 80, y: 80...};
</code></pre>

<p>That gives you an <code>x</code> and <code>y</code> position of 80. That's exactly the cat's
global position relative to the top left corner of the stage. </p>

<p>What if you want to find the global position of a sprite, but don't
know what the sprite's parent container
is? Every sprite has a property called <code>parent</code> that will tell you what the
sprite's parent is. If you add a sprite directly to the <code>stage</code>, then
<code>stage</code> will be the sprite's parent. In the example above, the <code>cat</code>'s
parent is <code>animals</code>. That means you can alternatively get the cat's global position
by writing code like this:</p>

<pre><code>cat.parent.toGlobal(cat.position);
</code></pre>

<p>And it will work even if you don't know what the cat's parent
container currently is.</p>

<p>There's one more way to calculate the global position! And, it's
actually the best way, so heads up! If you want to know the distance
from the top left corner of the canvas to the sprite, and don't know
or care what the sprite's parent containers are, use the
<code>getGlobalPosition</code> method. Here's how to use it to find the tiger's global position:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">tiger</span>.<span class="pl-en">getGlobalPosition</span>().<span class="pl-c1">x</span>
<span class="pl-smi">tiger</span>.<span class="pl-en">getGlobalPosition</span>().<span class="pl-c1">y</span></pre></div>

<p>This will give you <code>x</code> and <code>y</code> values of 128 in the example that we've
been using.
The special thing about <code>getGlobalPosition</code> is that it's highly
precise: it will give you the sprite's accurate global position as
soon as its local position changes. I asked the Pixi development team
to add this feature specifically for accurate collision detection for
games.</p>

<p>What if you want to convert a global position to a local position? you
can use the <code>toLocal</code> method. It works in a similar way, but uses this
general format:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">sprite</span>.<span class="pl-en">toLocal</span>(<span class="pl-smi">sprite</span>.<span class="pl-smi">position</span>, anyOtherSprite)</pre></div>

<p>Use <code>toLocal</code> to find the distance between a sprite and any other
sprite. Here's how you could find out the tiger's local
position, relative to the hedgehog.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">tiger</span>.<span class="pl-en">toLocal</span>(<span class="pl-smi">tiger</span>.<span class="pl-smi">position</span>, hedgehog).<span class="pl-c1">x</span>
<span class="pl-smi">tiger</span>.<span class="pl-en">toLocal</span>(<span class="pl-smi">tiger</span>.<span class="pl-smi">position</span>, hedgehog).<span class="pl-c1">y</span></pre></div>

<p>This gives you an <code>x</code> value of 32 and a <code>y</code> value of 32. You can see
in the example images that the tiger's top left corner is 32 pixels
down and to the left of the hedgehog's top left corner.</p>

<p><a id="user-content-spritebatch"></a></p>

<h3><a id="user-content-using-a-particlecontainer-to-group-sprites" class="anchor" href="#using-a-particlecontainer-to-group-sprites" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Using a ParticleContainer to group sprites</h3>

<p>Pixi has an alternative, high-performance way to group sprites called
a <code>ParticleContainer</code> (<code>PIXI.ParticleContainer</code>). Any sprites inside a
<code>ParticleContainer</code> will render 2 to 5
times faster than they would if they were in a regular
<code>Container</code>. It’s a great performance boost for games.</p>

<p>Create a <code>ParticleContainer</code> like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> superFastSprites <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">ParticleContainer</span>();</pre></div>

<p>Then use <code>addChild</code> to add sprites to it, just like you would with any
ordinary <code>Container</code>.</p>

<p>You have to make some compromises if you decide to use a
<code>ParticleContainer</code>. Sprites inside a <code>ParticleContainer</code> only have a few  basic properties:
<code>x</code>, <code>y</code>, <code>width</code>, <code>height</code>, <code>scale</code>, <code>pivot</code>, <code>alpha</code>, <code>visible</code> – and that’s
about it. Also, the sprites that it contains can’t have nested
children of their own. A <code>ParticleContainer</code> also can’t use Pixi’s advanced
visual effects like filters and blend modes. But for the huge performance boost that you get, those
compromises are usually worth it. And you can use
<code>Container</code>s and <code>ParticleContainer</code>s simultaneously in the same project, so you can fine-tune your optimization.</p>

<p>Why are sprites in a <code>Particle Container</code> so fast? Because the positions of
the sprites are being calculated directly on the GPU. The Pixi
development team is working to offload as much sprite processing as
possible on the GPU, so it’s likely that the latest version of Pixi
that you’re using will have much more feature-rich <code>ParticleContainer</code> than
what I've described here. Check the current <a href="http://pixijs.github.io/docs/PIXI.ParticleContainer.html"><code>ParticleContainer</code>
documentation</a> for details.</p>

<p>Where you create a <code>ParticleContainer</code>, there are two optional
arguments you can provide: the maximum number of sprites the container
can hold, and an options object.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> superFastSprites <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">ParticleContainer</span>(size, options);</pre></div>

<p>The default value for size is 15,000. So, if you need to contain more
sprites, set it to a higher number. The options argument is an object
with 5 Boolean values you can set: <code>scale</code>, <code>position</code>, <code>rotation</code>, <code>uvs</code> and
<code>alpha</code>. The default value of <code>position</code> is <code>true</code>, but all the others
are set to <code>false</code>. That means that if you want change the <code>rotation</code>,
<code>scale</code>, <code>alpha</code>, or <code>uvs</code> of sprite in the <code>ParticleContainer</code>, you
have to set those properties to <code>true</code>, like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> superFastSprites <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">ParticleContainer</span>(
  size, 
  {
    rotation<span class="pl-k">:</span> <span class="pl-c1">true</span>,
    alpha<span class="pl-k">:</span> <span class="pl-c1">true</span>,
    scale<span class="pl-k">:</span> <span class="pl-c1">true</span>,
    uvs<span class="pl-k">:</span> <span class="pl-c1">true</span>
  }
);</pre></div>

<p>But, if you don't think you'll need to use these properties, keep them
set to <code>false</code> to squeeze out the maximum amount of performance.</p>

<p>What's the <code>uvs</code> option? Only set it to <code>true</code> if you have particles
which change their textures while they're being animated. (All the
sprite's textures will also need to be on the same tileset image for
this to work.) </p>

<p>(Note: <strong>UV mapping</strong> is a 3D graphics display term that refers to
the <code>x</code> and <code>y</code> coordinates of the texture (the image) that is being
mapped onto a 3D surface. <code>U</code> is the <code>x</code> axis and <code>V</code> is the <code>y</code> axis.
WebGL already uses <code>x</code>, <code>y</code> and <code>z</code> for 3D spatial positioning, so <code>U</code>
and <code>V</code> were chosen to represent <code>x</code> and <code>y</code> for 2D image textures.)</p>

<p><a id="user-content-graphic"></a></p>

<h2><a id="user-content-pixis-graphic-primitives" class="anchor" href="#pixis-graphic-primitives" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Pixi's Graphic Primitives</h2>

<p>Using image textures is one of the most useful ways of making sprites,
but Pixi also has its own low-level drawing tools. You can use them to
make rectangles, shapes, lines, complex polygons and text. And,
fortunately, it uses almost the same API as the <a href="https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Drawing_graphics_with_canvas">Canvas Drawing API</a> so,
if you're already familiar with canvas, here’s nothing really new to
  learn. But the big advantage is that, unlike the Canvas Drawing API,
  the shapes you draw with Pixi are rendered by WebGL on the GPU. Pixi
  lets you access all that untapped performance power.
Let’s take a quick tour of how to make some basic shapes. Here are all
the shapes we'll make in the code ahead.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s23.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/23.png" alt="Graphic primitives" style="max-width:100%;"></a></p>

<p><a id="user-content-rectangles"></a></p>

<h3><a id="user-content-rectangles" class="anchor" href="#rectangles" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Rectangles</h3>

<p>All shapes are made by first creating a new instance of Pixi's
<code>Graphics</code> class (<code>PIXI.Graphics</code>).</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> rectangle <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Graphics</span>();</pre></div>

<p>Use <code>beginFill</code> with a hexadecimal color code value to set the
rectangle’ s fill color. Here’ how to set to it to light blue.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">rectangle</span>.<span class="pl-en">beginFill</span>(<span class="pl-c1">0x66CCFF</span>);</pre></div>

<p>If you want to give the shape an outline, use the <code>lineStyle</code> method. Here's
how to give the rectangle a 4 pixel wide red outline, with an <code>alpha</code>
value of 1.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">rectangle</span>.<span class="pl-en">lineStyle</span>(<span class="pl-c1">4</span>, <span class="pl-c1">0xFF3300</span>, <span class="pl-c1">1</span>);</pre></div>

<p>Use the <code>drawRect</code> method to draw the rectangle. Its four arguments
are <code>x</code>, <code>y</code>, <code>width</code> and <code>height</code>.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">rectangle</span>.<span class="pl-en">drawRect</span>(x, y, width, height);</pre></div>

<p>Use <code>endFill</code> when you’re done.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">rectangle</span>.<span class="pl-en">endFill</span>();</pre></div>

<p>It’s just like the Canvas Drawing API! Here’s all the code you need to
draw a rectangle, change its position, and add it to the stage.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> rectangle <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Graphics</span>();
<span class="pl-smi">rectangle</span>.<span class="pl-en">lineStyle</span>(<span class="pl-c1">4</span>, <span class="pl-c1">0xFF3300</span>, <span class="pl-c1">1</span>);
<span class="pl-smi">rectangle</span>.<span class="pl-en">beginFill</span>(<span class="pl-c1">0x66CCFF</span>);
<span class="pl-smi">rectangle</span>.<span class="pl-en">drawRect</span>(<span class="pl-c1">0</span>, <span class="pl-c1">0</span>, <span class="pl-c1">64</span>, <span class="pl-c1">64</span>);
<span class="pl-smi">rectangle</span>.<span class="pl-en">endFill</span>();
<span class="pl-smi">rectangle</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">170</span>;
<span class="pl-smi">rectangle</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">170</span>;
<span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(rectangle);
</pre></div>

<p>This code makes a 64 by 64 blue rectangle with a red border at an x and y position of 170.</p>

<p><a id="user-content-circles"></a></p>

<h3><a id="user-content-circles" class="anchor" href="#circles" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Circles</h3>

<p>Make a circle with the <code>drawCircle</code> method. Its three arguments are
<code>x</code>, <code>y</code> and <code>radius</code></p>

<div class="highlight highlight-source-js"><pre><span class="pl-en">drawCircle</span>(x, y, radius)</pre></div>

<p>Unlike rectangles and sprites, a circle’s x and y position is also its
center point. Here’s how to make a violet colored circle with a radius of 32 pixels.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> circle <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Graphics</span>();
<span class="pl-smi">circle</span>.<span class="pl-en">beginFill</span>(<span class="pl-c1">0x9966FF</span>);
<span class="pl-smi">circle</span>.<span class="pl-en">drawCircle</span>(<span class="pl-c1">0</span>, <span class="pl-c1">0</span>, <span class="pl-c1">32</span>);
<span class="pl-smi">circle</span>.<span class="pl-en">endFill</span>();
<span class="pl-smi">circle</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">64</span>;
<span class="pl-smi">circle</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">130</span>;
<span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(circle);</pre></div>

<p><a id="user-content-ellipses"></a></p>

<h3><a id="user-content-ellipses" class="anchor" href="#ellipses" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Ellipses</h3>

<p>As a one-up on the Canvas Drawing API, Pixi lets you draw an ellipse
with the <code>drawEllipse</code> method.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-en">drawEllipse</span>(x, y, width, height);</pre></div>

<p>The x/y position defines the ellipse’s top left corner (imagine that
the ellipse is surrounded by an invisible rectangular bounding box -
the top left corner of that box will represent the ellipse's x/y
anchor position). Here’s a yellow ellipse that’s 50 pixels wide and 20 pixels high.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> ellipse <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Graphics</span>();
<span class="pl-smi">ellipse</span>.<span class="pl-en">beginFill</span>(<span class="pl-c1">0xFFFF00</span>);
<span class="pl-smi">ellipse</span>.<span class="pl-en">drawEllipse</span>(<span class="pl-c1">0</span>, <span class="pl-c1">0</span>, <span class="pl-c1">50</span>, <span class="pl-c1">20</span>);
<span class="pl-smi">ellipse</span>.<span class="pl-en">endFill</span>();
<span class="pl-smi">ellipse</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">180</span>;
<span class="pl-smi">ellipse</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">130</span>;
<span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(ellipse);</pre></div>

<p><a id="user-content-roundedrect"></a></p>

<h3><a id="user-content-rounded-rectangles" class="anchor" href="#rounded-rectangles" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Rounded rectangles</h3>

<p>Pixi also lets you make rounded rectangles with the <code>drawRoundedRect</code>
method. The last argument, <code>cornerRadius</code> is a number in pixels that
determines by how much the corners should be rounded.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-en">drawRoundedRect</span>(x, y, width, height, cornerRadius)</pre></div>

<p>Here's how to make a rounded rectangle with a corner radius of 10
pixels.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> roundBox <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Graphics</span>();
<span class="pl-smi">roundBox</span>.<span class="pl-en">lineStyle</span>(<span class="pl-c1">4</span>, <span class="pl-c1">0x99CCFF</span>, <span class="pl-c1">1</span>);
<span class="pl-smi">roundBox</span>.<span class="pl-en">beginFill</span>(<span class="pl-c1">0xFF9933</span>);
<span class="pl-smi">roundBox</span>.<span class="pl-en">drawRoundedRect</span>(<span class="pl-c1">0</span>, <span class="pl-c1">0</span>, <span class="pl-c1">84</span>, <span class="pl-c1">36</span>, <span class="pl-c1">10</span>)
<span class="pl-smi">roundBox</span>.<span class="pl-en">endFill</span>();
<span class="pl-smi">roundBox</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">48</span>;
<span class="pl-smi">roundBox</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">190</span>;
<span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(roundBox);</pre></div>

<p><a id="user-content-lines"></a></p>

<h3><a id="user-content-lines" class="anchor" href="#lines" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Lines</h3>

<p>You've seen in the examples above that the <code>lineStyle</code> method lets you
define a line.  You can use the <code>moveTo</code> and <code>lineTo</code> methods to draw the
start and end points of the line, in just the same way you can with the Canvas
Drawing API. Here’s how to draw a 4 pixel wide, white diagonal line.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> line <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Graphics</span>();
<span class="pl-smi">line</span>.<span class="pl-en">lineStyle</span>(<span class="pl-c1">4</span>, <span class="pl-c1">0xFFFFFF</span>, <span class="pl-c1">1</span>);
<span class="pl-smi">line</span>.<span class="pl-c1">moveTo</span>(<span class="pl-c1">0</span>, <span class="pl-c1">0</span>);
<span class="pl-smi">line</span>.<span class="pl-en">lineTo</span>(<span class="pl-c1">80</span>, <span class="pl-c1">50</span>);
<span class="pl-smi">line</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">32</span>;
<span class="pl-smi">line</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">32</span>;
<span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(line);</pre></div>

<p><code>PIXI.Graphics</code> objects, like lines, have <code>x</code> and <code>y</code> values, just
like sprites, so you can position them anywhere on the stage after
you've drawn them.</p>

<p><a id="user-content-polygons"></a></p>

<h3><a id="user-content-polygons" class="anchor" href="#polygons" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Polygons</h3>

<p>You can join lines together and fill them with colors to make complex
shapes using the <code>drawPolygon</code> method. <code>drawPolygon</code>'s argument is an
path array of x/y points that define the positions of each point on the
shape.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> path <span class="pl-k">=</span> [
  point1X, point1Y,
  point2X, point2Y,
  point3X, point3Y
];

<span class="pl-smi">graphicsObject</span>.<span class="pl-en">drawPolygon</span>(path);</pre></div>

<p><code>drawPolygon</code> will join those three points together to make the shape.
Here’s how to use <code>drawPolygon</code> to connect three lines together to
make a red triangle with a blue border. The triangle is drawn at
position 0,0 and then moved to its position on the stage using its
<code>x</code> and <code>y</code> properties.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> triangle <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Graphics</span>();
<span class="pl-smi">triangle</span>.<span class="pl-en">beginFill</span>(<span class="pl-c1">0x66FF33</span>);

<span class="pl-c">//Use `drawPolygon` to define the triangle as</span>
<span class="pl-c">//a path array of x/y positions</span>

<span class="pl-smi">triangle</span>.<span class="pl-en">drawPolygon</span>([
    <span class="pl-k">-</span><span class="pl-c1">32</span>, <span class="pl-c1">64</span>,             <span class="pl-c">//First point</span>
    <span class="pl-c1">32</span>, <span class="pl-c1">64</span>,              <span class="pl-c">//Second point</span>
    <span class="pl-c1">0</span>, <span class="pl-c1">0</span>                 <span class="pl-c">//Third point</span>
]);

<span class="pl-c">//Fill shape's color</span>
<span class="pl-smi">triangle</span>.<span class="pl-en">endFill</span>();

<span class="pl-c">//Position the triangle after you've drawn it.</span>
<span class="pl-c">//The triangle's x/y position is anchored to its first point in the path</span>
<span class="pl-smi">triangle</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">180</span>;
<span class="pl-smi">triangle</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">22</span>;

<span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(triangle);</pre></div>

<p><a id="user-content-text"></a></p>

<h2><a id="user-content-displaying-text" class="anchor" href="#displaying-text" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Displaying text</h2>

<p>Use a <code>Text</code> object (<code>PIXI.Text</code>) to display text on the stage. The constructor
takes two arguments: the text you want to display and a style object
that defines the font’s properties. Here's how to display the words
"Hello Pixi", in white, 32 pixel high sans-serif font.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> message <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Text</span>(
  <span class="pl-s"><span class="pl-pds">"</span>Hello Pixi!<span class="pl-pds">"</span></span>,
  {font<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">"</span>32px sans-serif<span class="pl-pds">"</span></span>, fill<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">"</span>white<span class="pl-pds">"</span></span>}
);

<span class="pl-smi">message</span>.<span class="pl-smi">position</span>.<span class="pl-c1">set</span>(<span class="pl-c1">54</span>, <span class="pl-c1">96</span>);
<span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(message);</pre></div>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s24.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/24.png" alt="Displaying text" style="max-width:100%;"></a></p>

<p>Pixi’s Text objects inherit from the <code>Sprite</code> class, so they
contain all the same properties like <code>x</code>, <code>y</code>, <code>width</code>, <code>height</code>,
<code>alpha</code>, and <code>rotation</code>. Position and resize text on the stage just like you would any other sprite.</p>

<p>If you want to change the content of a text object after you've
created it, use the <code>text</code> property.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">message</span>.<span class="pl-c1">text</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">"</span>Text changed!<span class="pl-pds">"</span></span>;</pre></div>

<p>Use the <code>style</code> property if you want to redefine the font properties.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">message</span>.<span class="pl-c1">style</span> <span class="pl-k">=</span> {fill<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">"</span>black<span class="pl-pds">"</span></span>, font<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">"</span>16px PetMe64<span class="pl-pds">"</span></span>};</pre></div>

<p>Pixi makes text objects by using the Canvas Drawing API to
render the text to an invisible and temporary canvas
element. It then turns the canvas into a WebGL texture so that it
can be mapped onto a sprite. That’s why the text’s color needs to be
wrapped in a string: it’s a Canvas Drawing API color value. As with
any canvas color values, you can use words for common colors like
“red” or “green”, or use rgba, hsla or hex values.</p>

<p>Other style properties that you can include are <code>stroke</code> for the font
outline color and <code>strokeThickness</code> for the outline thickness. Set the
text's <code>dropShadow</code> property to <code>true</code> to make the text display a
shadow. Use <code>dropShadowColor</code> to set the shadow's hexadecimal color
value, use <code>dropShadowAngle</code> to set the shadow's angle in radians, and
use <code>dropShadowDistance</code> to set the pixel height of a shadow. And
there's more: <a href="http://pixijs.github.io/docs/PIXI.Text.html">check out Pixi's Text documentation for the full
list</a>.</p>

<p>Pixi can also wrap long lines of text. Set the text’s <code>wordWrap</code> style
property to <code>true</code>, and then set <code>wordWrapWidth</code> to the maximum length
in pixels, that the line of text should be. Use the <code>align</code> property
to set the alignment for multi-line text.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">message</span>.<span class="pl-c1">style</span> <span class="pl-k">=</span> {wordWrap<span class="pl-k">:</span> <span class="pl-c1">true</span>, wordWrapWidth<span class="pl-k">:</span> <span class="pl-c1">100</span>, align<span class="pl-k">:</span> center};</pre></div>

<p>(Note: <code>align</code> doesn't affect single line text.)</p>

<p>If you want to use a custom font file, use the CSS <code>@font-face</code> rule
to link the font file to the HTML page where your Pixi application is
running.</p>

<div class="highlight highlight-source-js"><pre>@font<span class="pl-k">-</span>face {
  font<span class="pl-k">-</span>family<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">"</span>fontFamilyName<span class="pl-pds">"</span></span>;
  src<span class="pl-k">:</span> <span class="pl-en">url</span>(<span class="pl-s"><span class="pl-pds">"</span>fonts/fontFile.ttf<span class="pl-pds">"</span></span>);
}</pre></div>

<p>Add this <code>@font-face</code> rule to your HTML page's CSS style sheet.</p>

<p><a href="http://pixijs.github.io/docs/PIXI.extras.BitmapText.html">Pixi also has support for bitmap
fonts</a>. You
can use Pixi's loader to load Bitmap font XML files, the same way you
load JSON or image files.</p>

<p><a id="user-content-collision"></a></p>

<h2><a id="user-content-collision-detection" class="anchor" href="#collision-detection" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Collision detection</h2>

<p>You now know how to make a huge variety of graphics objects, but what
can you do with them? A fun thing to do is to build a simple <strong>collision
detection</strong> system. You can use a custom function called
<code>hitTestRectangle</code> that checks whether any two rectangular Pixi sprites are
touching.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-en">hitTestRectangle</span>(spriteOne, spriteTwo)</pre></div>

<p>if they overlap, <code>hitTestRectangle</code> will return <code>true</code>. You can use <code>hitTestRectangle</code> with an <code>if</code> statement to check for a collision between two sprites like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">if</span> (<span class="pl-en">hitTestRectangle</span>(cat, box)) {
  <span class="pl-c">//There's a collision</span>
} <span class="pl-k">else</span> {
  <span class="pl-c">//There's no collision</span>
}</pre></div>

<p>As you'll see, <code>hitTestRectangle</code> is the front door into the vast universe of game design.</p>

<p>Run the <code>collisionDetection.html</code> file in the <code>examples</code> folder for a
working example of how to use <code>hitTestRectangle</code>. Use the arrow keys
to move the cat. If the cat hits the box, the box becomes red
and "Hit!" is displayed by the text object.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s25.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/25.png" alt="Displaying text" style="max-width:100%;"></a></p>

<p>You've already seen all the code that creates all these elements, as
well as the
keyboard control system that makes the cat move. The only new thing is the
way <code>hitTestRectangle</code> is used inside the <code>play</code> function to check for a
collision.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">play</span>() {

  <span class="pl-c">//use the cat's velocity to make it move</span>
  <span class="pl-smi">cat</span>.<span class="pl-c1">x</span> <span class="pl-k">+=</span> <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span>;
  <span class="pl-smi">cat</span>.<span class="pl-c1">y</span> <span class="pl-k">+=</span> <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span>;

  <span class="pl-c">//check for a collision between the cat and the box</span>
  <span class="pl-k">if</span> (<span class="pl-en">hitTestRectangle</span>(cat, box)) {

    <span class="pl-c">//if there's a collision, change the message text</span>
    <span class="pl-c">//and tint the box red</span>
    <span class="pl-smi">message</span>.<span class="pl-c1">text</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">"</span>hit!<span class="pl-pds">"</span></span>;
    <span class="pl-smi">box</span>.<span class="pl-smi">tint</span> <span class="pl-k">=</span> <span class="pl-c1">0xff3300</span>;

  } <span class="pl-k">else</span> {

    <span class="pl-c">//if there's no collision, reset the message</span>
    <span class="pl-c">//text and the box's color</span>
    <span class="pl-smi">message</span>.<span class="pl-c1">text</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">"</span>No collision...<span class="pl-pds">"</span></span>;
    <span class="pl-smi">box</span>.<span class="pl-smi">tint</span> <span class="pl-k">=</span> <span class="pl-c1">0xccff99</span>;
  }
}</pre></div>

<p>Because the <code>play</code> function is being called by the game loop 60 times
per second, this <code>if</code> statement is constantly checking for a collision
between the cat and the box. If <code>hitTestRectangle</code> is <code>true</code>, the
text <code>message</code> object uses <code>setText</code> to display "Hit":</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">message</span>.<span class="pl-c1">text</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">"</span>hit!<span class="pl-pds">"</span></span>;</pre></div>

<p>The color of the box is then changed from green to red by setting the
box's <code>tint</code> property to the hexadecimal red value.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">box</span>.<span class="pl-smi">tint</span> <span class="pl-k">=</span> <span class="pl-c1">0xff3300</span>;</pre></div>

<p>If there's no collision, the message and box are maintained in their
original states:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">message</span>.<span class="pl-c1">text</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">"</span>no collision...<span class="pl-pds">"</span></span>;
<span class="pl-smi">box</span>.<span class="pl-smi">tint</span> <span class="pl-k">=</span> <span class="pl-c1">0xccff99</span>;</pre></div>

<p>This code is pretty simple, but suddenly you've created an interactive
world that seems to be completely alive. It's almost like magic! And, perhaps
surprisingly, you now have all the skills you need to start making
games with Pixi!</p>

<p><a id="user-content-hittest"></a></p>

<h3><a id="user-content-the-hittestrectangle-function" class="anchor" href="#the-hittestrectangle-function" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>The hitTestRectangle function</h3>

<p>But what about the <code>hitTestRectangle</code> function? What does it do, and
how does it work? The details of how collision detection algorithms
like this work is a little bit outside the scope of this tutorial.
The most important thing is that you know how to use it. But, just for
your reference, and in case you're curious, here's the complete
<code>hitTestRectangle</code> function definition. Can you figure out from the
comments what it's doing?</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">hitTestRectangle</span>(<span class="pl-smi">r1</span>, <span class="pl-smi">r2</span>) {

  <span class="pl-c">//Define the variables we'll need to calculate</span>
  <span class="pl-k">var</span> hit, combinedHalfWidths, combinedHalfHeights, vx, vy;

  <span class="pl-c">//hit will determine whether there's a collision</span>
  hit <span class="pl-k">=</span> <span class="pl-c1">false</span>;

  <span class="pl-c">//Find the center points of each sprite</span>
  <span class="pl-smi">r1</span>.<span class="pl-smi">centerX</span> <span class="pl-k">=</span> <span class="pl-smi">r1</span>.<span class="pl-c1">x</span> <span class="pl-k">+</span> <span class="pl-smi">r1</span>.<span class="pl-c1">width</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;
  <span class="pl-smi">r1</span>.<span class="pl-smi">centerY</span> <span class="pl-k">=</span> <span class="pl-smi">r1</span>.<span class="pl-c1">y</span> <span class="pl-k">+</span> <span class="pl-smi">r1</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;
  <span class="pl-smi">r2</span>.<span class="pl-smi">centerX</span> <span class="pl-k">=</span> <span class="pl-smi">r2</span>.<span class="pl-c1">x</span> <span class="pl-k">+</span> <span class="pl-smi">r2</span>.<span class="pl-c1">width</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;
  <span class="pl-smi">r2</span>.<span class="pl-smi">centerY</span> <span class="pl-k">=</span> <span class="pl-smi">r2</span>.<span class="pl-c1">y</span> <span class="pl-k">+</span> <span class="pl-smi">r2</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;

  <span class="pl-c">//Find the half-widths and half-heights of each sprite</span>
  <span class="pl-smi">r1</span>.<span class="pl-smi">halfWidth</span> <span class="pl-k">=</span> <span class="pl-smi">r1</span>.<span class="pl-c1">width</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;
  <span class="pl-smi">r1</span>.<span class="pl-smi">halfHeight</span> <span class="pl-k">=</span> <span class="pl-smi">r1</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;
  <span class="pl-smi">r2</span>.<span class="pl-smi">halfWidth</span> <span class="pl-k">=</span> <span class="pl-smi">r2</span>.<span class="pl-c1">width</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;
  <span class="pl-smi">r2</span>.<span class="pl-smi">halfHeight</span> <span class="pl-k">=</span> <span class="pl-smi">r2</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;

  <span class="pl-c">//Calculate the distance vector between the sprites</span>
  vx <span class="pl-k">=</span> <span class="pl-smi">r1</span>.<span class="pl-smi">centerX</span> <span class="pl-k">-</span> <span class="pl-smi">r2</span>.<span class="pl-smi">centerX</span>;
  vy <span class="pl-k">=</span> <span class="pl-smi">r1</span>.<span class="pl-smi">centerY</span> <span class="pl-k">-</span> <span class="pl-smi">r2</span>.<span class="pl-smi">centerY</span>;

  <span class="pl-c">//Figure out the combined half-widths and half-heights</span>
  combinedHalfWidths <span class="pl-k">=</span> <span class="pl-smi">r1</span>.<span class="pl-smi">halfWidth</span> <span class="pl-k">+</span> <span class="pl-smi">r2</span>.<span class="pl-smi">halfWidth</span>;
  combinedHalfHeights <span class="pl-k">=</span> <span class="pl-smi">r1</span>.<span class="pl-smi">halfHeight</span> <span class="pl-k">+</span> <span class="pl-smi">r2</span>.<span class="pl-smi">halfHeight</span>;

  <span class="pl-c">//Check for a collision on the x axis</span>
  <span class="pl-k">if</span> (<span class="pl-c1">Math</span>.<span class="pl-c1">abs</span>(vx) <span class="pl-k">&lt;</span> combinedHalfWidths) {

    <span class="pl-c">//A collision might be occuring. Check for a collision on the y axis</span>
    <span class="pl-k">if</span> (<span class="pl-c1">Math</span>.<span class="pl-c1">abs</span>(vy) <span class="pl-k">&lt;</span> combinedHalfHeights) {

      <span class="pl-c">//There's definitely a collision happening</span>
      hit <span class="pl-k">=</span> <span class="pl-c1">true</span>;
    } <span class="pl-k">else</span> {

      <span class="pl-c">//There's no collision on the y axis</span>
      hit <span class="pl-k">=</span> <span class="pl-c1">false</span>;
    }
  } <span class="pl-k">else</span> {

    <span class="pl-c">//There's no collision on the x axis</span>
    hit <span class="pl-k">=</span> <span class="pl-c1">false</span>;
  }

  <span class="pl-c">//`hit` will be either `true` or `false`</span>
  <span class="pl-k">return</span> hit;
};
</pre></div>

<p><a id="user-content-casestudy"></a></p>

<h2><a id="user-content-case-study-treasure-hunter" class="anchor" href="#case-study-treasure-hunter" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Case study: Treasure Hunter</h2>

<p>So I told you that you now have all the skills you need to start
making games. What? You don't believe me? Let me prove it to you! Let’s take a
close at how to make a simple object collection and enemy
avoidance game called <strong>Treasure Hunter</strong>. (You'll find it the <code>examples</code>
folder.)</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s26.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/26.png" alt="Treasure Hunter" style="max-width:100%;"></a></p>

<p>Treasure Hunter is a good example of one of simplest complete
games you can make using the tools you've learnt so far. Use the
keyboard arrow
keys to help the explorer find the treasure and carry it to the exit.
Six blob monsters move up and down between the dungeon walls, and if
they hit the explorer he becomes semi-transparent and the health meter
at the top right corner shrinks. If all the health is used up, “You
Lost!” is displayed on the stage; if the explorer reaches the exit with
the treasure, “You Won!” is displayed. Although it’s a basic
prototype, Treasure Hunter contains most of the elements you’ll find
in much bigger games: texture atlas graphics, interactivity,
collision, and multiple game scenes. Let’s go on a tour of how the
game was put together so that you can use it as a starting point for one of your own games.</p>

<h3><a id="user-content-the-code-structure" class="anchor" href="#the-code-structure" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>The code structure</h3>

<p>Open the <code>treasureHunter.html</code> file and you'll see that all the game
code is in one big file. Here's a birds-eye view of how all the code is
organized.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c">//Setup Pixi and load the texture atlas files - call the `setup`</span>
<span class="pl-c">//function when they've loaded</span>

<span class="pl-k">function</span> <span class="pl-en">setup</span>() {
  <span class="pl-c">//Initialize the game sprites, set the game `state` to `play`</span>
  <span class="pl-c">//and start the 'gameLoop'</span>
}

<span class="pl-k">function</span> <span class="pl-en">gameLoop</span>() {
  <span class="pl-c">//Runs the current game `state` in a loop and renders the sprites</span>
}

<span class="pl-k">function</span> <span class="pl-en">play</span>() {
  <span class="pl-c">//All the game logic goes here</span>
}

<span class="pl-k">function</span> <span class="pl-en">end</span>() {
  <span class="pl-c">//All the code that should run at the end of the game</span>
}

<span class="pl-c">//The game's helper functions:</span>
<span class="pl-c">//`keyboard`, `hitTestRectangle`, `contain` and `randomInt`</span></pre></div>

<p>Use this as your world map to the game as we look at how each
section works.</p>

<p><a id="user-content-initialize"></a></p>

<h3><a id="user-content-initialize-the-game-in-the-setup-function" class="anchor" href="#initialize-the-game-in-the-setup-function" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Initialize the game in the setup function</h3>

<p>As soon as the texture atlas images have loaded, the <code>setup</code> function
runs. It only runs once, and lets you perform
one-time setup tasks for your game. It's a great place to create and initialize
objects, sprites, game scenes, populate data arrays or parse
loaded JSON game data.</p>

<p>Here's an abridged view of the <code>setup</code> function in Treasure Hunter,
and the tasks that it performs.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">setup</span>() {
  <span class="pl-c">//Create the `gameScene` group</span>
  <span class="pl-c">//Create the `door` sprite</span>
  <span class="pl-c">//Create the `player` sprite</span>
  <span class="pl-c">//Create the `treasure` sprite</span>
  <span class="pl-c">//Make the enemies</span>
  <span class="pl-c">//Create the health bar</span>
  <span class="pl-c">//Add some text for the game over message</span>
  <span class="pl-c">//Create a `gameOverScene` group</span>
  <span class="pl-c">//Assign the player's keyboard controllers</span>

  <span class="pl-c">//set the game state to `play`</span>
  state <span class="pl-k">=</span> play;

  <span class="pl-c">//Start the game loop</span>
  <span class="pl-en">gameLoop</span>();
}
</pre></div>

<p>The last two lines of code, <code>state = play;</code> and <code>gameLoop()</code> are perhaps
the most important. Running <code>gameLoop</code> switches on the game's engine,
and causes the <code>play</code> function to be called in a continuous loop. But before we look at how that works, let's see what the
specific code inside the <code>setup</code> function does.</p>

<p><a id="user-content-gamescene"></a></p>

<h4><a id="user-content-creating-the-game-scenes" class="anchor" href="#creating-the-game-scenes" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Creating the game scenes</h4>

<p>The <code>setup</code> function creates two <code>Container</code> groups called
<code>gameScene</code> and <code>gameOverScene</code>. Each of these are added to the stage.</p>

<div class="highlight highlight-source-js"><pre>gameScene <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Container</span>();
<span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(gameScene);

gameOverScene <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Container</span>();
<span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(gameOverScene);
</pre></div>

<p>All of the sprites that are part of the main game are added to the
<code>gameScene</code> group. The game over text that should be displayed at the
end of the game is added to the <code>gameOverScene</code> group.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s27.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/27.png" alt="Displaying text" style="max-width:100%;"></a></p>

<p>Although it's created in the <code>setup</code> function, the <code>gameOverScene</code>
shouldn't be visible when the game first starts, so its <code>visible</code>
property is initialized to <code>false</code>.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">gameOverScene</span>.<span class="pl-smi">visible</span> <span class="pl-k">=</span> <span class="pl-c1">false</span>;</pre></div>

<p>You'll see ahead that, when the game ends, the <code>gameOverScene</code>'s <code>visible</code>
property will be set to <code>true</code> to display the text that appears at the
end of the game.</p>

<p><a id="user-content-makingdungon"></a></p>

<h4><a id="user-content-making-the-dungeon-door-explorer-and-treasure" class="anchor" href="#making-the-dungeon-door-explorer-and-treasure" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Making the dungeon, door, explorer and treasure</h4>

<p>The player, exit door, treasure chest and the dungeon background image
are all sprites made from texture atlas frames. Very importantly,
they're all added as children of the <code>gameScene</code>.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c">//Create an alias for the texture atlas frame ids</span>
id <span class="pl-k">=</span> resources[<span class="pl-s"><span class="pl-pds">"</span>images/treasureHunter.json<span class="pl-pds">"</span></span>].<span class="pl-smi">textures</span>;

<span class="pl-c">//Dungeon</span>
dungeon <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(id[<span class="pl-s"><span class="pl-pds">"</span>dungeon.png<span class="pl-pds">"</span></span>]);
<span class="pl-smi">gameScene</span>.<span class="pl-en">addChild</span>(dungeon);

<span class="pl-c">//Door</span>
door <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(id[<span class="pl-s"><span class="pl-pds">"</span>door.png<span class="pl-pds">"</span></span>]);
<span class="pl-smi">door</span>.<span class="pl-smi">position</span>.<span class="pl-c1">set</span>(<span class="pl-c1">32</span>, <span class="pl-c1">0</span>);
<span class="pl-smi">gameScene</span>.<span class="pl-en">addChild</span>(door);

<span class="pl-c">//Explorer</span>
explorer <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(id[<span class="pl-s"><span class="pl-pds">"</span>explorer.png<span class="pl-pds">"</span></span>]);
<span class="pl-smi">explorer</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">68</span>;
<span class="pl-smi">explorer</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-smi">gameScene</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span> <span class="pl-k">-</span> <span class="pl-smi">explorer</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;
<span class="pl-smi">explorer</span>.<span class="pl-smi">vx</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
<span class="pl-smi">explorer</span>.<span class="pl-smi">vy</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
<span class="pl-smi">gameScene</span>.<span class="pl-en">addChild</span>(explorer);

<span class="pl-c">//Treasure</span>
treasure <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(id[<span class="pl-s"><span class="pl-pds">"</span>treasure.png<span class="pl-pds">"</span></span>]);
<span class="pl-smi">treasure</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-smi">gameScene</span>.<span class="pl-c1">width</span> <span class="pl-k">-</span> <span class="pl-smi">treasure</span>.<span class="pl-c1">width</span> <span class="pl-k">-</span> <span class="pl-c1">48</span>;
<span class="pl-smi">treasure</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-smi">gameScene</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span> <span class="pl-k">-</span> <span class="pl-smi">treasure</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;
<span class="pl-smi">gameScene</span>.<span class="pl-en">addChild</span>(treasure);</pre></div>

<p>Keeping them together in the <code>gameScene</code> group will make it easy for
us to hide the <code>gameScene</code> and display the <code>gameOverScene</code> when the game is finished.</p>

<p><a id="user-content-makingblob"></a></p>

<h4><a id="user-content-making-the-blob-monsters" class="anchor" href="#making-the-blob-monsters" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Making the blob monsters</h4>

<p>The six blob monsters are created in a loop. Each blob is given a
random initial position and velocity. The vertical velocity is
alternately multiplied by <code>1</code> or <code>-1</code> for each blob, and that’s what
causes each blob to move in the opposite direction to the one next to
it. Each blob monster that's created is pushed into an array called
<code>blobs</code>.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> numberOfBlobs <span class="pl-k">=</span> <span class="pl-c1">6</span>,
    spacing <span class="pl-k">=</span> <span class="pl-c1">48</span>,
    xOffset <span class="pl-k">=</span> <span class="pl-c1">150</span>,
    speed <span class="pl-k">=</span> <span class="pl-c1">2</span>,
    direction <span class="pl-k">=</span> <span class="pl-c1">1</span>;

<span class="pl-c">//An array to store all the blob monsters</span>
blobs <span class="pl-k">=</span> [];

<span class="pl-c">//Make as many blobs as there are `numberOfBlobs`</span>
<span class="pl-k">for</span> (<span class="pl-k">var</span> i <span class="pl-k">=</span> <span class="pl-c1">0</span>; i <span class="pl-k">&lt;</span> numberOfBlobs; i<span class="pl-k">++</span>) {

  <span class="pl-c">//Make a blob</span>
  <span class="pl-k">var</span> blob <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(id[<span class="pl-s"><span class="pl-pds">"</span>blob.png<span class="pl-pds">"</span></span>]);

  <span class="pl-c">//Space each blob horizontally according to the `spacing` value.</span>
  <span class="pl-c">//`xOffset` determines the point from the left of the screen</span>
  <span class="pl-c">//at which the first blob should be added</span>
  <span class="pl-k">var</span> x <span class="pl-k">=</span> spacing <span class="pl-k">*</span> i <span class="pl-k">+</span> xOffset;

  <span class="pl-c">//Give the blob a random `y` position</span>
  <span class="pl-k">var</span> y <span class="pl-k">=</span> <span class="pl-en">randomInt</span>(<span class="pl-c1">0</span>, <span class="pl-smi">stage</span>.<span class="pl-c1">height</span> <span class="pl-k">-</span> <span class="pl-smi">blob</span>.<span class="pl-c1">height</span>);

  <span class="pl-c">//Set the blob's position</span>
  <span class="pl-smi">blob</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> x;
  <span class="pl-smi">blob</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> y;

  <span class="pl-c">//Set the blob's vertical velocity. `direction` will be either `1` or</span>
  <span class="pl-c">//`-1`. `1` means the enemy will move down and `-1` means the blob will</span>
  <span class="pl-c">//move up. Multiplying `direction` by `speed` determines the blob's</span>
  <span class="pl-c">//vertical direction</span>
  <span class="pl-smi">blob</span>.<span class="pl-smi">vy</span> <span class="pl-k">=</span> speed <span class="pl-k">*</span> direction;

  <span class="pl-c">//Reverse the direction for the next blob</span>
  direction <span class="pl-k">*=</span> <span class="pl-k">-</span><span class="pl-c1">1</span>;

  <span class="pl-c">//Push the blob into the `blobs` array</span>
  <span class="pl-smi">blobs</span>.<span class="pl-c1">push</span>(blob);

  <span class="pl-c">//Add the blob to the `gameScene`</span>
  <span class="pl-smi">gameScene</span>.<span class="pl-en">addChild</span>(blob);
}
</pre></div>

<p><a id="user-content-healthbar"></a></p>

<h4><a id="user-content-making-the-health-bar" class="anchor" href="#making-the-health-bar" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Making the health bar</h4>

<p>When you play Treasure Hunter you'll notice that when the explorer touches
one of the enemies, the width of the health bar at the top right
corner of the screen decreases. How was this health bar made? It's
just two overlapping rectangles at exactly the same position: a black rectangle behind, and
a red rectangle in front. They're grouped into a single <code>healthBar</code>
group. The <code>healthBar</code> is then added to the <code>gameScene</code> and positioned
on the stage.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c">//Create the health bar</span>
healthBar <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.DisplayObjectContainer</span>();
<span class="pl-smi">healthBar</span>.<span class="pl-smi">position</span>.<span class="pl-c1">set</span>(<span class="pl-smi">stage</span>.<span class="pl-c1">width</span> <span class="pl-k">-</span> <span class="pl-c1">170</span>, <span class="pl-c1">6</span>)
<span class="pl-smi">gameScene</span>.<span class="pl-en">addChild</span>(healthBar);

<span class="pl-c">//Create the black background rectangle</span>
<span class="pl-k">var</span> innerBar <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.Graphics</span>();
<span class="pl-smi">innerBar</span>.<span class="pl-en">beginFill</span>(<span class="pl-c1">0x000000</span>);
<span class="pl-smi">innerBar</span>.<span class="pl-en">drawRect</span>(<span class="pl-c1">0</span>, <span class="pl-c1">0</span>, <span class="pl-c1">128</span>, <span class="pl-c1">8</span>);
<span class="pl-smi">innerBar</span>.<span class="pl-en">endFill</span>();
<span class="pl-smi">healthBar</span>.<span class="pl-en">addChild</span>(innerBar);

<span class="pl-c">//Create the front red rectangle</span>
<span class="pl-k">var</span> outerBar <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.Graphics</span>();
<span class="pl-smi">outerBar</span>.<span class="pl-en">beginFill</span>(<span class="pl-c1">0xFF3300</span>);
<span class="pl-smi">outerBar</span>.<span class="pl-en">drawRect</span>(<span class="pl-c1">0</span>, <span class="pl-c1">0</span>, <span class="pl-c1">128</span>, <span class="pl-c1">8</span>);
<span class="pl-smi">outerBar</span>.<span class="pl-en">endFill</span>();
<span class="pl-smi">healthBar</span>.<span class="pl-en">addChild</span>(outerBar);

<span class="pl-smi">healthBar</span>.<span class="pl-smi">outer</span> <span class="pl-k">=</span> outerBar;</pre></div>

<p>You can see that a property called <code>outer</code> has been added to the
<code>healthBar</code>. It just references the <code>outerBar</code> (the red rectangle) so that it will be convenient to access later.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">healthBar</span>.<span class="pl-smi">outer</span> <span class="pl-k">=</span> outerBar;</pre></div>

<p>You don't have to do this; but, hey why not! It means that if you want
to control the width of the red <code>outerBar</code>, you can write some smooth code that looks like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">healthBar</span>.<span class="pl-smi">outer</span>.<span class="pl-c1">width</span> <span class="pl-k">=</span> <span class="pl-c1">30</span>;</pre></div>

<p>That's pretty neat and readable, so we'll keep it!</p>

<p><a id="user-content-message"></a></p>

<h4><a id="user-content-making-the-message-text" class="anchor" href="#making-the-message-text" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Making the message text</h4>

<p>When the game is finished, some text displays “You won!” or “You
lost!”, depending on the outcome of the game. This is made using a
text sprite and adding it to the <code>gameOverScene</code>. Because the
<code>gameOverScene</code>‘s <code>visible</code> property is set to <code>false</code> when the game
starts, you can’t see this text. Here’s the code from the <code>setup</code>
function that creates the message text and adds it to the
<code>gameOverScene</code>.</p>

<div class="highlight highlight-source-js"><pre>message <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Text</span>(
  <span class="pl-s"><span class="pl-pds">"</span>The End!<span class="pl-pds">"</span></span>,
  {font<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">"</span>64px Futura<span class="pl-pds">"</span></span>, fill<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">"</span>white<span class="pl-pds">"</span></span>}
);

<span class="pl-smi">message</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">120</span>;
<span class="pl-smi">message</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-smi">stage</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span> <span class="pl-k">-</span> <span class="pl-c1">32</span>;

<span class="pl-smi">gameOverScene</span>.<span class="pl-en">addChild</span>(message);</pre></div>

<p><a id="user-content-playing"></a></p>

<h3><a id="user-content-playing-the-game" class="anchor" href="#playing-the-game" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Playing the game</h3>

<p>All the game logic and the code that makes the sprites move happens
inside the <code>play</code> function, which runs in a continuous loop. Here's an
overview of what the <code>play</code> function does</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">play</span>() {
  <span class="pl-c">//Move the explorer and contain it inside the dungeon</span>
  <span class="pl-c">//Move the blob monsters</span>
  <span class="pl-c">//Check for a collision between the blobs and the explorer</span>
  <span class="pl-c">//Check for a collision between the explorer and the treasure</span>
  <span class="pl-c">//Check for a collsion between the treasure and the door</span>
  <span class="pl-c">//Decide whether the game has been won or lost</span>
  <span class="pl-c">//Change the game `state` to `end` when the game is finsihed</span>
}</pre></div>

<p>Let's find out how all these features work.</p>

<p><a id="user-content-movingexplorer"></a></p>

<h3><a id="user-content-moving-the-explorer" class="anchor" href="#moving-the-explorer" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Moving the explorer</h3>

<p>The explorer is controlled using the keyboard, and the code that does
that is very similar to the keyboard control code you learnt earlier.
The <code>keyboard</code> objects modify the explorer’s velocity, and that
velocity is added to the explorer’s position inside the <code>play</code>
function.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">explorer</span>.<span class="pl-c1">x</span> <span class="pl-k">+=</span> <span class="pl-smi">explorer</span>.<span class="pl-smi">vx</span>;
<span class="pl-smi">explorer</span>.<span class="pl-c1">y</span> <span class="pl-k">+=</span> <span class="pl-smi">explorer</span>.<span class="pl-smi">vy</span>;</pre></div>

<p><a id="user-content-containingmovement"></a></p>

<h4><a id="user-content-containing-movement" class="anchor" href="#containing-movement" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Containing movement</h4>

<p>But what's new is that the explorer's movement is contained inside the walls of the
dungeon. The green outline shows the limits of the explorer's
movement.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s28.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/28.png" alt="Displaying text" style="max-width:100%;"></a></p>

<p>That's done with the help of a custom function called
<code>contain</code>.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-en">contain</span>(explorer, {x<span class="pl-k">:</span> <span class="pl-c1">28</span>, y<span class="pl-k">:</span> <span class="pl-c1">10</span>, width<span class="pl-k">:</span> <span class="pl-c1">488</span>, height<span class="pl-k">:</span> <span class="pl-c1">480</span>});</pre></div>

<p><code>contain</code> takes two arguments. The first is the sprite you want to keep
contained. The second is any object with <code>x</code>, <code>y</code>, <code>width</code> and
<code>height</code> properties that define a rectangular area. In this example,
the containing object defines an area that's just slightly offset
from, and smaller than, the stage. It matches dimensions of the dungeon
walls.</p>

<p>Here's the <code>contain</code> function that does all this work. The function checks
to see if the sprite has crossed the boundaries of the containing
object. If it has, the code moves the sprite back into that boundary.
The <code>contain</code> function also returns a <code>collision</code> variable with the
value "top", "right", "bottom" or "left", depending on which side of
the boundary the sprite hit. (<code>collision</code> will be <code>undefined</code> if the
sprite didn't hit any of the boundaries.)</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">contain</span>(<span class="pl-smi">sprite</span>, <span class="pl-smi">container</span>) {

  <span class="pl-k">var</span> collision <span class="pl-k">=</span> <span class="pl-c1">undefined</span>;

  <span class="pl-c">//Left</span>
  <span class="pl-k">if</span> (<span class="pl-smi">sprite</span>.<span class="pl-c1">x</span> <span class="pl-k">&lt;</span> <span class="pl-smi">container</span>.<span class="pl-c1">x</span>) {
    <span class="pl-smi">sprite</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-smi">container</span>.<span class="pl-c1">x</span>;
    collision <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">"</span>left<span class="pl-pds">"</span></span>;
  }

  <span class="pl-c">//Top</span>
  <span class="pl-k">if</span> (<span class="pl-smi">sprite</span>.<span class="pl-c1">y</span> <span class="pl-k">&lt;</span> <span class="pl-smi">container</span>.<span class="pl-c1">y</span>) {
    <span class="pl-smi">sprite</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-smi">container</span>.<span class="pl-c1">y</span>;
    collision <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">"</span>top<span class="pl-pds">"</span></span>;
  }

  <span class="pl-c">//Right</span>
  <span class="pl-k">if</span> (<span class="pl-smi">sprite</span>.<span class="pl-c1">x</span> <span class="pl-k">+</span> <span class="pl-smi">sprite</span>.<span class="pl-c1">width</span> <span class="pl-k">&gt;</span> <span class="pl-smi">container</span>.<span class="pl-c1">width</span>) {
    <span class="pl-smi">sprite</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-smi">container</span>.<span class="pl-c1">width</span> <span class="pl-k">-</span> <span class="pl-smi">sprite</span>.<span class="pl-c1">width</span>;
    collision <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">"</span>right<span class="pl-pds">"</span></span>;
  }

  <span class="pl-c">//Bottom</span>
  <span class="pl-k">if</span> (<span class="pl-smi">sprite</span>.<span class="pl-c1">y</span> <span class="pl-k">+</span> <span class="pl-smi">sprite</span>.<span class="pl-c1">height</span> <span class="pl-k">&gt;</span> <span class="pl-smi">container</span>.<span class="pl-c1">height</span>) {
    <span class="pl-smi">sprite</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-smi">container</span>.<span class="pl-c1">height</span> <span class="pl-k">-</span> <span class="pl-smi">sprite</span>.<span class="pl-c1">height</span>;
    collision <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">"</span>bottom<span class="pl-pds">"</span></span>;
  }

  <span class="pl-c">//Return the `collision` value</span>
  <span class="pl-k">return</span> collision;
}</pre></div>

<p>You'll see how the <code>collision</code> return value will be use in the code
ahead to make the blob monsters bounce back and forth between the top
and bottom dungeon walls.</p>

<p><a id="user-content-movingmonsters"></a></p>

<h3><a id="user-content-moving-the-monsters" class="anchor" href="#moving-the-monsters" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Moving the monsters</h3>

<p>The <code>play</code> function also moves the blob monsters, keeps them contained
inside the dungeon walls, and checks each one for a collision with the
player. If a blob bumps into the dungeon’s top or bottom walls, its
direction is reversed. All this is done with the help of a <code>forEach</code> loop
which iterates through each of <code>blob</code> sprites in the <code>blobs</code> array on
every frame.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">blobs</span>.<span class="pl-c1">forEach</span>(<span class="pl-k">function</span>(<span class="pl-smi">blob</span>) {

  <span class="pl-c">//Move the blob</span>
  <span class="pl-smi">blob</span>.<span class="pl-c1">y</span> <span class="pl-k">+=</span> <span class="pl-smi">blob</span>.<span class="pl-smi">vy</span>;

  <span class="pl-c">//Check the blob's screen boundaries</span>
  <span class="pl-k">var</span> blobHitsWall <span class="pl-k">=</span> <span class="pl-en">contain</span>(blob, {x<span class="pl-k">:</span> <span class="pl-c1">28</span>, y<span class="pl-k">:</span> <span class="pl-c1">10</span>, width<span class="pl-k">:</span> <span class="pl-c1">488</span>, height<span class="pl-k">:</span> <span class="pl-c1">480</span>});

  <span class="pl-c">//If the blob hits the top or bottom of the stage, reverse</span>
  <span class="pl-c">//its direction</span>
  <span class="pl-k">if</span> (blobHitsWall <span class="pl-k">===</span> <span class="pl-s"><span class="pl-pds">"</span>top<span class="pl-pds">"</span></span> <span class="pl-k">||</span> blobHitsWall <span class="pl-k">===</span> <span class="pl-s"><span class="pl-pds">"</span>bottom<span class="pl-pds">"</span></span>) {
    <span class="pl-smi">blob</span>.<span class="pl-smi">vy</span> <span class="pl-k">*=</span> <span class="pl-k">-</span><span class="pl-c1">1</span>;
  }

  <span class="pl-c">//Test for a collision. If any of the enemies are touching</span>
  <span class="pl-c">//the explorer, set `explorerHit` to `true`</span>
  <span class="pl-k">if</span>(<span class="pl-en">hitTestRectangle</span>(explorer, blob)) {
    explorerHit <span class="pl-k">=</span> <span class="pl-c1">true</span>;
  }
});
</pre></div>

<p>You can see in this code above how the return value of the <code>contain</code>
function is used to make the blobs bounce off the walls. A variable
called <code>blobHitsWall</code> is used to capture the return value:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> blobHitsWall <span class="pl-k">=</span> <span class="pl-en">contain</span>(blob, {x<span class="pl-k">:</span> <span class="pl-c1">28</span>, y<span class="pl-k">:</span> <span class="pl-c1">10</span>, width<span class="pl-k">:</span> <span class="pl-c1">488</span>, height<span class="pl-k">:</span> <span class="pl-c1">480</span>});</pre></div>

<p><code>blobHitsWall</code> will usually be <code>undefined</code>. But if the blob hits the
top wall, <code>blobHitsWall</code> will have the value "top". If the blob hits
the bottom wall, <code>blobHitsWall</code> will have the value "bottom". If
either of these cases are <code>true</code>, you can reverse the blob's direction
by reversing its velocity. Here's the code that does this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">if</span> (blobHitsWall <span class="pl-k">===</span> <span class="pl-s"><span class="pl-pds">"</span>top<span class="pl-pds">"</span></span> <span class="pl-k">||</span> blobHitsWall <span class="pl-k">===</span> <span class="pl-s"><span class="pl-pds">"</span>bottom<span class="pl-pds">"</span></span>) {
  <span class="pl-smi">blob</span>.<span class="pl-smi">vy</span> <span class="pl-k">*=</span> <span class="pl-k">-</span><span class="pl-c1">1</span>;
}</pre></div>

<p>Multiplying the blob's <code>vy</code> (vertical velocity) value by <code>-1</code> will flip
the direction of its movement.</p>

<p><a id="user-content-checkingcollisions"></a></p>

<h3><a id="user-content-checking-for-collisions" class="anchor" href="#checking-for-collisions" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Checking for collisions</h3>

<p>The code in the loop above uses <code>hitTestRectangle</code> to figure
out if any of the enemies have touched the explorer.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">if</span>(<span class="pl-en">hitTestRectangle</span>(explorer, blob)) {
  explorerHit <span class="pl-k">=</span> <span class="pl-c1">true</span>;
}</pre></div>

<p>If <code>hitTestRectangle</code> returns <code>true</code>, it means there’s been a collision
and a variable called <code>explorerHit</code> is set to <code>true</code>. If <code>explorerHit</code>
is <code>true</code>, the <code>play</code> function makes the explorer semi-transparent
and reduces the width of the <code>health</code> bar by 1 pixel.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">if</span>(explorerHit) {

  <span class="pl-c">//Make the explorer semi-transparent</span>
  <span class="pl-smi">explorer</span>.<span class="pl-smi">alpha</span> <span class="pl-k">=</span> <span class="pl-c1">0.5</span>;

  <span class="pl-c">//Reduce the width of the health bar's inner rectangle by 1 pixel</span>
  <span class="pl-smi">healthBar</span>.<span class="pl-smi">outer</span>.<span class="pl-c1">width</span> <span class="pl-k">-=</span> <span class="pl-c1">1</span>;

} <span class="pl-k">else</span> {

  <span class="pl-c">//Make the explorer fully opaque (non-transparent) if it hasn't been hit</span>
  <span class="pl-smi">explorer</span>.<span class="pl-smi">alpha</span> <span class="pl-k">=</span> <span class="pl-c1">1</span>;
}
</pre></div>

<p>If  <code>explorerHit</code> is <code>false</code>, the explorer's <code>alpha</code> property is
maintained at 1, which makes it fully opaque.</p>

<p>The <code>play</code> function also checks for a collision between the treasure
chest and the explorer. If there’s a hit, the <code>treasure</code> is set to the
explorer’s position, with a slight offset. This makes it look like the
explorer is carrying the treasure.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s29.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/29.png" alt="Displaying text" style="max-width:100%;"></a></p>

<p>Here's the code that does this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">if</span> (<span class="pl-en">hitTestRectangle</span>(explorer, treasure)) {
  <span class="pl-smi">treasure</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-smi">explorer</span>.<span class="pl-c1">x</span> <span class="pl-k">+</span> <span class="pl-c1">8</span>;
  <span class="pl-smi">treasure</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-smi">explorer</span>.<span class="pl-c1">y</span> <span class="pl-k">+</span> <span class="pl-c1">8</span>;
}</pre></div>

<p><a id="user-content-reachingexit"></a></p>

<h3><a id="user-content-reaching-the-exit-door-and-ending-the-game" class="anchor" href="#reaching-the-exit-door-and-ending-the-game" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Reaching the exit door and ending the game</h3>

<p>There are two ways the game can end: You can win if you carry the
treasure to the exit, or you can lose if you run out of health.</p>

<p>To win the game, the treasure chest just needs to touch the exit door. If
that happens, the game <code>state</code> is set to <code>end</code>, and the <code>message</code> text
displays "You won".</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">if</span> (<span class="pl-en">hitTestRectangle</span>(treasure, door)) {
  state <span class="pl-k">=</span> end;
  <span class="pl-smi">message</span>.<span class="pl-c1">text</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">"</span>You won!<span class="pl-pds">"</span></span>;
}</pre></div>

<p>If you run out of health, you lose the game. The game <code>state</code> is also
set to <code>end</code> and the <code>message</code> text displays "You Lost!"</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">if</span> (<span class="pl-smi">healthBar</span>.<span class="pl-smi">outer</span>.<span class="pl-c1">width</span> <span class="pl-k">&lt;</span> <span class="pl-c1">0</span>) {
  state <span class="pl-k">=</span> end;
  <span class="pl-smi">message</span>.<span class="pl-c1">text</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">"</span>You lost!<span class="pl-pds">"</span></span>;
}</pre></div>

<p>But what does this mean?</p>

<div class="highlight highlight-source-js"><pre>state <span class="pl-k">=</span> end;</pre></div>

<p>You'll remember from earlier examples that the <code>gameLoop</code> is constantly updating a function called
<code>state</code> at 60 times per second. Here's the <code>gameLoop</code>that does this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">gameLoop</span>(){

  <span class="pl-c">//Loop this function 60 times per second</span>
  <span class="pl-en">requestAnimationFrame</span>(gameLoop);

  <span class="pl-c">//Update the current game state</span>
  <span class="pl-en">state</span>();

  <span class="pl-c">//Render the stage</span>
  <span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);
}</pre></div>

<p>You'll also remember that we initially set the value of
<code>state</code> to <code>play</code>, which is why the <code>play</code> function runs in a loop.
By setting <code>state</code> to <code>end</code> we're telling the code that we want
another function, called <code>end</code> to run in a loop. In a bigger game you
could have a <code>tileScene</code> state, and states for each game level, like
<code>leveOne</code>, <code>levelTwo</code> and <code>levelThree</code>.</p>

<p>So what is that <code>end</code> function? Here it is!</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">end</span>() {
  <span class="pl-smi">gameScene</span>.<span class="pl-smi">visible</span> <span class="pl-k">=</span> <span class="pl-c1">false</span>;
  <span class="pl-smi">gameOverScene</span>.<span class="pl-smi">visible</span> <span class="pl-k">=</span> <span class="pl-c1">true</span>;
}</pre></div>

<p>It just flips the visibility of the game scenes. This is what hides
the <code>gameScene</code> and displays the <code>gameOverScene</code> when the game ends.</p>

<p>This is a really simple example of how to switch a game's state, but
you can have as many game states as you like in your games, and fill them
with as much code as you need. Just change the value of <code>state</code> to
whatever function you want to run in a loop.</p>

<p>And that’s really all there is to Treasure Hunter! With a little more work you could turn this simple prototype into a full game – try it!</p>

<p><a id="user-content-spriteproperties"></a></p>

<h2><a id="user-content-more-about-sprites" class="anchor" href="#more-about-sprites" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>More about sprites</h2>

<p>You've learnt how to use quite a few useful sprite properties so far, like <code>x</code>, <code>y</code>,
<code>visible</code>, and <code>rotation</code> that give you a lot of control over a
sprite's position and appearance. But Pixi Sprites also have many more
useful properties that are fun to play with. <a href="http://pixijs.github.io/docs/PIXI.Sprite.html">Here's the full list.</a></p>

<p>How does Pixi’s class inheritance system work? (<a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Introduction_to_Object-Oriented_JavaScript">What is a <strong>class</strong>
and what is <strong>inheritence</strong>? Click this link to find out.</a>) Pixi’s sprites are
built on an inheritance model that follows this chain:</p>

<pre><code>DisplayObject &gt; Container &gt; Sprite
</code></pre>

<p>Inheritance just means that the classes later in the chain use
properties and methods from classes earlier in the chain.
The most basic class is <code>DisplayObject</code>. Anything that’s a
<code>DisplayObject</code> can be rendered on the stage. <code>Container</code>
is the next class in the inheritance chain. It allows <code>DisplayObject</code>s
to act as containers for other <code>DisplayObject</code>s. Third up the chain is
the <code>Sprite</code> class. That’s the class you’ll be using to make most of
your game objects. However, later you’ll learn how to use
<code>Container</code>s to group sprites together.</p>

<p><a id="user-content-takingitfurther"></a></p>

<h2><a id="user-content-taking-it-further" class="anchor" href="#taking-it-further" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Taking it further</h2>

<p>Pixi can do a lot, but it can't do everything! If you want to start
making games or complex interactive applications with Pixi, you'll need
to use some helper libraries:</p>

<ul>
<li><a href="https://github.com/kittykatattack/bump">Bump</a>: A complete suite of 2D collision functions for games.</li>
<li><a href="https://github.com/kittykatattack/tink">Tink</a>: Drag-and-drop, buttons, a universal pointer and other
helpful interactivity tools.</li>
<li><a href="https://github.com/kittykatattack/charm">Charm</a>: Easy-to-use tweening animation effects for Pixi sprites.</li>
<li><a href="https://github.com/kittykatattack/dust">Dust</a>: Particle effects for creating things like explosions, fire
and magic.</li>
<li><a href="https://github.com/kittykatattack/spriteUtilities">Sprite Utilities</a>: Easier and more intuitive ways to
create and use Pixi sprites, as well adding a state machine and
animation player. Makes working with Pixi a lot more fun.</li>
<li><a href="https://github.com/kittykatattack/sound.js">Sound.js</a>: A micro-library for loading, controlling and generating
sound and music effects. Everything you need to add sound to games.</li>
<li><a href="https://github.com/kittykatattack/smoothie">Smoothie</a>: Ultra-smooth sprite animation using true delta-time interpolation. It also lets you specify the fps (frames-per-second) at which your game or application runs, and completely separates your sprite rendering loop from your application logic loop.</li>
</ul>

<p>You can find out how to use all these libraries with Pixi in the book 
<a href="http://www.springer.com/us/book/9781484210956">Learn PixiJS</a>.</p>

<p><a id="user-content-hexi"></a></p>

<h3><a id="user-content-hexi" class="anchor" href="#hexi" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Hexi</h3>

<p>Do you want to use all the functionality of those libraries, but don't
want the hassle of integrating them yourself? Use <strong>Hexi</strong>: a complete
development environment for building games and interactive
applications:</p>

<p><a href="https://github.com/kittykatattack/hexi">https://github.com/kittykatattack/hexi</a></p>

<p>It bundles the best version of Pixi (the latest stable one) with all these 
libraries (and more!) for a simple and fun way to make games. Hexi also
lets you access the global <code>PIXI</code> object directly, so you can write
low-level Pixi code directly in a Hexi application, and optionally choose to use as many or
as few of Hexi's extra conveniences as you need.</p>

<p><a id="user-content-supportingthisproject"></a></p>

<h2><a id="user-content-please-help-to-support-this-project" class="anchor" href="#please-help-to-support-this-project" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Please help to support this project!</h2>

<p>Buy the book! Incredibly, someone actually paid me to finish writing this tutorial
and turn it into a book! </p>

<p><a href="http://www.springer.com/us/book/9781484210956">Learn PixiJS</a></p>

<p>(And it's not just some junky "e-book", but a real, heavy, paper book, published by Springer,
the world's largest publisher! That means you can invite your friends
over, set it on fire, and roast marshmallows!!) There's 80% more
content than what's in this tutorial, and it's
packed full of all the essential techniques you need to know to use
Pixi to make all kinds of interactive applications and games.</p>

<p>Find out how to:</p>

<ul>
<li>Make animated game characters.</li>
<li>Create a full-featured animation state player.</li>
<li>Dynamically animate lines and shapes.</li>
<li>Use tiling sprites for infinite parallax scrolling.</li>
<li>Use blend modes, filters, tinting, masks, video, and render textures.</li>
<li>Produce content for multiple resolutions.</li>
<li>Create interactive buttons.</li>
<li>Create a flexible drag and drop interface for Pixi.</li>
<li>Create particle effects.</li>
<li>Build a stable software architectural model that will scale to any size.</li>
<li>Make complete games.</li>
</ul>

<p>And, as a bonus, all the code is written entirely in the latest version of
JavaScript: ES6/2015.</p>

<p>If you want to support this project, please buy a copy of this book,
and buy another copy for your mom!</p>
</article>