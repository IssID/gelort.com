<style type="text/css">

</style>
<div class=titlebar> Learning Pixi </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
	echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
	<div class=contentInBox>


<a href="?LP_Content">Содержание </a>
<br>

<p><a id="user-content-rotation"></a></p>

<h2><a id="user-content-rotation" class="anchor" href="#rotation" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Rotation</h2>

<p>You can make a sprite rotate by setting its <code>rotation</code> property to a
value in <a href="http://www.mathsisfun.com/geometry/radians.html">radians</a>.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">cat</span>.<span class="pl-smi">rotation</span> <span class="pl-k">=</span> <span class="pl-c1">0.5</span>;</pre></div>

<p>But around which point does that rotation happen?</p>

<p>You've seen that a sprite's top left corner represents its <code>x</code> and <code>y</code> position. That point is
called the <strong>anchor point</strong>. If you set the sprite’s <code>rotation</code>
property to something like <code>0.5</code>, the rotation will happen <em>around the
sprite’s anchor point</em>.
This diagram shows what effect this will have on our cat sprite.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s07.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/07.png" alt="Rotation around anchor point - diagram" style="max-width:100%;"></a></p>

<p>You can see that the anchor point, the cat’s left ear, is the center of the imaginary circle around which the cat is rotating.
What if you want the sprite to rotate around its center? Change the
sprite’s <code>anchor</code> point so that it’s centered inside the sprite, like
this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">cat</span>.<span class="pl-smi">anchor</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">0.5</span>;
<span class="pl-smi">cat</span>.<span class="pl-smi">anchor</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">0.5</span>;</pre></div>

<p>The <code>anchor.x</code> and <code>anchor.y</code> values represent a percentage of the texture’s dimensions, from 0 to 1 (0%
to 100%). Setting it to 0.5 centers the texture over the point. The location of the point
itself won’t change, just the way the texture is positioned over it.</p>

<p>This next diagram shows what happens to the rotated sprite if you center its anchor point.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s08.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/08.png" alt="Rotation around centered anchor point - diagram" style="max-width:100%;"></a></p>

<p>You can see that the sprite’s texture shifts up and to the left. This
is an important side-effect to remember!</p>

<p>Just like with <code>position</code> and <code>scale</code>, you can set the anchor’s x and
y values with one line of code like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">sprite</span>.<span class="pl-smi">anchor</span>.<span class="pl-c1">set</span>(x, y)</pre></div>

<p>Sprites also have a <code>pivot</code> property which works in a similar way to
<code>anchor</code>. <code>pivot</code> sets the position
of the sprite's x/y origin point. If you change the pivot point and
then rotate the sprite, it will
rotate around that origin point. For example, the following code will
set the sprite's <code>pivot.x</code> point to 32, and its <code>pivot.y</code> point to 32</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">cat</span>.<span class="pl-smi">pivot</span>.<span class="pl-c1">set</span>(<span class="pl-c1">32</span>, <span class="pl-c1">32</span>)</pre></div>

<p>Assuming that the sprite is 64x64 pixels, the sprite will now rotate
around its center point. But remember: if you change a sprite's pivot
point, you've also changed its x/y origin point. </p>

<p>So, what's the difference between <code>anchor</code> and <code>pivot</code>? They're really
similar! <code>anchor</code> shifts the origin point of the sprite's image texture, using a 0 to 1 normalized value.
<code>pivot</code> shifts the origin of the sprite's x and y point, using pixel
values. Which should you use? It's up to you. Just play around
with both of them and see which you prefer.</p>