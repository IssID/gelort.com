<style type="text/css">

</style>
<div class=titlebar> Learning Pixi </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
  echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
  <div class=contentInBox>


<a href="?LP_Content">Содержание </a>
<br>

<p><a id="user-content-casestudy"></a></p>

<h2><a id="user-content-case-study-treasure-hunter" class="anchor" href="#case-study-treasure-hunter" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Case study: Treasure Hunter</h2>

<p>So I told you that you now have all the skills you need to start
making games. What? You don't believe me? Let me prove it to you! Let’s take a
close at how to make a simple object collection and enemy
avoidance game called <strong>Treasure Hunter</strong>. (You'll find it the <code>examples</code>
folder.)</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s26.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/26.png" alt="Treasure Hunter" style="max-width:100%;"></a></p>

<p>Treasure Hunter is a good example of one of simplest complete
games you can make using the tools you've learnt so far. Use the
keyboard arrow
keys to help the explorer find the treasure and carry it to the exit.
Six blob monsters move up and down between the dungeon walls, and if
they hit the explorer he becomes semi-transparent and the health meter
at the top right corner shrinks. If all the health is used up, “You
Lost!” is displayed on the stage; if the explorer reaches the exit with
the treasure, “You Won!” is displayed. Although it’s a basic
prototype, Treasure Hunter contains most of the elements you’ll find
in much bigger games: texture atlas graphics, interactivity,
collision, and multiple game scenes. Let’s go on a tour of how the
game was put together so that you can use it as a starting point for one of your own games.</p>

<h3><a id="user-content-the-code-structure" class="anchor" href="#the-code-structure" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>The code structure</h3>

<p>Open the <code>treasureHunter.html</code> file and you'll see that all the game
code is in one big file. Here's a birds-eye view of how all the code is
organized.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c">//Setup Pixi and load the texture atlas files - call the `setup`</span>
<span class="pl-c">//function when they've loaded</span>

<span class="pl-k">function</span> <span class="pl-en">setup</span>() {
  <span class="pl-c">//Initialize the game sprites, set the game `state` to `play`</span>
  <span class="pl-c">//and start the 'gameLoop'</span>
}

<span class="pl-k">function</span> <span class="pl-en">gameLoop</span>() {
  <span class="pl-c">//Runs the current game `state` in a loop and renders the sprites</span>
}

<span class="pl-k">function</span> <span class="pl-en">play</span>() {
  <span class="pl-c">//All the game logic goes here</span>
}

<span class="pl-k">function</span> <span class="pl-en">end</span>() {
  <span class="pl-c">//All the code that should run at the end of the game</span>
}

<span class="pl-c">//The game's helper functions:</span>
<span class="pl-c">//`keyboard`, `hitTestRectangle`, `contain` and `randomInt`</span></pre></div>

<p>Use this as your world map to the game as we look at how each
section works.</p>

<p><a id="user-content-initialize"></a></p>

<h3><a id="user-content-initialize-the-game-in-the-setup-function" class="anchor" href="#initialize-the-game-in-the-setup-function" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Initialize the game in the setup function</h3>

<p>As soon as the texture atlas images have loaded, the <code>setup</code> function
runs. It only runs once, and lets you perform
one-time setup tasks for your game. It's a great place to create and initialize
objects, sprites, game scenes, populate data arrays or parse
loaded JSON game data.</p>

<p>Here's an abridged view of the <code>setup</code> function in Treasure Hunter,
and the tasks that it performs.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">setup</span>() {
  <span class="pl-c">//Create the `gameScene` group</span>
  <span class="pl-c">//Create the `door` sprite</span>
  <span class="pl-c">//Create the `player` sprite</span>
  <span class="pl-c">//Create the `treasure` sprite</span>
  <span class="pl-c">//Make the enemies</span>
  <span class="pl-c">//Create the health bar</span>
  <span class="pl-c">//Add some text for the game over message</span>
  <span class="pl-c">//Create a `gameOverScene` group</span>
  <span class="pl-c">//Assign the player's keyboard controllers</span>

  <span class="pl-c">//set the game state to `play`</span>
  state <span class="pl-k">=</span> play;

  <span class="pl-c">//Start the game loop</span>
  <span class="pl-en">gameLoop</span>();
}
</pre></div>

<p>The last two lines of code, <code>state = play;</code> and <code>gameLoop()</code> are perhaps
the most important. Running <code>gameLoop</code> switches on the game's engine,
and causes the <code>play</code> function to be called in a continuous loop. But before we look at how that works, let's see what the
specific code inside the <code>setup</code> function does.</p>

<p><a id="user-content-gamescene"></a></p>

<h4><a id="user-content-creating-the-game-scenes" class="anchor" href="#creating-the-game-scenes" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Creating the game scenes</h4>

<p>The <code>setup</code> function creates two <code>Container</code> groups called
<code>gameScene</code> and <code>gameOverScene</code>. Each of these are added to the stage.</p>

<div class="highlight highlight-source-js"><pre>gameScene <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Container</span>();
<span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(gameScene);

gameOverScene <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Container</span>();
<span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(gameOverScene);
</pre></div>

<p>All of the sprites that are part of the main game are added to the
<code>gameScene</code> group. The game over text that should be displayed at the
end of the game is added to the <code>gameOverScene</code> group.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s27.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/27.png" alt="Displaying text" style="max-width:100%;"></a></p>

<p>Although it's created in the <code>setup</code> function, the <code>gameOverScene</code>
shouldn't be visible when the game first starts, so its <code>visible</code>
property is initialized to <code>false</code>.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">gameOverScene</span>.<span class="pl-smi">visible</span> <span class="pl-k">=</span> <span class="pl-c1">false</span>;</pre></div>

<p>You'll see ahead that, when the game ends, the <code>gameOverScene</code>'s <code>visible</code>
property will be set to <code>true</code> to display the text that appears at the
end of the game.</p>

<p><a id="user-content-makingdungon"></a></p>

<h4><a id="user-content-making-the-dungeon-door-explorer-and-treasure" class="anchor" href="#making-the-dungeon-door-explorer-and-treasure" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Making the dungeon, door, explorer and treasure</h4>

<p>The player, exit door, treasure chest and the dungeon background image
are all sprites made from texture atlas frames. Very importantly,
they're all added as children of the <code>gameScene</code>.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c">//Create an alias for the texture atlas frame ids</span>
id <span class="pl-k">=</span> resources[<span class="pl-s"><span class="pl-pds">"</span>images/treasureHunter.json<span class="pl-pds">"</span></span>].<span class="pl-smi">textures</span>;

<span class="pl-c">//Dungeon</span>
dungeon <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(id[<span class="pl-s"><span class="pl-pds">"</span>dungeon.png<span class="pl-pds">"</span></span>]);
<span class="pl-smi">gameScene</span>.<span class="pl-en">addChild</span>(dungeon);

<span class="pl-c">//Door</span>
door <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(id[<span class="pl-s"><span class="pl-pds">"</span>door.png<span class="pl-pds">"</span></span>]);
<span class="pl-smi">door</span>.<span class="pl-smi">position</span>.<span class="pl-c1">set</span>(<span class="pl-c1">32</span>, <span class="pl-c1">0</span>);
<span class="pl-smi">gameScene</span>.<span class="pl-en">addChild</span>(door);

<span class="pl-c">//Explorer</span>
explorer <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(id[<span class="pl-s"><span class="pl-pds">"</span>explorer.png<span class="pl-pds">"</span></span>]);
<span class="pl-smi">explorer</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">68</span>;
<span class="pl-smi">explorer</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-smi">gameScene</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span> <span class="pl-k">-</span> <span class="pl-smi">explorer</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;
<span class="pl-smi">explorer</span>.<span class="pl-smi">vx</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
<span class="pl-smi">explorer</span>.<span class="pl-smi">vy</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
<span class="pl-smi">gameScene</span>.<span class="pl-en">addChild</span>(explorer);

<span class="pl-c">//Treasure</span>
treasure <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(id[<span class="pl-s"><span class="pl-pds">"</span>treasure.png<span class="pl-pds">"</span></span>]);
<span class="pl-smi">treasure</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-smi">gameScene</span>.<span class="pl-c1">width</span> <span class="pl-k">-</span> <span class="pl-smi">treasure</span>.<span class="pl-c1">width</span> <span class="pl-k">-</span> <span class="pl-c1">48</span>;
<span class="pl-smi">treasure</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-smi">gameScene</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span> <span class="pl-k">-</span> <span class="pl-smi">treasure</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>;
<span class="pl-smi">gameScene</span>.<span class="pl-en">addChild</span>(treasure);</pre></div>

<p>Keeping them together in the <code>gameScene</code> group will make it easy for
us to hide the <code>gameScene</code> and display the <code>gameOverScene</code> when the game is finished.</p>

<p><a id="user-content-makingblob"></a></p>

<h4><a id="user-content-making-the-blob-monsters" class="anchor" href="#making-the-blob-monsters" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Making the blob monsters</h4>

<p>The six blob monsters are created in a loop. Each blob is given a
random initial position and velocity. The vertical velocity is
alternately multiplied by <code>1</code> or <code>-1</code> for each blob, and that’s what
causes each blob to move in the opposite direction to the one next to
it. Each blob monster that's created is pushed into an array called
<code>blobs</code>.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> numberOfBlobs <span class="pl-k">=</span> <span class="pl-c1">6</span>,
    spacing <span class="pl-k">=</span> <span class="pl-c1">48</span>,
    xOffset <span class="pl-k">=</span> <span class="pl-c1">150</span>,
    speed <span class="pl-k">=</span> <span class="pl-c1">2</span>,
    direction <span class="pl-k">=</span> <span class="pl-c1">1</span>;

<span class="pl-c">//An array to store all the blob monsters</span>
blobs <span class="pl-k">=</span> [];

<span class="pl-c">//Make as many blobs as there are `numberOfBlobs`</span>
<span class="pl-k">for</span> (<span class="pl-k">var</span> i <span class="pl-k">=</span> <span class="pl-c1">0</span>; i <span class="pl-k">&lt;</span> numberOfBlobs; i<span class="pl-k">++</span>) {

  <span class="pl-c">//Make a blob</span>
  <span class="pl-k">var</span> blob <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(id[<span class="pl-s"><span class="pl-pds">"</span>blob.png<span class="pl-pds">"</span></span>]);

  <span class="pl-c">//Space each blob horizontally according to the `spacing` value.</span>
  <span class="pl-c">//`xOffset` determines the point from the left of the screen</span>
  <span class="pl-c">//at which the first blob should be added</span>
  <span class="pl-k">var</span> x <span class="pl-k">=</span> spacing <span class="pl-k">*</span> i <span class="pl-k">+</span> xOffset;

  <span class="pl-c">//Give the blob a random `y` position</span>
  <span class="pl-k">var</span> y <span class="pl-k">=</span> <span class="pl-en">randomInt</span>(<span class="pl-c1">0</span>, <span class="pl-smi">stage</span>.<span class="pl-c1">height</span> <span class="pl-k">-</span> <span class="pl-smi">blob</span>.<span class="pl-c1">height</span>);

  <span class="pl-c">//Set the blob's position</span>
  <span class="pl-smi">blob</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> x;
  <span class="pl-smi">blob</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> y;

  <span class="pl-c">//Set the blob's vertical velocity. `direction` will be either `1` or</span>
  <span class="pl-c">//`-1`. `1` means the enemy will move down and `-1` means the blob will</span>
  <span class="pl-c">//move up. Multiplying `direction` by `speed` determines the blob's</span>
  <span class="pl-c">//vertical direction</span>
  <span class="pl-smi">blob</span>.<span class="pl-smi">vy</span> <span class="pl-k">=</span> speed <span class="pl-k">*</span> direction;

  <span class="pl-c">//Reverse the direction for the next blob</span>
  direction <span class="pl-k">*=</span> <span class="pl-k">-</span><span class="pl-c1">1</span>;

  <span class="pl-c">//Push the blob into the `blobs` array</span>
  <span class="pl-smi">blobs</span>.<span class="pl-c1">push</span>(blob);

  <span class="pl-c">//Add the blob to the `gameScene`</span>
  <span class="pl-smi">gameScene</span>.<span class="pl-en">addChild</span>(blob);
}
</pre></div>

<p><a id="user-content-healthbar"></a></p>

<h4><a id="user-content-making-the-health-bar" class="anchor" href="#making-the-health-bar" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Making the health bar</h4>

<p>When you play Treasure Hunter you'll notice that when the explorer touches
one of the enemies, the width of the health bar at the top right
corner of the screen decreases. How was this health bar made? It's
just two overlapping rectangles at exactly the same position: a black rectangle behind, and
a red rectangle in front. They're grouped into a single <code>healthBar</code>
group. The <code>healthBar</code> is then added to the <code>gameScene</code> and positioned
on the stage.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c">//Create the health bar</span>
healthBar <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.DisplayObjectContainer</span>();
<span class="pl-smi">healthBar</span>.<span class="pl-smi">position</span>.<span class="pl-c1">set</span>(<span class="pl-smi">stage</span>.<span class="pl-c1">width</span> <span class="pl-k">-</span> <span class="pl-c1">170</span>, <span class="pl-c1">6</span>)
<span class="pl-smi">gameScene</span>.<span class="pl-en">addChild</span>(healthBar);

<span class="pl-c">//Create the black background rectangle</span>
<span class="pl-k">var</span> innerBar <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.Graphics</span>();
<span class="pl-smi">innerBar</span>.<span class="pl-en">beginFill</span>(<span class="pl-c1">0x000000</span>);
<span class="pl-smi">innerBar</span>.<span class="pl-en">drawRect</span>(<span class="pl-c1">0</span>, <span class="pl-c1">0</span>, <span class="pl-c1">128</span>, <span class="pl-c1">8</span>);
<span class="pl-smi">innerBar</span>.<span class="pl-en">endFill</span>();
<span class="pl-smi">healthBar</span>.<span class="pl-en">addChild</span>(innerBar);

<span class="pl-c">//Create the front red rectangle</span>
<span class="pl-k">var</span> outerBar <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.Graphics</span>();
<span class="pl-smi">outerBar</span>.<span class="pl-en">beginFill</span>(<span class="pl-c1">0xFF3300</span>);
<span class="pl-smi">outerBar</span>.<span class="pl-en">drawRect</span>(<span class="pl-c1">0</span>, <span class="pl-c1">0</span>, <span class="pl-c1">128</span>, <span class="pl-c1">8</span>);
<span class="pl-smi">outerBar</span>.<span class="pl-en">endFill</span>();
<span class="pl-smi">healthBar</span>.<span class="pl-en">addChild</span>(outerBar);

<span class="pl-smi">healthBar</span>.<span class="pl-smi">outer</span> <span class="pl-k">=</span> outerBar;</pre></div>

<p>You can see that a property called <code>outer</code> has been added to the
<code>healthBar</code>. It just references the <code>outerBar</code> (the red rectangle) so that it will be convenient to access later.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">healthBar</span>.<span class="pl-smi">outer</span> <span class="pl-k">=</span> outerBar;</pre></div>

<p>You don't have to do this; but, hey why not! It means that if you want
to control the width of the red <code>outerBar</code>, you can write some smooth code that looks like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">healthBar</span>.<span class="pl-smi">outer</span>.<span class="pl-c1">width</span> <span class="pl-k">=</span> <span class="pl-c1">30</span>;</pre></div>

<p>That's pretty neat and readable, so we'll keep it!</p>

<p><a id="user-content-message"></a></p>

<h4><a id="user-content-making-the-message-text" class="anchor" href="#making-the-message-text" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Making the message text</h4>

<p>When the game is finished, some text displays “You won!” or “You
lost!”, depending on the outcome of the game. This is made using a
text sprite and adding it to the <code>gameOverScene</code>. Because the
<code>gameOverScene</code>‘s <code>visible</code> property is set to <code>false</code> when the game
starts, you can’t see this text. Here’s the code from the <code>setup</code>
function that creates the message text and adds it to the
<code>gameOverScene</code>.</p>

<div class="highlight highlight-source-js"><pre>message <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Text</span>(
  <span class="pl-s"><span class="pl-pds">"</span>The End!<span class="pl-pds">"</span></span>,
  {font<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">"</span>64px Futura<span class="pl-pds">"</span></span>, fill<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">"</span>white<span class="pl-pds">"</span></span>}
);

<span class="pl-smi">message</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">120</span>;
<span class="pl-smi">message</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-smi">stage</span>.<span class="pl-c1">height</span> <span class="pl-k">/</span> <span class="pl-c1">2</span> <span class="pl-k">-</span> <span class="pl-c1">32</span>;

<span class="pl-smi">gameOverScene</span>.<span class="pl-en">addChild</span>(message);</pre></div>

<p><a id="user-content-playing"></a></p>

<h3><a id="user-content-playing-the-game" class="anchor" href="#playing-the-game" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Playing the game</h3>

<p>All the game logic and the code that makes the sprites move happens
inside the <code>play</code> function, which runs in a continuous loop. Here's an
overview of what the <code>play</code> function does</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">play</span>() {
  <span class="pl-c">//Move the explorer and contain it inside the dungeon</span>
  <span class="pl-c">//Move the blob monsters</span>
  <span class="pl-c">//Check for a collision between the blobs and the explorer</span>
  <span class="pl-c">//Check for a collision between the explorer and the treasure</span>
  <span class="pl-c">//Check for a collsion between the treasure and the door</span>
  <span class="pl-c">//Decide whether the game has been won or lost</span>
  <span class="pl-c">//Change the game `state` to `end` when the game is finsihed</span>
}</pre></div>

<p>Let's find out how all these features work.</p>

<p><a id="user-content-movingexplorer"></a></p>

<h3><a id="user-content-moving-the-explorer" class="anchor" href="#moving-the-explorer" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Moving the explorer</h3>

<p>The explorer is controlled using the keyboard, and the code that does
that is very similar to the keyboard control code you learnt earlier.
The <code>keyboard</code> objects modify the explorer’s velocity, and that
velocity is added to the explorer’s position inside the <code>play</code>
function.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">explorer</span>.<span class="pl-c1">x</span> <span class="pl-k">+=</span> <span class="pl-smi">explorer</span>.<span class="pl-smi">vx</span>;
<span class="pl-smi">explorer</span>.<span class="pl-c1">y</span> <span class="pl-k">+=</span> <span class="pl-smi">explorer</span>.<span class="pl-smi">vy</span>;</pre></div>

<p><a id="user-content-containingmovement"></a></p>

<h4><a id="user-content-containing-movement" class="anchor" href="#containing-movement" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Containing movement</h4>

<p>But what's new is that the explorer's movement is contained inside the walls of the
dungeon. The green outline shows the limits of the explorer's
movement.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s28.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/28.png" alt="Displaying text" style="max-width:100%;"></a></p>

<p>That's done with the help of a custom function called
<code>contain</code>.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-en">contain</span>(explorer, {x<span class="pl-k">:</span> <span class="pl-c1">28</span>, y<span class="pl-k">:</span> <span class="pl-c1">10</span>, width<span class="pl-k">:</span> <span class="pl-c1">488</span>, height<span class="pl-k">:</span> <span class="pl-c1">480</span>});</pre></div>

<p><code>contain</code> takes two arguments. The first is the sprite you want to keep
contained. The second is any object with <code>x</code>, <code>y</code>, <code>width</code> and
<code>height</code> properties that define a rectangular area. In this example,
the containing object defines an area that's just slightly offset
from, and smaller than, the stage. It matches dimensions of the dungeon
walls.</p>

<p>Here's the <code>contain</code> function that does all this work. The function checks
to see if the sprite has crossed the boundaries of the containing
object. If it has, the code moves the sprite back into that boundary.
The <code>contain</code> function also returns a <code>collision</code> variable with the
value "top", "right", "bottom" or "left", depending on which side of
the boundary the sprite hit. (<code>collision</code> will be <code>undefined</code> if the
sprite didn't hit any of the boundaries.)</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">contain</span>(<span class="pl-smi">sprite</span>, <span class="pl-smi">container</span>) {

  <span class="pl-k">var</span> collision <span class="pl-k">=</span> <span class="pl-c1">undefined</span>;

  <span class="pl-c">//Left</span>
  <span class="pl-k">if</span> (<span class="pl-smi">sprite</span>.<span class="pl-c1">x</span> <span class="pl-k">&lt;</span> <span class="pl-smi">container</span>.<span class="pl-c1">x</span>) {
    <span class="pl-smi">sprite</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-smi">container</span>.<span class="pl-c1">x</span>;
    collision <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">"</span>left<span class="pl-pds">"</span></span>;
  }

  <span class="pl-c">//Top</span>
  <span class="pl-k">if</span> (<span class="pl-smi">sprite</span>.<span class="pl-c1">y</span> <span class="pl-k">&lt;</span> <span class="pl-smi">container</span>.<span class="pl-c1">y</span>) {
    <span class="pl-smi">sprite</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-smi">container</span>.<span class="pl-c1">y</span>;
    collision <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">"</span>top<span class="pl-pds">"</span></span>;
  }

  <span class="pl-c">//Right</span>
  <span class="pl-k">if</span> (<span class="pl-smi">sprite</span>.<span class="pl-c1">x</span> <span class="pl-k">+</span> <span class="pl-smi">sprite</span>.<span class="pl-c1">width</span> <span class="pl-k">&gt;</span> <span class="pl-smi">container</span>.<span class="pl-c1">width</span>) {
    <span class="pl-smi">sprite</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-smi">container</span>.<span class="pl-c1">width</span> <span class="pl-k">-</span> <span class="pl-smi">sprite</span>.<span class="pl-c1">width</span>;
    collision <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">"</span>right<span class="pl-pds">"</span></span>;
  }

  <span class="pl-c">//Bottom</span>
  <span class="pl-k">if</span> (<span class="pl-smi">sprite</span>.<span class="pl-c1">y</span> <span class="pl-k">+</span> <span class="pl-smi">sprite</span>.<span class="pl-c1">height</span> <span class="pl-k">&gt;</span> <span class="pl-smi">container</span>.<span class="pl-c1">height</span>) {
    <span class="pl-smi">sprite</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-smi">container</span>.<span class="pl-c1">height</span> <span class="pl-k">-</span> <span class="pl-smi">sprite</span>.<span class="pl-c1">height</span>;
    collision <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">"</span>bottom<span class="pl-pds">"</span></span>;
  }

  <span class="pl-c">//Return the `collision` value</span>
  <span class="pl-k">return</span> collision;
}</pre></div>

<p>You'll see how the <code>collision</code> return value will be use in the code
ahead to make the blob monsters bounce back and forth between the top
and bottom dungeon walls.</p>

<p><a id="user-content-movingmonsters"></a></p>

<h3><a id="user-content-moving-the-monsters" class="anchor" href="#moving-the-monsters" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Moving the monsters</h3>

<p>The <code>play</code> function also moves the blob monsters, keeps them contained
inside the dungeon walls, and checks each one for a collision with the
player. If a blob bumps into the dungeon’s top or bottom walls, its
direction is reversed. All this is done with the help of a <code>forEach</code> loop
which iterates through each of <code>blob</code> sprites in the <code>blobs</code> array on
every frame.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">blobs</span>.<span class="pl-c1">forEach</span>(<span class="pl-k">function</span>(<span class="pl-smi">blob</span>) {

  <span class="pl-c">//Move the blob</span>
  <span class="pl-smi">blob</span>.<span class="pl-c1">y</span> <span class="pl-k">+=</span> <span class="pl-smi">blob</span>.<span class="pl-smi">vy</span>;

  <span class="pl-c">//Check the blob's screen boundaries</span>
  <span class="pl-k">var</span> blobHitsWall <span class="pl-k">=</span> <span class="pl-en">contain</span>(blob, {x<span class="pl-k">:</span> <span class="pl-c1">28</span>, y<span class="pl-k">:</span> <span class="pl-c1">10</span>, width<span class="pl-k">:</span> <span class="pl-c1">488</span>, height<span class="pl-k">:</span> <span class="pl-c1">480</span>});

  <span class="pl-c">//If the blob hits the top or bottom of the stage, reverse</span>
  <span class="pl-c">//its direction</span>
  <span class="pl-k">if</span> (blobHitsWall <span class="pl-k">===</span> <span class="pl-s"><span class="pl-pds">"</span>top<span class="pl-pds">"</span></span> <span class="pl-k">||</span> blobHitsWall <span class="pl-k">===</span> <span class="pl-s"><span class="pl-pds">"</span>bottom<span class="pl-pds">"</span></span>) {
    <span class="pl-smi">blob</span>.<span class="pl-smi">vy</span> <span class="pl-k">*=</span> <span class="pl-k">-</span><span class="pl-c1">1</span>;
  }

  <span class="pl-c">//Test for a collision. If any of the enemies are touching</span>
  <span class="pl-c">//the explorer, set `explorerHit` to `true`</span>
  <span class="pl-k">if</span>(<span class="pl-en">hitTestRectangle</span>(explorer, blob)) {
    explorerHit <span class="pl-k">=</span> <span class="pl-c1">true</span>;
  }
});
</pre></div>

<p>You can see in this code above how the return value of the <code>contain</code>
function is used to make the blobs bounce off the walls. A variable
called <code>blobHitsWall</code> is used to capture the return value:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> blobHitsWall <span class="pl-k">=</span> <span class="pl-en">contain</span>(blob, {x<span class="pl-k">:</span> <span class="pl-c1">28</span>, y<span class="pl-k">:</span> <span class="pl-c1">10</span>, width<span class="pl-k">:</span> <span class="pl-c1">488</span>, height<span class="pl-k">:</span> <span class="pl-c1">480</span>});</pre></div>

<p><code>blobHitsWall</code> will usually be <code>undefined</code>. But if the blob hits the
top wall, <code>blobHitsWall</code> will have the value "top". If the blob hits
the bottom wall, <code>blobHitsWall</code> will have the value "bottom". If
either of these cases are <code>true</code>, you can reverse the blob's direction
by reversing its velocity. Here's the code that does this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">if</span> (blobHitsWall <span class="pl-k">===</span> <span class="pl-s"><span class="pl-pds">"</span>top<span class="pl-pds">"</span></span> <span class="pl-k">||</span> blobHitsWall <span class="pl-k">===</span> <span class="pl-s"><span class="pl-pds">"</span>bottom<span class="pl-pds">"</span></span>) {
  <span class="pl-smi">blob</span>.<span class="pl-smi">vy</span> <span class="pl-k">*=</span> <span class="pl-k">-</span><span class="pl-c1">1</span>;
}</pre></div>

<p>Multiplying the blob's <code>vy</code> (vertical velocity) value by <code>-1</code> will flip
the direction of its movement.</p>

<p><a id="user-content-checkingcollisions"></a></p>

<h3><a id="user-content-checking-for-collisions" class="anchor" href="#checking-for-collisions" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Checking for collisions</h3>

<p>The code in the loop above uses <code>hitTestRectangle</code> to figure
out if any of the enemies have touched the explorer.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">if</span>(<span class="pl-en">hitTestRectangle</span>(explorer, blob)) {
  explorerHit <span class="pl-k">=</span> <span class="pl-c1">true</span>;
}</pre></div>

<p>If <code>hitTestRectangle</code> returns <code>true</code>, it means there’s been a collision
and a variable called <code>explorerHit</code> is set to <code>true</code>. If <code>explorerHit</code>
is <code>true</code>, the <code>play</code> function makes the explorer semi-transparent
and reduces the width of the <code>health</code> bar by 1 pixel.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">if</span>(explorerHit) {

  <span class="pl-c">//Make the explorer semi-transparent</span>
  <span class="pl-smi">explorer</span>.<span class="pl-smi">alpha</span> <span class="pl-k">=</span> <span class="pl-c1">0.5</span>;

  <span class="pl-c">//Reduce the width of the health bar's inner rectangle by 1 pixel</span>
  <span class="pl-smi">healthBar</span>.<span class="pl-smi">outer</span>.<span class="pl-c1">width</span> <span class="pl-k">-=</span> <span class="pl-c1">1</span>;

} <span class="pl-k">else</span> {

  <span class="pl-c">//Make the explorer fully opaque (non-transparent) if it hasn't been hit</span>
  <span class="pl-smi">explorer</span>.<span class="pl-smi">alpha</span> <span class="pl-k">=</span> <span class="pl-c1">1</span>;
}
</pre></div>

<p>If  <code>explorerHit</code> is <code>false</code>, the explorer's <code>alpha</code> property is
maintained at 1, which makes it fully opaque.</p>

<p>The <code>play</code> function also checks for a collision between the treasure
chest and the explorer. If there’s a hit, the <code>treasure</code> is set to the
explorer’s position, with a slight offset. This makes it look like the
explorer is carrying the treasure.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s29.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/29.png" alt="Displaying text" style="max-width:100%;"></a></p>

<p>Here's the code that does this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">if</span> (<span class="pl-en">hitTestRectangle</span>(explorer, treasure)) {
  <span class="pl-smi">treasure</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-smi">explorer</span>.<span class="pl-c1">x</span> <span class="pl-k">+</span> <span class="pl-c1">8</span>;
  <span class="pl-smi">treasure</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-smi">explorer</span>.<span class="pl-c1">y</span> <span class="pl-k">+</span> <span class="pl-c1">8</span>;
}</pre></div>

<p><a id="user-content-reachingexit"></a></p>

<h3><a id="user-content-reaching-the-exit-door-and-ending-the-game" class="anchor" href="#reaching-the-exit-door-and-ending-the-game" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Reaching the exit door and ending the game</h3>

<p>There are two ways the game can end: You can win if you carry the
treasure to the exit, or you can lose if you run out of health.</p>

<p>To win the game, the treasure chest just needs to touch the exit door. If
that happens, the game <code>state</code> is set to <code>end</code>, and the <code>message</code> text
displays "You won".</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">if</span> (<span class="pl-en">hitTestRectangle</span>(treasure, door)) {
  state <span class="pl-k">=</span> end;
  <span class="pl-smi">message</span>.<span class="pl-c1">text</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">"</span>You won!<span class="pl-pds">"</span></span>;
}</pre></div>

<p>If you run out of health, you lose the game. The game <code>state</code> is also
set to <code>end</code> and the <code>message</code> text displays "You Lost!"</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">if</span> (<span class="pl-smi">healthBar</span>.<span class="pl-smi">outer</span>.<span class="pl-c1">width</span> <span class="pl-k">&lt;</span> <span class="pl-c1">0</span>) {
  state <span class="pl-k">=</span> end;
  <span class="pl-smi">message</span>.<span class="pl-c1">text</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">"</span>You lost!<span class="pl-pds">"</span></span>;
}</pre></div>

<p>But what does this mean?</p>

<div class="highlight highlight-source-js"><pre>state <span class="pl-k">=</span> end;</pre></div>

<p>You'll remember from earlier examples that the <code>gameLoop</code> is constantly updating a function called
<code>state</code> at 60 times per second. Here's the <code>gameLoop</code>that does this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">gameLoop</span>(){

  <span class="pl-c">//Loop this function 60 times per second</span>
  <span class="pl-en">requestAnimationFrame</span>(gameLoop);

  <span class="pl-c">//Update the current game state</span>
  <span class="pl-en">state</span>();

  <span class="pl-c">//Render the stage</span>
  <span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);
}</pre></div>

<p>You'll also remember that we initially set the value of
<code>state</code> to <code>play</code>, which is why the <code>play</code> function runs in a loop.
By setting <code>state</code> to <code>end</code> we're telling the code that we want
another function, called <code>end</code> to run in a loop. In a bigger game you
could have a <code>tileScene</code> state, and states for each game level, like
<code>leveOne</code>, <code>levelTwo</code> and <code>levelThree</code>.</p>

<p>So what is that <code>end</code> function? Here it is!</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">end</span>() {
  <span class="pl-smi">gameScene</span>.<span class="pl-smi">visible</span> <span class="pl-k">=</span> <span class="pl-c1">false</span>;
  <span class="pl-smi">gameOverScene</span>.<span class="pl-smi">visible</span> <span class="pl-k">=</span> <span class="pl-c1">true</span>;
}</pre></div>

<p>It just flips the visibility of the game scenes. This is what hides
the <code>gameScene</code> and displays the <code>gameOverScene</code> when the game ends.</p>

<p>This is a really simple example of how to switch a game's state, but
you can have as many game states as you like in your games, and fill them
with as much code as you need. Just change the value of <code>state</code> to
whatever function you want to run in a loop.</p>

<p>And that’s really all there is to Treasure Hunter! With a little more work you could turn this simple prototype into a full game – try it!</p>
