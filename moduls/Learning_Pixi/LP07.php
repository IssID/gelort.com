<style type="text/css">

</style>
<div class=titlebar> Learning Pixi </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
  echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
  <div class=contentInBox>


<a href="?LP_Content">Содержание </a>
<br>

<p><a id="user-content-positioning"></a></p> 

<h2><a id="user-content-positioning-sprites" class="anchor" href="#positioning-sprites" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Positioning sprites</h2>

<p>Now that you know how to create and display sprites, let's find out
how to position and resize them.</p>

<p>In the earlier example the cat sprite was added to stage at
the top left corner. The cat has an <code>x</code> position of
0 and a <code>y</code> position of 0. You can change the position of the cat by
changing the values of its <code>x</code> and <code>y</code> properties. Here's how you can center the cat in the stage by
setting its <code>x</code> and <code>y</code> property values to 96.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">cat</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">96</span>;
<span class="pl-smi">cat</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">96</span>;</pre></div>

<p>Add these two lines of code anywhere inside the <code>setup</code>
function, after you've created the sprite. </p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">setup</span>() {

  <span class="pl-c">//Create the `cat` sprite</span>
  <span class="pl-k">var</span> cat <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(resources[<span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>].<span class="pl-smi">texture</span>);

  <span class="pl-c">//Change the sprite's position</span>
  <span class="pl-smi">cat</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">96</span>;
  <span class="pl-smi">cat</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">96</span>;

  <span class="pl-c">//Add the cat to the stage so you can see it</span>
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(cat);

  <span class="pl-c">//Render the stage</span>
  <span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);
}</pre></div>

<p>(Note: In this example,
<code>Sprite</code> is an alias for <code>PIXI.Sprite</code>, <code>TextureCache</code> is an
alias for <code>PIXI.utils.TextureCache</code>, and <code>resources</code> is an alias for
<code>PIXI.loader.resources</code> as described earlier. I'll be
using alias that follow this same format for all Pixi objects and
methods in the example code from now on.)</p>

<p>These two new lines of code will move the cat 96 pixels to the right,
and 96 pixels down. Here's the result:</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s03.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/03.png" alt="Cat centered on the stage" style="max-width:100%;"></a></p>

<p>The cat's top left corner (its left ear) represents its <code>x</code> and <code>y</code>
anchor point. To make the cat move to the right, increase the
value of its <code>x</code> property. To make the cat move down, increase the
value of its <code>y</code> property. If the cat has an <code>x</code> value of 0, it will be at
the very left side of the stage. If it has a <code>y</code> value of 0, it will
be at the very top of the stage.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s04.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/04.png" alt="Cat centered on the stage - diagram" style="max-width:100%;"></a></p>

<p>Instead of setting the sprite's <code>x</code> and <code>y</code> properties independently,
you can set them together in a single line of code, like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">sprite</span>.<span class="pl-smi">position</span>.<span class="pl-c1">set</span>(x, y)</pre></div>