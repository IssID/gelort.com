<style type="text/css">

</style>
<div class=titlebar> Learning Pixi </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
  echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
  <div class=contentInBox>


<a href="?LP_Content">Содержание </a>
<br>

<p><a id="user-content-tileset"></a></p>

<h2><a id="user-content-make-a-sprite-from-a-tileset-sub-image" class="anchor" href="#make-a-sprite-from-a-tileset-sub-image" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Make a sprite from a tileset sub-image</h2>

<p>You now know how to make a sprite from a single image file. But, as a
game designer, you’ll usually be making your sprites using
<strong>tilesets</strong> (also known as <strong>spritesheets</strong>.) Pixi has some convenient built-in ways to help you do this.
A tileset is a single image file that contains sub-images. The sub-images
represent all the graphics you want to use in your game. Here's an
example of a tileset image that contains game characters and game
objects as sub-images.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s09.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/09.png" alt="An example tileset" style="max-width:100%;"></a></p>

<p>The entire tileset is 192 by 192 pixels. Each image is in a its own 32 by 32
pixel grid cell. Storing and accessing all your game graphics on a
tileset is a very
processor and memory efficient way to work with graphics, and Pixi is
optimized for them.</p>

<p>You can capture a sub-image from a tileset by defining a rectangular
area
that's the same size and position as the sub-image you want to
extract. Here's an example of the rocket sub-image that’s been extracted from
the tileset.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s10.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/10.png" alt="Rocket extracted from tileset" style="max-width:100%;"></a></p>

<p>Let's look at the code that does this. First, load the <code>tileset.png</code> image
with Pixi’s <code>loader</code>, just as you've done in earlier examples.</p>

<div class="highlight highlight-source-js"><pre>loader
  .<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">"</span>images/tileset.png<span class="pl-pds">"</span></span>)
  .<span class="pl-c1">load</span>(setup);</pre></div>

<p>Next, when the image has loaded, use a rectangular sub-section of the tileset to create the
sprite’s image. Here's the code that extracts the sub image, creates
the rocket sprite, and positions and displays it on the canvas.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">setup</span>() {

  <span class="pl-c">//Create the `tileset` sprite from the texture</span>
  <span class="pl-k">var</span> texture <span class="pl-k">=</span> TextureCache[<span class="pl-s"><span class="pl-pds">"</span>images/tileset.png<span class="pl-pds">"</span></span>];

  <span class="pl-c">//Create a rectangle object that defines the position and</span>
  <span class="pl-c">//size of the sub-image you want to extract from the texture</span>
  <span class="pl-k">var</span> rectangle <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Rectangle</span>(<span class="pl-c1">192</span>, <span class="pl-c1">128</span>, <span class="pl-c1">64</span>, <span class="pl-c1">64</span>);

  <span class="pl-c">//Tell the texture to use that rectangular section</span>
  <span class="pl-smi">texture</span>.<span class="pl-c1">frame</span> <span class="pl-k">=</span> rectangle;

  <span class="pl-c">//Create the sprite from the texture</span>
  <span class="pl-k">var</span> rocket <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(texture);

  <span class="pl-c">//Position the rocket sprite on the canvas</span>
  <span class="pl-smi">rocket</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">32</span>;
  <span class="pl-smi">rocket</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">32</span>;

  <span class="pl-c">//Add the rocket to the stage</span>
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(rocket);

  <span class="pl-c">//Render the stage   </span>
  <span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);
}</pre></div>

<p>How does this work?</p>

<p>Pixi has a built-in <code>Rectangle</code> object (PIXI.Rectangle) that is a general-purpose
object for defining rectangular shapes. It takes four arguments. The
first two arguments define the rectangle's <code>x</code> and <code>y</code> position. The
last two define its <code>width</code> and <code>height</code>. Here's the format
for defining a new <code>Rectangle</code> object.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> rectangle <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.Rectangle</span>(x, y, width, height);</pre></div>

<p>The rectangle object is just a <em>data object</em>; it's up to you to decide how you want to use it. In
our example we're using it to define the position and area of the
sub-image on the tileset that we want to extract. Pixi textures have a useful
property called <code>frame</code> that can be set to any <code>Rectangle</code> objects. 
The <code>frame</code> crops the texture to the dimensions of the <code>Rectangle</code>.
Here's how to use <code>frame</code>
to crop the texture to the size and position of the rocket.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> rectangle <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Rectangle</span>(<span class="pl-c1">192</span>, <span class="pl-c1">128</span>, <span class="pl-c1">64</span>, <span class="pl-c1">64</span>);
<span class="pl-smi">texture</span>.<span class="pl-c1">frame</span> <span class="pl-k">=</span> rectangle;</pre></div>

<p>You can then use that cropped texture to create the sprite:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> rocket <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(texture);</pre></div>

<p>And that's how it works!</p>

<p>Because making sprite textures from a tileset
is something you’ll do with great frequency, Pixi has a more convenient way
to help you do this - let's find out what that is next.</p>