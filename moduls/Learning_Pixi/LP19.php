<style type="text/css">

</style>
<div class=titlebar> Learning Pixi </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
	echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
	<div class=contentInBox>


<a href="?LP_Content">Содержание </a>
<br>

<p><a id="user-content-graphic"></a></p>

<h2><a id="user-content-pixis-graphic-primitives" class="anchor" href="#pixis-graphic-primitives" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Pixi's Graphic Primitives</h2>

<p>Using image textures is one of the most useful ways of making sprites,
but Pixi also has its own low-level drawing tools. You can use them to
make rectangles, shapes, lines, complex polygons and text. And,
fortunately, it uses almost the same API as the <a href="https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Drawing_graphics_with_canvas">Canvas Drawing API</a> so,
if you're already familiar with canvas, here’s nothing really new to
  learn. But the big advantage is that, unlike the Canvas Drawing API,
  the shapes you draw with Pixi are rendered by WebGL on the GPU. Pixi
  lets you access all that untapped performance power.
Let’s take a quick tour of how to make some basic shapes. Here are all
the shapes we'll make in the code ahead.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s23.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/23.png" alt="Graphic primitives" style="max-width:100%;"></a></p>

<p><a id="user-content-rectangles"></a></p>

<h3><a id="user-content-rectangles" class="anchor" href="#rectangles" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Rectangles</h3>

<p>All shapes are made by first creating a new instance of Pixi's
<code>Graphics</code> class (<code>PIXI.Graphics</code>).</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> rectangle <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Graphics</span>();</pre></div>

<p>Use <code>beginFill</code> with a hexadecimal color code value to set the
rectangle’ s fill color. Here’ how to set to it to light blue.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">rectangle</span>.<span class="pl-en">beginFill</span>(<span class="pl-c1">0x66CCFF</span>);</pre></div>

<p>If you want to give the shape an outline, use the <code>lineStyle</code> method. Here's
how to give the rectangle a 4 pixel wide red outline, with an <code>alpha</code>
value of 1.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">rectangle</span>.<span class="pl-en">lineStyle</span>(<span class="pl-c1">4</span>, <span class="pl-c1">0xFF3300</span>, <span class="pl-c1">1</span>);</pre></div>

<p>Use the <code>drawRect</code> method to draw the rectangle. Its four arguments
are <code>x</code>, <code>y</code>, <code>width</code> and <code>height</code>.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">rectangle</span>.<span class="pl-en">drawRect</span>(x, y, width, height);</pre></div>

<p>Use <code>endFill</code> when you’re done.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">rectangle</span>.<span class="pl-en">endFill</span>();</pre></div>

<p>It’s just like the Canvas Drawing API! Here’s all the code you need to
draw a rectangle, change its position, and add it to the stage.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> rectangle <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Graphics</span>();
<span class="pl-smi">rectangle</span>.<span class="pl-en">lineStyle</span>(<span class="pl-c1">4</span>, <span class="pl-c1">0xFF3300</span>, <span class="pl-c1">1</span>);
<span class="pl-smi">rectangle</span>.<span class="pl-en">beginFill</span>(<span class="pl-c1">0x66CCFF</span>);
<span class="pl-smi">rectangle</span>.<span class="pl-en">drawRect</span>(<span class="pl-c1">0</span>, <span class="pl-c1">0</span>, <span class="pl-c1">64</span>, <span class="pl-c1">64</span>);
<span class="pl-smi">rectangle</span>.<span class="pl-en">endFill</span>();
<span class="pl-smi">rectangle</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">170</span>;
<span class="pl-smi">rectangle</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">170</span>;
<span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(rectangle);
</pre></div>

<p>This code makes a 64 by 64 blue rectangle with a red border at an x and y position of 170.</p>

<p><a id="user-content-circles"></a></p>

<h3><a id="user-content-circles" class="anchor" href="#circles" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Circles</h3>

<p>Make a circle with the <code>drawCircle</code> method. Its three arguments are
<code>x</code>, <code>y</code> and <code>radius</code></p>

<div class="highlight highlight-source-js"><pre><span class="pl-en">drawCircle</span>(x, y, radius)</pre></div>

<p>Unlike rectangles and sprites, a circle’s x and y position is also its
center point. Here’s how to make a violet colored circle with a radius of 32 pixels.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> circle <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Graphics</span>();
<span class="pl-smi">circle</span>.<span class="pl-en">beginFill</span>(<span class="pl-c1">0x9966FF</span>);
<span class="pl-smi">circle</span>.<span class="pl-en">drawCircle</span>(<span class="pl-c1">0</span>, <span class="pl-c1">0</span>, <span class="pl-c1">32</span>);
<span class="pl-smi">circle</span>.<span class="pl-en">endFill</span>();
<span class="pl-smi">circle</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">64</span>;
<span class="pl-smi">circle</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">130</span>;
<span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(circle);</pre></div>

<p><a id="user-content-ellipses"></a></p>

<h3><a id="user-content-ellipses" class="anchor" href="#ellipses" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Ellipses</h3>

<p>As a one-up on the Canvas Drawing API, Pixi lets you draw an ellipse
with the <code>drawEllipse</code> method.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-en">drawEllipse</span>(x, y, width, height);</pre></div>

<p>The x/y position defines the ellipse’s top left corner (imagine that
the ellipse is surrounded by an invisible rectangular bounding box -
the top left corner of that box will represent the ellipse's x/y
anchor position). Here’s a yellow ellipse that’s 50 pixels wide and 20 pixels high.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> ellipse <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Graphics</span>();
<span class="pl-smi">ellipse</span>.<span class="pl-en">beginFill</span>(<span class="pl-c1">0xFFFF00</span>);
<span class="pl-smi">ellipse</span>.<span class="pl-en">drawEllipse</span>(<span class="pl-c1">0</span>, <span class="pl-c1">0</span>, <span class="pl-c1">50</span>, <span class="pl-c1">20</span>);
<span class="pl-smi">ellipse</span>.<span class="pl-en">endFill</span>();
<span class="pl-smi">ellipse</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">180</span>;
<span class="pl-smi">ellipse</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">130</span>;
<span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(ellipse);</pre></div>

<p><a id="user-content-roundedrect"></a></p>

<h3><a id="user-content-rounded-rectangles" class="anchor" href="#rounded-rectangles" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Rounded rectangles</h3>

<p>Pixi also lets you make rounded rectangles with the <code>drawRoundedRect</code>
method. The last argument, <code>cornerRadius</code> is a number in pixels that
determines by how much the corners should be rounded.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-en">drawRoundedRect</span>(x, y, width, height, cornerRadius)</pre></div>

<p>Here's how to make a rounded rectangle with a corner radius of 10
pixels.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> roundBox <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Graphics</span>();
<span class="pl-smi">roundBox</span>.<span class="pl-en">lineStyle</span>(<span class="pl-c1">4</span>, <span class="pl-c1">0x99CCFF</span>, <span class="pl-c1">1</span>);
<span class="pl-smi">roundBox</span>.<span class="pl-en">beginFill</span>(<span class="pl-c1">0xFF9933</span>);
<span class="pl-smi">roundBox</span>.<span class="pl-en">drawRoundedRect</span>(<span class="pl-c1">0</span>, <span class="pl-c1">0</span>, <span class="pl-c1">84</span>, <span class="pl-c1">36</span>, <span class="pl-c1">10</span>)
<span class="pl-smi">roundBox</span>.<span class="pl-en">endFill</span>();
<span class="pl-smi">roundBox</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">48</span>;
<span class="pl-smi">roundBox</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">190</span>;
<span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(roundBox);</pre></div>

<p><a id="user-content-lines"></a></p>

<h3><a id="user-content-lines" class="anchor" href="#lines" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Lines</h3>

<p>You've seen in the examples above that the <code>lineStyle</code> method lets you
define a line.  You can use the <code>moveTo</code> and <code>lineTo</code> methods to draw the
start and end points of the line, in just the same way you can with the Canvas
Drawing API. Here’s how to draw a 4 pixel wide, white diagonal line.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> line <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Graphics</span>();
<span class="pl-smi">line</span>.<span class="pl-en">lineStyle</span>(<span class="pl-c1">4</span>, <span class="pl-c1">0xFFFFFF</span>, <span class="pl-c1">1</span>);
<span class="pl-smi">line</span>.<span class="pl-c1">moveTo</span>(<span class="pl-c1">0</span>, <span class="pl-c1">0</span>);
<span class="pl-smi">line</span>.<span class="pl-en">lineTo</span>(<span class="pl-c1">80</span>, <span class="pl-c1">50</span>);
<span class="pl-smi">line</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">32</span>;
<span class="pl-smi">line</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">32</span>;
<span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(line);</pre></div>

<p><code>PIXI.Graphics</code> objects, like lines, have <code>x</code> and <code>y</code> values, just
like sprites, so you can position them anywhere on the stage after
you've drawn them.</p>

<p><a id="user-content-polygons"></a></p>

<h3><a id="user-content-polygons" class="anchor" href="#polygons" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Polygons</h3>

<p>You can join lines together and fill them with colors to make complex
shapes using the <code>drawPolygon</code> method. <code>drawPolygon</code>'s argument is an
path array of x/y points that define the positions of each point on the
shape.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> path <span class="pl-k">=</span> [
  point1X, point1Y,
  point2X, point2Y,
  point3X, point3Y
];

<span class="pl-smi">graphicsObject</span>.<span class="pl-en">drawPolygon</span>(path);</pre></div>

<p><code>drawPolygon</code> will join those three points together to make the shape.
Here’s how to use <code>drawPolygon</code> to connect three lines together to
make a red triangle with a blue border. The triangle is drawn at
position 0,0 and then moved to its position on the stage using its
<code>x</code> and <code>y</code> properties.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> triangle <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Graphics</span>();
<span class="pl-smi">triangle</span>.<span class="pl-en">beginFill</span>(<span class="pl-c1">0x66FF33</span>);

<span class="pl-c">//Use `drawPolygon` to define the triangle as</span>
<span class="pl-c">//a path array of x/y positions</span>

<span class="pl-smi">triangle</span>.<span class="pl-en">drawPolygon</span>([
    <span class="pl-k">-</span><span class="pl-c1">32</span>, <span class="pl-c1">64</span>,             <span class="pl-c">//First point</span>
    <span class="pl-c1">32</span>, <span class="pl-c1">64</span>,              <span class="pl-c">//Second point</span>
    <span class="pl-c1">0</span>, <span class="pl-c1">0</span>                 <span class="pl-c">//Third point</span>
]);

<span class="pl-c">//Fill shape's color</span>
<span class="pl-smi">triangle</span>.<span class="pl-en">endFill</span>();

<span class="pl-c">//Position the triangle after you've drawn it.</span>
<span class="pl-c">//The triangle's x/y position is anchored to its first point in the path</span>
<span class="pl-smi">triangle</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">180</span>;
<span class="pl-smi">triangle</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">22</span>;

<span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(triangle);</pre></div>