<style type="text/css">

</style>
<div class=titlebar> Learning Pixi </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
  echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
  <div class=contentInBox>


<a href="?LP_Content">Содержание </a>
<br>

<p><a id="user-content-sizenscale"></a></p>

<h2><a id="user-content-size-and-scale" class="anchor" href="#size-and-scale" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Size and scale</h2>

<p>You can change a sprite's size by setting its <code>width</code> and <code>height</code>
properties. Here's how to give the cat a <code>width</code> of 80 pixels and a <code>height</code> of
120 pixels.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">cat</span>.<span class="pl-c1">width</span> <span class="pl-k">=</span> <span class="pl-c1">80</span>;
<span class="pl-smi">cat</span>.<span class="pl-c1">height</span> <span class="pl-k">=</span> <span class="pl-c1">120</span>;</pre></div>

<p>Add those two lines of code to the <code>setup</code> function, like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">setup</span>() {

  <span class="pl-c">//Create the `cat` sprite</span>
  <span class="pl-k">var</span> cat <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(resources[<span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>].<span class="pl-smi">texture</span>);

  <span class="pl-c">//Change the sprite's position</span>
  <span class="pl-smi">cat</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">96</span>;
  <span class="pl-smi">cat</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">96</span>;

  <span class="pl-c">//Change the sprite's size</span>
  <span class="pl-smi">cat</span>.<span class="pl-c1">width</span> <span class="pl-k">=</span> <span class="pl-c1">80</span>;
  <span class="pl-smi">cat</span>.<span class="pl-c1">height</span> <span class="pl-k">=</span> <span class="pl-c1">120</span>;

  <span class="pl-c">//Add the cat to the stage so you can see it</span>
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(cat);
}</pre></div>

<p>Here's the result:</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s05.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/05.png" alt="Cat's height and width changed" style="max-width:100%;"></a></p>

<p>You can see that the cat's position (its top left corner) didn't
change, only its width and height.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s06.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/06.png" alt="Cat's height and width changed - diagram" style="max-width:100%;"></a></p>

<p>Sprites also have <code>scale.x</code> and <code>scale.y</code> properties that change the
sprite's width and height proportionately. Here's how to set the cat's
scale to half size:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">cat</span>.<span class="pl-smi">scale</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">0.5</span>;
<span class="pl-smi">cat</span>.<span class="pl-smi">scale</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">0.5</span>;</pre></div>

<p>Scale values are numbers between 0 and 1 that represent a
percentage of the sprite's size. 1 means 100% (full size), while
0.5 means 50% (half size). You can double the sprite's size by setting
its scale values to 2, like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">cat</span>.<span class="pl-smi">scale</span>.<span class="pl-c1">x</span> <span class="pl-k">=</span> <span class="pl-c1">2</span>;
<span class="pl-smi">cat</span>.<span class="pl-smi">scale</span>.<span class="pl-c1">y</span> <span class="pl-k">=</span> <span class="pl-c1">2</span>;</pre></div>

<p>Pixi has an alternative, concise way for you set sprite's scale in one
line of code using the <code>scale.set</code> method.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">cat</span>.<span class="pl-smi">scale</span>.<span class="pl-c1">set</span>(<span class="pl-c1">0.5</span>, <span class="pl-c1">0.5</span>);</pre></div>

<p>If that appeals to you, use it!</p>

<p><a id="user-content-rotation"></a></p>