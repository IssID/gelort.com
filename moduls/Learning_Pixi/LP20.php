<style type="text/css">

</style>
<div class=titlebar> Learning Pixi </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
	echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
	<div class=contentInBox>


<a href="?LP_Content">Содержание </a>
<br>

<p><a id="user-content-text"></a></p>

<h2><a id="user-content-displaying-text" class="anchor" href="#displaying-text" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Displaying text</h2>

<p>Use a <code>Text</code> object (<code>PIXI.Text</code>) to display text on the stage. The constructor
takes two arguments: the text you want to display and a style object
that defines the font’s properties. Here's how to display the words
"Hello Pixi", in white, 32 pixel high sans-serif font.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> message <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Text</span>(
  <span class="pl-s"><span class="pl-pds">"</span>Hello Pixi!<span class="pl-pds">"</span></span>,
  {font<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">"</span>32px sans-serif<span class="pl-pds">"</span></span>, fill<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">"</span>white<span class="pl-pds">"</span></span>}
);

<span class="pl-smi">message</span>.<span class="pl-smi">position</span>.<span class="pl-c1">set</span>(<span class="pl-c1">54</span>, <span class="pl-c1">96</span>);
<span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(message);</pre></div>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s24.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/24.png" alt="Displaying text" style="max-width:100%;"></a></p>

<p>Pixi’s Text objects inherit from the <code>Sprite</code> class, so they
contain all the same properties like <code>x</code>, <code>y</code>, <code>width</code>, <code>height</code>,
<code>alpha</code>, and <code>rotation</code>. Position and resize text on the stage just like you would any other sprite.</p>

<p>If you want to change the content of a text object after you've
created it, use the <code>text</code> property.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">message</span>.<span class="pl-c1">text</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">"</span>Text changed!<span class="pl-pds">"</span></span>;</pre></div>

<p>Use the <code>style</code> property if you want to redefine the font properties.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">message</span>.<span class="pl-c1">style</span> <span class="pl-k">=</span> {fill<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">"</span>black<span class="pl-pds">"</span></span>, font<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">"</span>16px PetMe64<span class="pl-pds">"</span></span>};</pre></div>

<p>Pixi makes text objects by using the Canvas Drawing API to
render the text to an invisible and temporary canvas
element. It then turns the canvas into a WebGL texture so that it
can be mapped onto a sprite. That’s why the text’s color needs to be
wrapped in a string: it’s a Canvas Drawing API color value. As with
any canvas color values, you can use words for common colors like
“red” or “green”, or use rgba, hsla or hex values.</p>

<p>Other style properties that you can include are <code>stroke</code> for the font
outline color and <code>strokeThickness</code> for the outline thickness. Set the
text's <code>dropShadow</code> property to <code>true</code> to make the text display a
shadow. Use <code>dropShadowColor</code> to set the shadow's hexadecimal color
value, use <code>dropShadowAngle</code> to set the shadow's angle in radians, and
use <code>dropShadowDistance</code> to set the pixel height of a shadow. And
there's more: <a href="http://pixijs.github.io/docs/PIXI.Text.html">check out Pixi's Text documentation for the full
list</a>.</p>

<p>Pixi can also wrap long lines of text. Set the text’s <code>wordWrap</code> style
property to <code>true</code>, and then set <code>wordWrapWidth</code> to the maximum length
in pixels, that the line of text should be. Use the <code>align</code> property
to set the alignment for multi-line text.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">message</span>.<span class="pl-c1">style</span> <span class="pl-k">=</span> {wordWrap<span class="pl-k">:</span> <span class="pl-c1">true</span>, wordWrapWidth<span class="pl-k">:</span> <span class="pl-c1">100</span>, align<span class="pl-k">:</span> center};</pre></div>

<p>(Note: <code>align</code> doesn't affect single line text.)</p>

<p>If you want to use a custom font file, use the CSS <code>@font-face</code> rule
to link the font file to the HTML page where your Pixi application is
running.</p>

<div class="highlight highlight-source-js"><pre>@font<span class="pl-k">-</span>face {
  font<span class="pl-k">-</span>family<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">"</span>fontFamilyName<span class="pl-pds">"</span></span>;
  src<span class="pl-k">:</span> <span class="pl-en">url</span>(<span class="pl-s"><span class="pl-pds">"</span>fonts/fontFile.ttf<span class="pl-pds">"</span></span>);
}</pre></div>

<p>Add this <code>@font-face</code> rule to your HTML page's CSS style sheet.</p>

<p><a href="http://pixijs.github.io/docs/PIXI.extras.BitmapText.html">Pixi also has support for bitmap
fonts</a>. You
can use Pixi's loader to load Bitmap font XML files, the same way you
load JSON or image files.</p>