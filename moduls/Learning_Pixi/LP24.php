<style type="text/css">

</style>
<div class=titlebar> Learning Pixi </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
	echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
	<div class=contentInBox>


<a href="?LP_Content">Содержание </a>
<br>

<p><a id="user-content-takingitfurther"></a></p>

<h2><a id="user-content-taking-it-further" class="anchor" href="#taking-it-further" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Taking it further</h2>

<p>Pixi can do a lot, but it can't do everything! If you want to start
making games or complex interactive applications with Pixi, you'll need
to use some helper libraries:</p>

<ul>
<li><a href="https://github.com/kittykatattack/bump">Bump</a>: A complete suite of 2D collision functions for games.</li>
<li><a href="https://github.com/kittykatattack/tink">Tink</a>: Drag-and-drop, buttons, a universal pointer and other
helpful interactivity tools.</li>
<li><a href="https://github.com/kittykatattack/charm">Charm</a>: Easy-to-use tweening animation effects for Pixi sprites.</li>
<li><a href="https://github.com/kittykatattack/dust">Dust</a>: Particle effects for creating things like explosions, fire
and magic.</li>
<li><a href="https://github.com/kittykatattack/spriteUtilities">Sprite Utilities</a>: Easier and more intuitive ways to
create and use Pixi sprites, as well adding a state machine and
animation player. Makes working with Pixi a lot more fun.</li>
<li><a href="https://github.com/kittykatattack/sound.js">Sound.js</a>: A micro-library for loading, controlling and generating
sound and music effects. Everything you need to add sound to games.</li>
<li><a href="https://github.com/kittykatattack/smoothie">Smoothie</a>: Ultra-smooth sprite animation using true delta-time interpolation. It also lets you specify the fps (frames-per-second) at which your game or application runs, and completely separates your sprite rendering loop from your application logic loop.</li>
</ul>

<p>You can find out how to use all these libraries with Pixi in the book 
<a href="http://www.springer.com/us/book/9781484210956">Learn PixiJS</a>.</p>