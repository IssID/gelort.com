<style type="text/css">

</style>
<div class=titlebar> Learning Pixi </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
	echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
	<div class=contentInBox>


<a href="?LP_Content">Содержание </a>
<br>

<p><a id="user-content-loadingatlas"></a></p>

<h2><a id="user-content-loading-the-texture-atlas" class="anchor" href="#loading-the-texture-atlas" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Loading the texture atlas</h2>

<p>To get the texture atlas into Pixi, load it using Pixi’s
<code>loader</code>. If the JSON file was made with Texture Packer, the
<code>loader</code> will interpret the data and create a texture from each
frame on the tileset automatically.  Here’s how to use the <code>loader</code> to load the <code>treasureHunter.json</code>
file. When it has loaded, the <code>setup</code> function will run.</p>

<div class="highlight highlight-source-js"><pre>loader
  .<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">"</span>images/treasureHunter.json<span class="pl-pds">"</span></span>)
  .<span class="pl-c1">load</span>(setup);</pre></div>

<p>Each image on the tileset is now an individual texture in Pixi’s
cache. You can access each texture in the cache with the same name it
had in Texture Packer (“blob.png”, “dungeon.png”, “explorer.png”,
etc.).</p>