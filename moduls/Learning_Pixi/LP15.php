<style type="text/css">

</style>
<div class=titlebar> Learning Pixi </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
  echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
  <div class=contentInBox>


<a href="?LP_Content">Содержание </a>
<br>

<p><a id="user-content-velocity"></a></p>

<h2><a id="user-content-using-velocity-properties" class="anchor" href="#using-velocity-properties" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Using velocity properties</h2>

<p>To give you more flexibility, its a good idea control a sprite's
movement speed using two <strong>velocity properties</strong>: <code>vx</code> and <code>vy</code>. <code>vx</code>
is used to set the sprite's speed and direction on the x axis
(horizontally). <code>vy</code> is
used to set the sprite's speed and direction on the y axis (vertically). Instead of
changing a sprite's <code>x</code> and <code>y</code> values directly, first update the velocity
variables, and then assign those velocity values to the sprite. This is an
extra bit of modularity that you'll need for interactive game animation.</p>

<p>The first step is to create <code>vx</code> and <code>vy</code> properties on your sprite,
and give them an initial value.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
<span class="pl-smi">cat</span>.<span class="pl-smi">vy</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;</pre></div>

<p>Setting <code>vx</code> and <code>vy</code> to 0 means that the sprite isn't moving.</p>

<p>Next, in the game loop, update <code>vx</code> and <code>vy</code> with the velocity
that you want the sprite to move at. Then assign those values to the
sprite's <code>x</code> and <code>y</code> properties. Here's how you could use this
technique to make the cat sprite move down and to right at one pixel each
frame:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">setup</span>() {

  <span class="pl-c">//Create the `cat` sprite </span>
  cat <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(resources[<span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>].<span class="pl-smi">texture</span>);
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(cat);

  <span class="pl-c">//Initialize the cat's velocity variables</span>
  <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;
  <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;

  <span class="pl-c">//Start the game loop</span>
  <span class="pl-en">gameLoop</span>();
}

<span class="pl-k">function</span> <span class="pl-en">gameLoop</span>(){

  <span class="pl-c">//Loop this function 60 times per second</span>
  <span class="pl-en">requestAnimationFrame</span>(gameLoop);

  <span class="pl-c">//Update the cat's velocity</span>
  <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span> <span class="pl-k">=</span> <span class="pl-c1">1</span>;
  <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span> <span class="pl-k">=</span> <span class="pl-c1">1</span>;

  <span class="pl-c">//Apply the velocity values to the cat's </span>
  <span class="pl-c">//position to make it move</span>
  <span class="pl-smi">cat</span>.<span class="pl-c1">x</span> <span class="pl-k">+=</span> <span class="pl-smi">cat</span>.<span class="pl-smi">vx</span>;
  <span class="pl-smi">cat</span>.<span class="pl-c1">y</span> <span class="pl-k">+=</span> <span class="pl-smi">cat</span>.<span class="pl-smi">vy</span>;

  <span class="pl-c">//Render the stage</span>
  <span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);
}

</pre></div>

<p>When you run this code, the cat will move down and to the right at one
pixel per frame:</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s16.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/16.png" alt="Moving sprites" style="max-width:100%;"></a></p>

<p>What if you want to make the cat move in a different direction? To
make the cat move to the left, give it a <code>vx</code> value of <code>-1</code>. To make
it move up, give the cat a <code>vy</code> value of <code>-1</code>. To make the cat move
faster, give it larger <code>vx</code> and <code>vy</code> values, like <code>3</code>, <code>5</code>, <code>-2</code>, or
<code>-4</code>.</p>

<p>You'll see ahead how modularizing a sprite's velocity with <code>vx</code> and
<code>vy</code> velocity properties helps with keyboard and mouse pointer
control systems for games, as well as making it easier to implement physics.</p>