<style type="text/css">

</style>
<div class=titlebar> Learning Pixi </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
	echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
	<div class=contentInBox>


<a href="?LP_Content">Содержание </a>
<br>

<p><a id="user-content-textureatlas"></a></p>

<h2><a id="user-content-using-a-texture-atlas" class="anchor" href="#using-a-texture-atlas" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Using a texture atlas</h2>

<p>If you’re working on a big, complex game, you’ll want a fast and
efficient way to create sprites from tilesets. This is where a
<strong>texture atlas</strong> becomes really useful. A texture atlas is a JSON
data file that contains the positions and sizes of sub-images on a
matching tileset PNG image. If you use a texture atlas, all you need
to know about the sub-image you want to display is its name. You
can arrange your tileset images in any order and the JSON file
will keep track of their sizes and positions for you. This is
really convenient because it means the sizes and positions of
tileset images aren’t hard-coded into your game program. If you
make changes to the tileset, like adding images, resizing them,
or removing them, just re-publish the JSON file and your game will
use that data to display the correct images. You won’t have to
make any changes to your game code.</p>

<p>Pixi is compatible with a standard JSON texture atlas format that is
output by a popular software tool called <a href="https://www.codeandweb.com/texturepacker">Texture
Packer</a>. Texture Packer’s
“Essential” license is free. Let’s find out how to use it to make a
texture atlas, and load the atlas into Pixi. (You don’t have to use
Texture Packer. Similar tools, like <a href="http://renderhjs.net/shoebox/">Shoebox</a> or <a href="https://github.com/krzysztof-o/spritesheet.js/">spritesheet.js</a>, output PNG and JSON files
in a standard format that is compatible with Pixi.)</p>

<p>First, start with a collection of individual image files that you'd
like to use in your game.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s11.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/11.png" alt="Image files" style="max-width:100%;"></a></p>

<p>(All the images in this section were created by Lanea Zimmerman. You
can find more of her artwork
<a href="http://opengameart.org/users/sharm">here</a>.
Thanks, Lanea!)</p>

<p>Next, open Texture Packer and choose <strong>JSON Hash</strong> as the framework
type. Drag your images into Texture Packer's workspace. (You can
also point Texture Packer to any folder that contains your images.)
It will automatically arrange the images on a single tileset image, and give them names that match their original image names.</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s12.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/12.png" alt="Image files" style="max-width:100%;"></a></p>

<p>(If you're using the free version of
Texture Packer, set <strong>Algorithm</strong> to <code>Basic</code>, set <strong>Trim mode</strong> to
<code>None</code>, set <strong>Size constraints</strong> to <code>Any size</code> and slide the <strong>PNG Opt
Level</strong> all the way to the left to <code>0</code>. These are the basic
settings that will allow the free version of Texture Packer to create
your files without any warnings or errors.)</p>

<p>When you’re done, click the <strong>Publish</strong> button. Choose the file name and
location, and save the published files. You’ll end up with 2 files: a
PNG file and a JSON file. In this example my file names are
<code>treasureHunter.json</code> and <code>treasureHunter.png</code>. To make your life easier,
just keep both files in your project’s <code>images</code> folder. (You can think
of the JSON file as extra metadata for the image file, so it makes
sense to keep both files in the same folder.)
The JSON file describes the name, size and position of each of the
sub-images
in the tileset. Here’s an excerpt that describes the blob monster
sub-image.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-s"><span class="pl-pds">"</span>blob.png<span class="pl-pds">"</span></span><span class="pl-k">:</span>
{
    <span class="pl-s"><span class="pl-pds">"</span>frame<span class="pl-pds">"</span></span><span class="pl-k">:</span> {<span class="pl-s"><span class="pl-pds">"</span>x<span class="pl-pds">"</span></span><span class="pl-k">:</span><span class="pl-c1">55</span>,<span class="pl-s"><span class="pl-pds">"</span>y<span class="pl-pds">"</span></span><span class="pl-k">:</span><span class="pl-c1">2</span>,<span class="pl-s"><span class="pl-pds">"</span>w<span class="pl-pds">"</span></span><span class="pl-k">:</span><span class="pl-c1">32</span>,<span class="pl-s"><span class="pl-pds">"</span>h<span class="pl-pds">"</span></span><span class="pl-k">:</span><span class="pl-c1">24</span>},
    <span class="pl-s"><span class="pl-pds">"</span>rotated<span class="pl-pds">"</span></span><span class="pl-k">:</span> <span class="pl-c1">false</span>,
    <span class="pl-s"><span class="pl-pds">"</span>trimmed<span class="pl-pds">"</span></span><span class="pl-k">:</span> <span class="pl-c1">false</span>,
    <span class="pl-s"><span class="pl-pds">"</span>spriteSourceSize<span class="pl-pds">"</span></span><span class="pl-k">:</span> {<span class="pl-s"><span class="pl-pds">"</span>x<span class="pl-pds">"</span></span><span class="pl-k">:</span><span class="pl-c1">0</span>,<span class="pl-s"><span class="pl-pds">"</span>y<span class="pl-pds">"</span></span><span class="pl-k">:</span><span class="pl-c1">0</span>,<span class="pl-s"><span class="pl-pds">"</span>w<span class="pl-pds">"</span></span><span class="pl-k">:</span><span class="pl-c1">32</span>,<span class="pl-s"><span class="pl-pds">"</span>h<span class="pl-pds">"</span></span><span class="pl-k">:</span><span class="pl-c1">24</span>},
    <span class="pl-s"><span class="pl-pds">"</span>sourceSize<span class="pl-pds">"</span></span><span class="pl-k">:</span> {<span class="pl-s"><span class="pl-pds">"</span>w<span class="pl-pds">"</span></span><span class="pl-k">:</span><span class="pl-c1">32</span>,<span class="pl-s"><span class="pl-pds">"</span>h<span class="pl-pds">"</span></span><span class="pl-k">:</span><span class="pl-c1">24</span>},
    <span class="pl-s"><span class="pl-pds">"</span>pivot<span class="pl-pds">"</span></span><span class="pl-k">:</span> {<span class="pl-s"><span class="pl-pds">"</span>x<span class="pl-pds">"</span></span><span class="pl-k">:</span><span class="pl-c1">0.5</span>,<span class="pl-s"><span class="pl-pds">"</span>y<span class="pl-pds">"</span></span><span class="pl-k">:</span><span class="pl-c1">0.5</span>}
},</pre></div>

<p>The <code>treasureHunter.json</code> file also contains “dungeon.png”,
“door.png”, "exit.png", and "explorer.png" properties each with
similar data. Each of these sub-images are called <strong>frames</strong>. Having
this data is really helpful because now you don’t need to know the
size and position of each sub-image in the tileset. All you need to
know is the sprite’s <strong>frame id</strong>. The frame id is just the name
of the original image file, like "blob.png" or "explorer.png".</p>

<p>Among the many advantages to using a texture atlas is that you can
easily add 2 pixels of padding around each image (Texture Packer does
this by default.) This is important to prevent the possibility of
<strong>texture bleed</strong>. Texture bleed is an effect that happens when the
edge of an adjacent image on the tileset appears next to a sprite.
This happens because of the way your computer's GPU (Graphics
Processing Unit) decides how to round fractional pixels values. Should
it round them up or down? This will be different for each GPU.
Leaving 1 or 2 pixels spacing around images on a tilseset makes all
images display consistently.</p>

<p>(Note: If you have two pixels of padding around a graphic, and you still notice a strange "off by one pixel" glitch in the
way Pixi is displaying it, try changing the texture's scale mode
algorithm. Here's how: <code>texture.baseTexture.scaleMode =
PIXI.SCALE_MODES.NEAREST;</code>. These glitches can sometimes happen
because of GPU floating point rounding errors.)</p>

<p>Now that you know how to create a texture atlas, let's find out how to
load it into your game code.</p>
