<style type="text/css">

</style>
<div class=titlebar> Learning Pixi </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
  echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
  <div class=contentInBox>


<a href="?LP_Content">Содержание </a>
<br>

<p><a id="user-content-displaying"></a></p>

<h2><a id="user-content-displaying-sprites" class="anchor" href="#displaying-sprites" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Displaying sprites</h2>

<p>After you've loaded an image, and used it to make a sprite, there
are two more things you have to do before you can actually see it on
the canvas:</p>

<p>-1. You need to add the sprite to Pixi's <code>stage</code> with the <code>stage.addChild</code> method, like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(cat);</pre></div>

<p>The stage is the main container that holds all of your sprites.</p>

<p>-2. You need to tell Pixi's <code>renderer</code> to render the stage.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);</pre></div>

<p><strong>None of your sprites will be visible before you do these two
things</strong>.</p>

<p>Before we continue, let's look at a practical example of how to use what
you've just learnt to display a single image. In the <code>examples/images</code>
folder you'll find a 64 by 64 pixel PNG image of a cat.</p>

<p><a href="https://github.com/kittykatattack/learningPixi/blob/master/examples/images/cat.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/cat.png" alt="Basic display" style="max-width:100%;"></a></p>

<p>Here's all the JavaScript code you need to load the image, create a
sprite, and display it on Pixi's stage:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> stage <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.Container</span>(),
    renderer <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-en">autoDetectRenderer</span>(<span class="pl-c1">256</span>, <span class="pl-c1">256</span>);
<span class="pl-c1">document</span>.<span class="pl-c1">body</span>.<span class="pl-c1">appendChild</span>(<span class="pl-smi">renderer</span>.<span class="pl-smi">view</span>);

<span class="pl-c">//Use Pixi's built-in `loader` object to load an image</span>
<span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>
  .<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>)
  .<span class="pl-c1">load</span>(setup);

<span class="pl-c">//This `setup` function will run when the image has loaded</span>
<span class="pl-k">function</span> <span class="pl-en">setup</span>() {

  <span class="pl-c">//Create the `cat` sprite from the texture</span>
  <span class="pl-k">var</span> cat <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.Sprite</span>(
    <span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>.<span class="pl-smi">resources</span>[<span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>].<span class="pl-smi">texture</span>
  );

  <span class="pl-c">//Add the cat to the stage</span>
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(cat);

  <span class="pl-c">//Render the stage   </span>
  <span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);
}</pre></div>

<p>When this code runs, here's what you'll see:</p>

<p><a href="https://github.comhttps://github.com/kittykatattack/learningPixi/blob/master/examples/images/screenshot/s02.png" target="_blank"><img src="https://raw.githubusercontent.com/kittykatattack/learningPixi/master/examples/images/screenshots/02.png" alt="Cat on the stage" style="max-width:100%;"></a></p>

<p>Now we're getting somewhere!</p>

<p>If you ever need to remove a sprite from the stage, use the <code>removeChild</code> method:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">stage</span>.<span class="pl-c1">removeChild</span>(anySprite)</pre></div>

<p>But usually setting a sprite’s <code>visible</code> property to <code>false</code> will be a simpler and more efficient way of making sprites disappear.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">anySprite</span>.<span class="pl-smi">visible</span> <span class="pl-k">=</span> <span class="pl-c1">false</span>;</pre></div>

<p><a id="user-content-usingaliases"></a></p>

<h3><a id="user-content-using-aliases" class="anchor" href="#using-aliases" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Using aliases</h3>

<p>You can save yourself a little typing and make your code more readable
by creating short-form aliases for the Pixi objects and methods that you
use frequently. For example, is <code>PIXI.utils.TextureCache</code> too much to
type? I think so, especially in a big project where you might use it
it dozens of times. So, create a shorter alias that points to it, like
this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> TextureCache <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">utils</span>.<span class="pl-smi">TextureCache</span></pre></div>

<p>Then, use that alias in place of the original, like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> texture <span class="pl-k">=</span> TextureCache[<span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>];</pre></div>

<p>In addition to letting you write more succinct code, using aliases has
an extra benefit: it helps to buffer you slightly from Pixi's frequently
changing API. If Pixi's API changes in future
versions - which it will! - you just need to update these aliases to
Pixi object and methods in one place, at the beginning of
your program, instead of every instance where they're used throughout
your code. So when Pixi's development team decides they want to
rearrange the furniture a bit, you'll be one step ahead of them!</p>

<p>To see how to do this, let's re-write the code we wrote to load an image and display it,
using aliases for all the Pixi objects and methods.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c">//Aliases</span>
<span class="pl-k">var</span> Container <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">Container</span>,
    autoDetectRenderer <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">autoDetectRenderer</span>,
    loader <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>,
    resources <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>.<span class="pl-smi">resources</span>,
    Sprite <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">Sprite</span>;

<span class="pl-c">//Create a Pixi stage and renderer and add the </span>
<span class="pl-c">//renderer.view to the DOM</span>
<span class="pl-k">var</span> stage <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Container</span>(),
    renderer <span class="pl-k">=</span> <span class="pl-en">autoDetectRenderer</span>(<span class="pl-c1">256</span>, <span class="pl-c1">256</span>);
<span class="pl-c1">document</span>.<span class="pl-c1">body</span>.<span class="pl-c1">appendChild</span>(<span class="pl-smi">renderer</span>.<span class="pl-smi">view</span>);

<span class="pl-c">//load an image and run the `setup` function when it's done</span>
loader
  .<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>)
  .<span class="pl-c1">load</span>(setup);

<span class="pl-k">function</span> <span class="pl-en">setup</span>() {

  <span class="pl-c">//Create the `cat` sprite, add it to the stage, and render it</span>
  <span class="pl-k">var</span> cat <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">Sprite</span>(resources[<span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>].<span class="pl-smi">texture</span>);  
  <span class="pl-smi">stage</span>.<span class="pl-en">addChild</span>(cat);
  <span class="pl-smi">renderer</span>.<span class="pl-en">render</span>(stage);
}
</pre></div>

<p>Most of the examples in this tutorial will use aliases for Pixi
objects that follow this same model. Unless otherwise stated, you can
assume that all the code examples use aliases like these.</p>

<p>This is all you need to know to start loading images and creating
sprites.</p>

<p><a id="user-content-alittlemoreaboutloadingthings"></a></p>

<h3><a id="user-content-a-little-more-about-loading-things" class="anchor" href="#a-little-more-about-loading-things" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>A little more about loading things</h3>

<p>The format I've shown you above is what I suggest you use as your
standard template for loading images and displaying sprites. So, you
can safely ignore the next few paragraphs and jump straight to the
next section, "Positioning sprites." But Pixi's <code>loader</code> object is
quite sophisticated and includes a few features that you should be
aware of, even if you don't use them on a regular basis. Let's
look at some of the most useful.</p>

<p><a id="user-content-makeaspritefromanordinaryjavascriptimageobject"></a></p>

<h4><a id="user-content-make-a-sprite-from-an-ordinary-javascript-image-object-or-canvas" class="anchor" href="#make-a-sprite-from-an-ordinary-javascript-image-object-or-canvas" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Make a sprite from an ordinary JavaScript Image object or Canvas</h4>

<p>For optimization and efficiency it’s always best to make a sprite from
a texture that’s been pre-loaded into Pixi’s texture cache. But if for
some reason you need to make a texture from a regular JavaScript
<code>Image</code>
object, you can do that using Pixi’s <code>BaseTexture</code> and <code>Texture</code>
classes:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> base <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.BaseTexture</span>(anyImageObject),
    texture <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.Texture</span>(base),
    sprite <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.Sprite</span>(texture);</pre></div>

<p>You can use <code>BaseTexture.fromCanvas</code> if you want to make a texture
from any existing canvas element:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> base <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.BaseTexture.fromCanvas</span>(anyCanvasElement),</pre></div>

<p>If you want to change the texture the sprite is displaying, use the
<code>texture</code> property. Set it to any <code>Texture</code> object, like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-smi">anySprite</span>.<span class="pl-smi">texture</span> <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">utils</span>.<span class="pl-smi">TextureCache</span>[<span class="pl-s"><span class="pl-pds">"</span>anyTexture.png<span class="pl-pds">"</span></span>];</pre></div>

<p>You can use this technique to interactively change the sprite’s
appearance if something significant happens to it in the game.</p>

<p><a id="user-content-assigninganametoaloadingfile"></a></p>

<h4><a id="user-content-assigning-a-name-to-a-loading-file" class="anchor" href="#assigning-a-name-to-a-loading-file" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Assigning a name to a loading file</h4>

<p>It's possible to assign a unique name to each resource you want to
load. Just supply the name (a string) as the first argument in the
<code>add</code> method. For example, here's how to name an image of a cat as
<code>catImage</code>.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>
  .<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">"</span>catImage<span class="pl-pds">"</span></span>, <span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>)
  .<span class="pl-c1">load</span>(setup);</pre></div>

<p>This creates an object called <code>catImage</code> in <code>loader.resources</code>.
That means you can create a sprite by referencing the <code>catImage</code> object, like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> cat <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.Sprite</span>(<span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>.<span class="pl-smi">resources</span>.<span class="pl-smi">catImage</span>.<span class="pl-smi">texture</span>);</pre></div>

<p>However, I recommend you don't use this feature! That's because you'll
have to remember all names you gave each loaded files, as well as make sure
you don't accidentally use the same name more than once. Using the file path name, as we've done in previous
examples is simpler and less error prone.</p>

<p><a id="user-content-monitoringloadprogress"></a></p>

<h4><a id="user-content-monitoring-load-progress" class="anchor" href="#monitoring-load-progress" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Monitoring load progress</h4>

<p>Pixi's loader has a special <code>progress</code> event that will call a
customizable function that will run each time a file loads. <code>progress</code> events are called by the
<code>loader</code>'s <code>on</code> method, like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>.<span class="pl-en">on</span>(<span class="pl-s"><span class="pl-pds">"</span>progress<span class="pl-pds">"</span></span>, loadProgressHandler);</pre></div>

<p>Here's how to include the <code>on</code> method in the loading chain, and call
a user-definable function called <code>loadProgressHandler</code> each time a file loads.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>
  .<span class="pl-c1">add</span>([
    <span class="pl-s"><span class="pl-pds">"</span>images/one.png<span class="pl-pds">"</span></span>,
    <span class="pl-s"><span class="pl-pds">"</span>images/two.png<span class="pl-pds">"</span></span>,
    <span class="pl-s"><span class="pl-pds">"</span>images/three.png<span class="pl-pds">"</span></span>
  ])
  .<span class="pl-en">on</span>(<span class="pl-s"><span class="pl-pds">"</span>progress<span class="pl-pds">"</span></span>, loadProgressHandler)
  .<span class="pl-c1">load</span>(setup);

<span class="pl-k">function</span> <span class="pl-en">loadProgressHandler</span>() {
  <span class="pl-en">console</span>.<span class="pl-c1">log</span>(<span class="pl-s"><span class="pl-pds">"</span>loading<span class="pl-pds">"</span></span>); 
}

<span class="pl-k">function</span> <span class="pl-en">setup</span>() {
  <span class="pl-en">console</span>.<span class="pl-c1">log</span>(<span class="pl-s"><span class="pl-pds">"</span>setup<span class="pl-pds">"</span></span>);
}</pre></div>

<p>Each time one of the files loads, the progress event calls
<code>loadProgressHandler</code> to display "loading" in the console. When all three files have loaded, the <code>setup</code>
function will run. Here's the output of the above code in the console:</p>

<div class="highlight highlight-source-js"><pre>loading
loading
loading
setup</pre></div>

<p>That's neat, but it gets better. You can also find out exactly which file
has loaded and what percentage of overall files are have currently
loaded. You can do this by adding optional <code>loader</code> and
<code>resource</code> parameters to the <code>loadProgressHandler</code>, like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">function</span> <span class="pl-en">loadProgressHandler</span>(<span class="pl-smi">loader</span>, <span class="pl-smi">resource</span>) { <span class="pl-c">//...</span></pre></div>

<p>You can then use <code>resource.url</code> to find the file that's currently
loaded. (Use <code>resource.name</code> if you want to find the optional name
that you might have assigned to the file, as the first argument in the
<code>add</code> method.) And you can use <code>loader.progress</code> to find what
percentage of total resources have currently loaded. Here's some code
that does just that.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>
  .<span class="pl-c1">add</span>([
    <span class="pl-s"><span class="pl-pds">"</span>images/one.png<span class="pl-pds">"</span></span>,
    <span class="pl-s"><span class="pl-pds">"</span>images/two.png<span class="pl-pds">"</span></span>,
    <span class="pl-s"><span class="pl-pds">"</span>images/three.png<span class="pl-pds">"</span></span>
  ])
  .<span class="pl-en">on</span>(<span class="pl-s"><span class="pl-pds">"</span>progress<span class="pl-pds">"</span></span>, loadProgressHandler)
  .<span class="pl-c1">load</span>(setup);

<span class="pl-k">function</span> <span class="pl-en">loadProgressHandler</span>(<span class="pl-smi">loader</span>, <span class="pl-smi">resource</span>) {

  <span class="pl-c">//Display the file `url` currently being loaded</span>
  <span class="pl-en">console</span>.<span class="pl-c1">log</span>(<span class="pl-s"><span class="pl-pds">"</span>loading: <span class="pl-pds">"</span></span> <span class="pl-k">+</span> <span class="pl-smi">resource</span>.<span class="pl-smi">url</span>); 

  <span class="pl-c">//Display the precentage of files currently loaded</span>
  <span class="pl-en">console</span>.<span class="pl-c1">log</span>(<span class="pl-s"><span class="pl-pds">"</span>progress: <span class="pl-pds">"</span></span> <span class="pl-k">+</span> <span class="pl-smi">loader</span>.<span class="pl-smi">progress</span> <span class="pl-k">+</span> <span class="pl-s"><span class="pl-pds">"</span>%<span class="pl-pds">"</span></span>); 

  <span class="pl-c">//If you gave your files names as the first argument </span>
  <span class="pl-c">//of the `add` method, you can access them like this</span>
  <span class="pl-c">//console.log("loading: " + resource.name);</span>
}

<span class="pl-k">function</span> <span class="pl-en">setup</span>() {
  <span class="pl-en">console</span>.<span class="pl-c1">log</span>(<span class="pl-s"><span class="pl-pds">"</span>All files loaded<span class="pl-pds">"</span></span>);
}</pre></div>

<p>Here's what this code will display in the console when it runs:</p>

<div class="highlight highlight-source-js"><pre>loading<span class="pl-k">:</span> images<span class="pl-k">/</span><span class="pl-smi">one</span>.<span class="pl-smi">png</span>
progress<span class="pl-k">:</span> <span class="pl-c1">33.333333333333336</span><span class="pl-k">%</span>
loading<span class="pl-k">:</span> images<span class="pl-k">/</span><span class="pl-smi">two</span>.<span class="pl-smi">png</span>
progress<span class="pl-k">:</span> <span class="pl-c1">66.66666666666667</span><span class="pl-k">%</span>
loading<span class="pl-k">:</span> images<span class="pl-k">/</span><span class="pl-smi">three</span>.<span class="pl-smi">png</span>
progress<span class="pl-k">:</span> <span class="pl-c1">100</span><span class="pl-k">%</span>
All files loaded</pre></div>

<p>That's really cool, because you could use this as the basis for
creating a loading progress bar. </p>

<p>(Note: There are additional properties you can access on the
<code>resource</code> object. <code>resource.error</code> will tell you of any possible
error that happened while
trying to load a file. <code>resource.data</code> lets you
access the file's raw binary data.)</p>

<p><a id="user-content-moreaboutpixisloader"></a></p>

<h4><a id="user-content-more-about-pixis-loader" class="anchor" href="#more-about-pixis-loader" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>More about Pixi's loader</h4>

<p>Pixi's loader is ridiculously feature-rich and configurable. Let's
take a quick bird's-eye view of it's usage to
get you started.</p>

<p>The loader's chainable <code>add</code> method takes 4 basic arguments:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-en">add</span>(name, url, optionObject, callbackFunction)</pre></div>

<p>Here's what the loader's source code documentation has to say about
these parameters:</p>

<p><code>name</code> (string): The name of the resource to load. If it's not passed, the <code>url</code> is used.
<code>url</code> (string): The url for this resource, relative to the <code>baseUrl</code> of the loader.
<code>options</code> (object literal): The options for the load.
<code>options.crossOrigin</code> (Boolean): Is the request cross-origin? The default is to determine automatically.
<code>options.loadType</code>: How should the resource be loaded? The default value is <code>Resource.LOAD_TYPE.XHR</code>.
<code>options.xhrType</code>: How should the data being loaded be interpreted
when using XHR? The default value is <code>Resource.XHR_RESPONSE_TYPE.DEFAULT</code>
<code>callbackFunction</code>: The function to call when this specific resource completes loading.</p>

<p>The only one of these arguments that's required is the <code>url</code> (the file that you want to
load.) </p>

<p>Here are some examples of some ways you could use the <code>add</code>
method to load files. These first ones are what the docs call the loader's "normal syntax":</p>

<div class="highlight highlight-source-js"><pre>.<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">'</span>key<span class="pl-pds">'</span></span>, <span class="pl-s"><span class="pl-pds">'</span>http://...<span class="pl-pds">'</span></span>, <span class="pl-k">function</span> () {})
.<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">'</span>http://...<span class="pl-pds">'</span></span>, <span class="pl-k">function</span> () {})
.<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">'</span>http://...<span class="pl-pds">'</span></span>)</pre></div>

<p>And these are examples of the loader's "object syntax":</p>

<div class="highlight highlight-source-js"><pre>.<span class="pl-c1">add</span>({
  name<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">'</span>key2<span class="pl-pds">'</span></span>,
  url<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">'</span>http://...<span class="pl-pds">'</span></span>
}, <span class="pl-k">function</span> () {})

.<span class="pl-c1">add</span>({
  url<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">'</span>http://...<span class="pl-pds">'</span></span>
}, <span class="pl-k">function</span> () {})

.<span class="pl-c1">add</span>({
  name<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">'</span>key3<span class="pl-pds">'</span></span>,
  url<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">'</span>http://...<span class="pl-pds">'</span></span>
  <span class="pl-en">onComplete</span><span class="pl-k">:</span> <span class="pl-k">function</span> () {}
})

.<span class="pl-c1">add</span>({
  url<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">'</span>https://...<span class="pl-pds">'</span></span>,
  <span class="pl-en">onComplete</span><span class="pl-k">:</span> <span class="pl-k">function</span> () {},
  crossOrigin<span class="pl-k">:</span> <span class="pl-c1">true</span>
})</pre></div>

<p>You can also pass the <code>add</code> method an array of objects, or urls, or
both:</p>

<div class="highlight highlight-source-js"><pre>.<span class="pl-c1">add</span>([
  {name<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">'</span>key4<span class="pl-pds">'</span></span>, url<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">'</span>http://...<span class="pl-pds">'</span></span>, <span class="pl-en">onComplete</span><span class="pl-k">:</span> <span class="pl-k">function</span> () {} },
  {url<span class="pl-k">:</span> <span class="pl-s"><span class="pl-pds">'</span>http://...<span class="pl-pds">'</span></span>, <span class="pl-en">onComplete</span><span class="pl-k">:</span> <span class="pl-k">function</span> () {} },
  <span class="pl-s"><span class="pl-pds">'</span>http://...<span class="pl-pds">'</span></span>
]);</pre></div>

<p>(Note: If you ever need to reset the loader to load a new batch of files, call the
loader's <code>reset</code> method: <code>PIXI.loader.reset();</code>)</p>

<p>Pixi's loader has many more advanced features, including options to
let you load and parse binary files of all types. This is not
something you'll need to do on a day-to-day basis, and way outside the
scope of this tutorial, so <a href="https://github.com/englercj/resource-loader">make sure to check out the loader's GitHub repository
for more information</a>.</p>

<p><a id="user-content-positioning"></a></p>