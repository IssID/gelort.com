<style type="text/css">

</style>
<div class=titlebar> Learning Pixi </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
	echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
	<div class=contentInBox>


<a href="?LP_Content">Содержание </a>
<br>

<p><a id="user-content-spriteproperties"></a></p>

<h2><a id="user-content-more-about-sprites" class="anchor" href="#more-about-sprites" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>More about sprites</h2>

<p>You've learnt how to use quite a few useful sprite properties so far, like <code>x</code>, <code>y</code>,
<code>visible</code>, and <code>rotation</code> that give you a lot of control over a
sprite's position and appearance. But Pixi Sprites also have many more
useful properties that are fun to play with. <a href="http://pixijs.github.io/docs/PIXI.Sprite.html">Here's the full list.</a></p>

<p>How does Pixi’s class inheritance system work? (<a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Introduction_to_Object-Oriented_JavaScript">What is a <strong>class</strong>
and what is <strong>inheritence</strong>? Click this link to find out.</a>) Pixi’s sprites are
built on an inheritance model that follows this chain:</p>

<pre><code>DisplayObject &gt; Container &gt; Sprite
</code></pre>

<p>Inheritance just means that the classes later in the chain use
properties and methods from classes earlier in the chain.
The most basic class is <code>DisplayObject</code>. Anything that’s a
<code>DisplayObject</code> can be rendered on the stage. <code>Container</code>
is the next class in the inheritance chain. It allows <code>DisplayObject</code>s
to act as containers for other <code>DisplayObject</code>s. Third up the chain is
the <code>Sprite</code> class. That’s the class you’ll be using to make most of
your game objects. However, later you’ll learn how to use
<code>Container</code>s to group sprites together.</p>