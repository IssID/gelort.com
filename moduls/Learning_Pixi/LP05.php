<style type="text/css">

</style>
<div class=titlebar> Learning Pixi </div>
<div class=contentBox>
<?php
if ($auth->isAuth() && $auth->getAccess() >= 5){ 
	echo "<a href=\"?edit_news\">Редактировать</a>";
} ?>
	<div class=contentInBox>


<a href="?LP_Content">Содержание </a>
<br>

<p><a id="user-content-loading"></a></p>

<h2><a id="user-content-loading-images-into-the-texture-cache" class="anchor" href="#loading-images-into-the-texture-cache" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Loading images into the texture cache</h2>

<p>Because Pixi renders the image on the GPU with WebGL, the image needs
to be in a format that the GPU can process. A WebGL-ready image is
called a <strong>texture</strong>. Before you can make a sprite display an image,
you need to convert an ordinary image file into a WebGL texture. To
keep everything working fast and efficiently under the hood, Pixi uses
a <strong>texture cache</strong> to store and reference all the textures your
sprites will need. The names of the textures are strings that match
the file locations of the images they refer to. That means if you have
a texture that was loaded from <code>"images/cat.png"</code>, you could find it in the texture cache like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c1">PIXI</span>.<span class="pl-smi">utils</span>.<span class="pl-smi">TextureCache</span>[<span class="pl-s"><span class="pl-pds">"</span>images/cat.png<span class="pl-pds">"</span></span>];</pre></div>

<p>The textures are stored in a WebGL compatible format that’s efficient
for Pixi’s renderer to work with. You can then use Pixi’s <code>Sprite</code> class to make a new sprite using the texture.</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> texture <span class="pl-k">=</span> <span class="pl-c1">PIXI</span>.<span class="pl-smi">utils</span>.<span class="pl-smi">TextureCache</span>[<span class="pl-s"><span class="pl-pds">"</span>images/anySpriteImage.png<span class="pl-pds">"</span></span>];
<span class="pl-k">var</span> sprite <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.Sprite</span>(texture);</pre></div>

<p>But how do you load the image file and convert it into a texture? Use
Pixi’s built-in <code>loader</code> object. </p>

<p>Pixi's powerful <code>loader</code> object is all you need to load any kind of image. 
Here’s how to use it to load an image and call a function called <code>setup</code> when the 
image has finished loading:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>
  .<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">"</span>images/anyImage.png<span class="pl-pds">"</span></span>)
  .<span class="pl-c1">load</span>(setup);

<span class="pl-k">function</span> <span class="pl-en">setup</span>() {
  <span class="pl-c">//This code will run when the loader has finished loading the image</span>
}</pre></div>

<p><a href="http://www.html5gamedevs.com/topic/16019-preload-all-textures/?p=90907">Pixi’s development team
recommends</a>
that if you use the loader, you should create the sprite by
referencing the the texture in the <code>loader</code>’s <code>resources</code> object, like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-k">var</span> sprite <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.Sprite</span>(
  <span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>.<span class="pl-smi">resources</span>[<span class="pl-s"><span class="pl-pds">"</span>images/anyImage.png<span class="pl-pds">"</span></span>].<span class="pl-smi">texture</span>
);</pre></div>

<p>Here’s an example of some complete code you could write to load an image, 
call the <code>setup</code> function, and create a sprite from the loaded image:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>
  .<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">"</span>images/anyImage.png<span class="pl-pds">"</span></span>)
  .<span class="pl-c1">load</span>(setup);

<span class="pl-k">function</span> <span class="pl-en">setup</span>() {
  <span class="pl-k">var</span> sprite <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-en">PIXI.Sprite</span>(
    <span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>.<span class="pl-smi">resources</span>[<span class="pl-s"><span class="pl-pds">"</span>images/anyImage.png<span class="pl-pds">"</span></span>].<span class="pl-smi">texture</span>
  );
}</pre></div>

<p>This is the general format we’ll be using to load images and create
sprites in this tutorial.</p>

<p>You can load multiple images at single time by listing them with
chainable <code>add</code> methods, like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>
  .<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">"</span>images/imageOne.png<span class="pl-pds">"</span></span>)
  .<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">"</span>images/imageTwo.png<span class="pl-pds">"</span></span>)
  .<span class="pl-c1">add</span>(<span class="pl-s"><span class="pl-pds">"</span>images/imageThree.png<span class="pl-pds">"</span></span>)
  .<span class="pl-c1">load</span>(setup);</pre></div>

<p>Better yet, just list all the files you want to load in
an array inside a single <code>add</code> method, like this:</p>

<div class="highlight highlight-source-js"><pre><span class="pl-c1">PIXI</span>.<span class="pl-smi">loader</span>
  .<span class="pl-c1">add</span>([
    <span class="pl-s"><span class="pl-pds">"</span>images/imageOne.png<span class="pl-pds">"</span></span>,
    <span class="pl-s"><span class="pl-pds">"</span>images/imageTwo.png<span class="pl-pds">"</span></span>,
    <span class="pl-s"><span class="pl-pds">"</span>images/imageThree.png<span class="pl-pds">"</span></span>
  ])
  .<span class="pl-c1">load</span>(setup);</pre></div>

<p>The <code>loader</code> also lets you load JSON files, which you'll learn
about ahead.</p>

</div>
<a href="?LP_Content"> Содержание </a>
</div>
