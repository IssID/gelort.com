<?php
session_start();
class AuthClass {

//Проверяет, авторизован пользователь или нет
	public function isAuth(){
		if(isset($_SESSION["is_auth"])){
			return ($_SESSION["is_auth"]);
		}else return false;
	}
	// public function ban($ban){
	// 	$_SESSION["ban"] = $_SESSION["ban"] + $ban;
	// 		return $_SESSION["ban"];
	// }
//Автоизация пользователя
	public function auth($login,$passwors){
		include 'inc/connect.php';					// подключение к базе данных
			// $questpas = ''; 							// ключь шифрования
			// $md5pas = md5($questpas.$passwors);			// шифрование пароля в md5
			// $res = fetch_array($mysqli->query("SELECT * FROM users WHERE user = '$login'")); //old
		
		$query = $mysqli->query("SELECT * FROM users WHERE user = '$login'"); //new
		$res = $query->fetch_array(MYSQLI_BOTH); //new

		$sqluser = $res['user'];
		$sqlpassword = $res['password'];
		$sqlaccess = $res['access'];
		
		//проверка на авторизацию ($passwors заменить на md5pas)
		if($login == $sqluser && $passwors == $sqlpassword){
		$_SESSION["is_auth"] = true; 			// делаем пользователя авторизованным
		$_SESSION["login"] = $login; 			// записываем в сессию логин пользователя
		$_SESSION["access"] = $sqlaccess;			// записываем в сессию уровень правд доступа
		$_SESSION["ban"] = 0;
		return true;
		}else{ // логин и пароль не подошли
		$_SESSION["is_auth"] = false;
		$_SESSION["ban"] = $_SESSION["ban"] +1; //добавляем переменную для подсчета попыток зарегестрироваться
		return false;
		}
	}
	//возвращает логин авторизованного поьзователя
	public function getLogin(){
		if ($this->isAuth()){
			return $_SESSION["login"];
		}
	}
	//озвращает релевантность. "возможно соединить с предыдущим методом"
	public function getAccess(){
		if ($this->isAuth()){
			return $_SESSION["access"];
		}
	}

	public function out(){
		$_SESSION = array();			// Очищаем сессию
		session_destroy();				//Уничтожаем
	}
}

//проверка логина и пароля

$auth = new AuthClass();
if(isset($_POST["login"]) && isset($_POST["password"])){
	if(!$auth->auth($_POST["login"],$_POST["password"])){
		//echo "<h2>Логин или пароль введен не правильно!</h2>";
	}
}
if(isset($_GET["is_exit"])){ // если нажали кнопку выход
	if($_GET["is_exit"] == 1){
		$auth->out(); //Выходим
		// header("Location: ?is_exit=0");
	}
}
?>
